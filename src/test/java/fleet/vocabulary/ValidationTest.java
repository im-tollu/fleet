package fleet.vocabulary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import fleet.aux.validation.ValidationError;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidationTest {

    @ParameterizedTest
    @MethodSource("validExamples")
    void examplesShouldBeValid(FuelingRegistration registration) {
        var validationResult = registration.validate();

        var message = String.format("Expected to be valid: %s", registration.toString());
        assertTrue(validationResult.isValid(), message);
    }

    @Test
    void invalidWhenPriceIsZero() {
        var zeroPriceFueling = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("0"),
                Volume.wrap("30.22"),
                FuelingDate.wrap("2017-12-03"),
                DriverId.wrap("10f04991-cd75-4f03-a709-a36fcc0d8e14")
        );

        var validationResult = zeroPriceFueling.validate();
        var actualErrors = validationResult.getErrors().toArray();
        var expectedErrors = List.of(new ValidationError("Price must be greater than zero")).toArray();

        assertFalse(validationResult.isValid());
        assertArrayEquals(actualErrors, expectedErrors);
    }

    @Test
    void invalidWhenVolumeIsZero() {
        var zeroVolumeFueling = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("10.111"),
                Volume.wrap("0"),
                FuelingDate.wrap("2017-12-03"),
                DriverId.wrap("10f04991-cd75-4f03-a709-a36fcc0d8e14")
        );

        var validationResult = zeroVolumeFueling.validate();
        var actualErrors = validationResult.getErrors().toArray();
        var expectedErrors = List.of(new ValidationError("Volume must be greater than zero")).toArray();

        assertFalse(validationResult.isValid());
        assertArrayEquals(actualErrors, expectedErrors);
    }

    @Test
    void invalidWhenMembersAreNull() {
        var nullMembersFueling = new FuelingRegistration(null, null, null, null, null);

        var validationResult = nullMembersFueling.validate();
        var actualErrors = validationResult.getErrors().toArray(new ValidationError[]{});
        var expectedErrors = new ValidationError[]{
                new ValidationError("Fueling member must not be null: driverId"),
                new ValidationError("Fueling member must not be null: fuel"),
                new ValidationError("Fueling member must not be null: price"),
                new ValidationError("Fueling member must not be null: volume"),
                new ValidationError("Fueling member must not be null: fuelingDate")
        };
        Arrays.sort(actualErrors, ValidationError.getComparator());
        Arrays.sort(expectedErrors, ValidationError.getComparator());

        assertFalse(validationResult.isValid());
        assertArrayEquals(actualErrors, expectedErrors);
    }

    private static FuelingRegistration[] validExamples() {
        return new FuelingRegistration[]{
                new FuelingRegistration(
                        Fuel.A92,
                        Price.wrap("10.111"),
                        Volume.wrap("30.22"),
                        FuelingDate.wrap("2017-12-03"),
                        DriverId.wrap("10f04991-cd75-4f03-a709-a36fcc0d8e14")
                )
        };
    }

}
