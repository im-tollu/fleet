package fleet.components.registration.businesscore;

import akka.actor.AbstractActor;
import akka.actor.Props;
import fleet.aux.actors.WithReturnAddress;

import java.util.Objects;

public class MockOutPortActor extends AbstractActor {
//    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    static Props props(Class<? extends WithReturnAddress> matchingClass, WithReturnAddress response) {
        Objects.requireNonNull(matchingClass);
        Objects.requireNonNull(response);
        return Props.create(MockOutPortActor.class, () -> new MockOutPortActor(matchingClass, response));
    }

    private MockOutPortActor(Class<? extends WithReturnAddress> matchingClass, WithReturnAddress response) {
        this.matchingClass = matchingClass;
        this.response = response;
    }

    private final Class<? extends WithReturnAddress> matchingClass;
    private final WithReturnAddress response;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(matchingClass, this::receiveMatchingClass)
                .build();
    }

    private void receiveMatchingClass(WithReturnAddress message) {
        var returnAddress = Objects.requireNonNull(message.getReturnAddress());
        response.setReturnAddress(returnAddress);
        response.sendBack();
    }
}
