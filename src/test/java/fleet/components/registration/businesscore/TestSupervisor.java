package fleet.components.registration.businesscore;

import akka.actor.AbstractActor;
import akka.actor.Props;
import fleet.components.registration.coreoperations.CoreSupervisorActor;
import fleet.components.registration.coreoperations.CoreSupervisorRef;
import fleet.components.registration.ports.outdrivers.DriversQuery;
import fleet.components.registration.ports.outeventbus.BusRequest;
import fleet.components.registration.ports.outstorage.StorageRequest;

import static fleet.components.registration.coreoperations.CoreSupervisorRef.GET_ALL_PORTS;

public class TestSupervisor extends AbstractActor {
    static Props props(TestCase testCase) {
        return Props.create(TestSupervisor.class, () -> new TestSupervisor(testCase));
    }

    private TestSupervisor(TestCase testCase) {
        this.testCase = testCase;
    }

    private final TestCase testCase;
    private CoreSupervisorRef coreRef;

    @Override
    public void preStart() {
        var core = getContext().actorOf(CoreSupervisorActor.props(testCase.getConfig()), "Core");
        coreRef = new CoreSupervisorRef(core);
        var ports = coreRef.getAllPorts();

        var storageAdapter = getContext()
                .actorOf(MockOutPortActor.props(StorageRequest.class, testCase.getStorageResponse()), "StorageAdapter");
        var eventBusAdapter = getContext()
                .actorOf(MockOutPortActor.props(BusRequest.class, testCase.getBusResponse()), "EventBusAdapter");
        var driversAdapter = getContext()
                .actorOf(MockOutPortActor.props(DriversQuery.class, testCase.getDriversResponse()), "DriversAdapter");

        ports.getStoragePort().registerAdapter(storageAdapter);
        ports.getEventBusPort().registerAdapter(eventBusAdapter);
        ports.getDriversPort().registerAdapter(driversAdapter);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals(GET_ALL_PORTS, this::receiveGetAllPorts)
                .build();
    }

    private void receiveGetAllPorts(String query) {
        coreRef.getValue().forward(query, getContext());
    }
}
