package fleet.components.registration.businesscore;

import fleet.aux.correlation.CorrelationId;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.ports.inregistration.RegistrationRequest;
import fleet.components.registration.ports.inregistration.RegistrationResponse;
import fleet.components.registration.ports.outdrivers.DriversFailure;
import fleet.components.registration.ports.outdrivers.DriversQuery;
import fleet.components.registration.ports.outdrivers.DriversResponse;
import fleet.components.registration.ports.outeventbus.BusRequest;
import fleet.components.registration.ports.outeventbus.BusResponse;
import fleet.components.registration.ports.outeventbus.BusResponseFailure;
import fleet.components.registration.ports.outstorage.StorageRequest;
import fleet.components.registration.ports.outstorage.StorageResponse;
import fleet.components.registration.ports.outstorage.StorageResponseServiceFailure;

class TestCase {
    private String description;
    private RegistrationConfig config;

    private RegistrationRequest request;
    private RegistrationResponse expectedResponse;

    private DriversQuery expectedDriversQuery;
    private DriversResponse driversResponse = new DriversFailure("Default response", CorrelationId.generate());

    private StorageRequest expectedStorageRequest;
    private StorageResponse storageResponse = new StorageResponseServiceFailure("Default response");

    private BusRequest expectedEventBusEventBusRequest;
    private BusResponse busResponse = new BusResponseFailure("Default response");

    TestCase(String description) {
        this.description = description;
    }

    TestCase withConfig(RegistrationConfig config) {
        this.config = config;
        return this;
    }

    TestCase whenRequested(RegistrationRequest request) {
        this.request = request;
        return this;
    }

    TestCase thenExpectDriversRequest(DriversQuery request) {
        this.expectedDriversQuery = request;
        return this;
    }

    TestCase whenDriversResponseIs(DriversResponse response) {
        this.driversResponse = response;
        return this;
    }

    TestCase thenExpectStorageRequest(StorageRequest request) {
        this.expectedStorageRequest = request;
        return this;
    }

    TestCase whenStorageResponseIs(StorageResponse response) {
        this.storageResponse = response;
        return this;
    }

    TestCase thenExpectEventBusRequest(BusRequest eventBusRequest) {
        this.expectedEventBusEventBusRequest = eventBusRequest;
        return this;
    }

    TestCase whenEventBusResponseIs(BusResponse response) {
        this.busResponse = response;
        return this;
    }

    TestCase thenFinallyExpectRegistrationResponse(RegistrationResponse response) {
        this.expectedResponse = response;
        return this;
    }

    String getDescription() {
        return description;
    }

    RegistrationConfig getConfig() {
        return config;
    }

    RegistrationRequest getRequest() {
        return request;
    }

    RegistrationResponse getExpectedResponse() {
        return expectedResponse;
    }

    DriversResponse getDriversResponse() {
        return driversResponse;
    }

    StorageResponse getStorageResponse() {
        return storageResponse;
    }

    BusResponse getBusResponse() {
        return busResponse;
    }
}
