package fleet.components.registration.businesscore;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static fleet.components.registration.businesscore.TestSuite.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


class BusinessCoreTest {

    private static ActorSystem system;


    @BeforeAll
    static void setup() {
        system = ActorSystem.create();
    }

    @AfterAll
    static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @ParameterizedTest
    @MethodSource("getTestSuite")
    void testBusinessCoreCase(TestCase testCase) {
        var testSupervisor = start(testCase);
        var registrationPort = testSupervisor.getAllPorts().getRegistrationPort();

        var actualResponse = registrationPort.ask(testCase.getRequest(), 1000);

        testSupervisor.stop();

        assertEquals(testCase.getExpectedResponse(), actualResponse, testCase.getDescription());
    }

    private static TestCase[] getTestSuite() {
        return new TestCase[]{
                getSuccessfulCase(),
                getExpiredFuelingCase(),
                getFuelingInFutureCase(),
                getFailedExternalDataCase(),
                getInvalidRequestCase(),
                getNotificationFailureCase(),
                getStorageConstraintViolationCase(),
                getStorageFailureCase()
        };
    }

    private TestSupervisorRef start(TestCase testCase) {
        var testSupervisor = system.actorOf(TestSupervisor.props(testCase));
        return new TestSupervisorRef(testSupervisor);
    }

}
