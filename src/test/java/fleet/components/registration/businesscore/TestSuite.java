package fleet.components.registration.businesscore;

import fleet.aux.correlation.CorrelationId;
import fleet.components.registration.config.DecisionConfig;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.corefunctions.RegistrationApprovedEvent;
import fleet.components.registration.ports.inregistration.RegistrationFailureBusiness;
import fleet.components.registration.ports.inregistration.RegistrationFailureService;
import fleet.components.registration.ports.inregistration.RegistrationRequest;
import fleet.components.registration.ports.inregistration.RegistrationSuccess;
import fleet.components.registration.ports.outdrivers.DriversFailure;
import fleet.components.registration.ports.outdrivers.DriversQuery;
import fleet.components.registration.ports.outdrivers.DriversSuccess;
import fleet.components.registration.ports.outeventbus.BusRequest;
import fleet.components.registration.ports.outeventbus.BusResponseFailure;
import fleet.components.registration.ports.outeventbus.BusResponseSuccess;
import fleet.components.registration.ports.outstorage.StorageRequest;
import fleet.components.registration.ports.outstorage.StorageResponseConstraintViolation;
import fleet.components.registration.ports.outstorage.StorageResponseServiceFailure;
import fleet.components.registration.ports.outstorage.StorageResponseSuccess;
import fleet.vocabulary.*;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

class TestSuite {

    static TestCase getSuccessfulCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );
        var fueling = new Fueling(registration, FuelingId.getExample(), RegistrationTimestamp.now());

        var expectedDriversRequest = new DriversQuery(registration, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var event = new RegistrationApprovedEvent(registration, request.getRegistrationTimestamp());
        var expectedStorageRequest = new StorageRequest(event);


        var storageSuccessfulResponse = new StorageResponseSuccess(fueling);
        var expectedBusRequest = new BusRequest(fueling, request.getCorrelationId());
        var busSuccessfulResponse = new BusResponseSuccess();

        var expectedRegistrationResponse = new RegistrationSuccess(request, storageSuccessfulResponse.getFueling());

        return new TestCase("Successful case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenExpectStorageRequest(expectedStorageRequest)
                .whenStorageResponseIs(storageSuccessfulResponse)
                .thenExpectEventBusRequest(expectedBusRequest)
                .whenEventBusResponseIs(busSuccessfulResponse)
                .thenFinallyExpectRegistrationResponse(expectedRegistrationResponse);
    }

    static TestCase getInvalidRequestCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("-2.005"),
                Volume.wrap("0"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );

        var errorMessage = "Invalid request.";
        var details = List.of(
                "Price must not be negative",
                "Price must be greater than zero",
                "Volume must be greater than zero"
        );
        var expectedBusinessFailureResponse = new RegistrationFailureBusiness(request, errorMessage, details);
        return new TestCase("Invalid request case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenFinallyExpectRegistrationResponse(expectedBusinessFailureResponse);
    }

    static TestCase getExpiredFuelingCase() {
        var expiredRegistration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getExpiredFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                expiredRegistration,
                getRegistrationTimestamp()
        );

        var expectedDriversRequest = new DriversQuery(expiredRegistration, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var failureMessage = "Fueling registration period is expired";
        var expectedBusinessFailureResponse = new RegistrationFailureBusiness(
                request,
                "StorageRequest processing failed because of business rules violation.",
                List.of(failureMessage)
        );

        return new TestCase("Expired fueling case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenFinallyExpectRegistrationResponse(expectedBusinessFailureResponse);
    }
    static TestCase getFuelingInFutureCase() {
        var registrationInFuture = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateInFuture()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registrationInFuture,
                getRegistrationTimestamp()
        );

        var expectedDriversRequest = new DriversQuery(registrationInFuture, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var failureMessage = "Fueling registration is in future";
        var expectedBusinessFailureResponse = new RegistrationFailureBusiness(
                request,
                "StorageRequest processing failed because of business rules violation.",
                List.of(failureMessage)
        );

        return new TestCase("Expired fueling case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenFinallyExpectRegistrationResponse(expectedBusinessFailureResponse);
    }

    static TestCase getFailedExternalDataCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );

        var expectedDriversRequest = new DriversQuery(registration, request.getCorrelationId());
        var failureMessage = "Drivers service is under maintenance";
        var driversFailedResponse = new DriversFailure(failureMessage, request.getCorrelationId());

        var expectedServiceFailureResponse = new RegistrationFailureService(
                request,
                "StorageRequest processing failed because of failed dependency.",
                List.of(failureMessage)
        );

        return new TestCase("Failed external data case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversFailedResponse)
                .thenFinallyExpectRegistrationResponse(expectedServiceFailureResponse);
    }

    static TestCase getStorageConstraintViolationCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );

        var expectedDriversRequest = new DriversQuery(registration, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var event = new RegistrationApprovedEvent(registration, request.getRegistrationTimestamp());
        var expectedStorageRequest = new StorageRequest(event);
        var violationMessage = "Duplicate fuelingId";

        var storageViolationResponse = new StorageResponseConstraintViolation(violationMessage);

        var expectedBusinessFailureResponse = new RegistrationFailureBusiness(
                request,
                "StorageRequest violates storage constraints.",
                List.of(violationMessage)
        );

        return new TestCase("StorageComponent constraint violation case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenExpectStorageRequest(expectedStorageRequest)
                .whenStorageResponseIs(storageViolationResponse)
                .thenFinallyExpectRegistrationResponse(expectedBusinessFailureResponse);
    }

    static TestCase getStorageFailureCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );

        var expectedDriversRequest = new DriversQuery(registration, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var event = new RegistrationApprovedEvent(registration, request.getRegistrationTimestamp());
        var expectedStorageRequest = new StorageRequest(event);
        var failureMessage = "StorageComponent is not accessible";
        var storageFailureResponse = new StorageResponseServiceFailure(failureMessage);

        var expectedServiceFailureResponse = new RegistrationFailureService(
                request,
                "StorageRequest processing failed because of failed storage.",
                List.of(failureMessage)
        );

        return new TestCase("StorageComponent failure case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenExpectStorageRequest(expectedStorageRequest)
                .whenStorageResponseIs(storageFailureResponse)
                .thenFinallyExpectRegistrationResponse(expectedServiceFailureResponse);
    }

    static TestCase getNotificationFailureCase() {
        var registration = new FuelingRegistration(
                Fuel.A92,
                Price.wrap("1.123"),
                Volume.wrap("35.015"),
                FuelingDate.wrap(getFuelingDateString()),
                DriverId.wrap("9c64ec67-96f7-4609-b8c1-af63ea100d6f")
        );
        var request = new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                getRegistrationTimestamp()
        );
        var fueling = new Fueling(registration, FuelingId.getExample(), RegistrationTimestamp.now());

        var expectedDriversRequest = new DriversQuery(registration, request.getCorrelationId());
        var driversSuccessfulResponse = new DriversSuccess(true, request.getCorrelationId());

        var event = new RegistrationApprovedEvent(registration, request.getRegistrationTimestamp());
        var expectedStorageRequest = new StorageRequest(event);
        var storageSuccessfulResponse = new StorageResponseSuccess(fueling);

        var expectedBusRequest = new BusRequest(fueling, request.getCorrelationId());
        var failureMessage = "EventBusRef is overloaded.";
        var busFailureResponse = new BusResponseFailure(failureMessage);

        var expectedServiceFailureResponse = new RegistrationFailureService(
                request,
                "StorageRequest processing failed because of failed event bus.",
                List.of(failureMessage)
        );

        return new TestCase("Notification failure case")
                .withConfig(getConfig())
                .whenRequested(request)
                .thenExpectDriversRequest(expectedDriversRequest)
                .whenDriversResponseIs(driversSuccessfulResponse)
                .thenExpectStorageRequest(expectedStorageRequest)
                .whenStorageResponseIs(storageSuccessfulResponse)
                .thenExpectEventBusRequest(expectedBusRequest)
                .whenEventBusResponseIs(busFailureResponse)
                .thenFinallyExpectRegistrationResponse(expectedServiceFailureResponse);
    }

    private static RegistrationConfig getConfig() {
        return new RegistrationConfig("", 0, Duration.ofMillis(100), 60 * 25);
    }

    private static String getFuelingDateString() {
        return LocalDate.now().minusDays(1).toString();
    }

    private static String getExpiredFuelingDateString() {
        var decisionConfig = DecisionConfig.fromRegistrationConfig(getConfig());
        var expiredDays = decisionConfig.getFuelingExpirationDays() + 1;
        return LocalDate.now().minusDays(expiredDays).toString();
    }

    private static String getFuelingDateInFuture() {
        return LocalDate.now().plusDays(10).toString();
    }

    private static RegistrationTimestamp getRegistrationTimestamp() {
        return new RegistrationTimestamp(Instant.now());
    }
}
