package fleet.components.registration.businesscore;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import fleet.components.registration.coreoperations.CoreSupervisorRef;

class TestSupervisorRef extends CoreSupervisorRef {
    TestSupervisorRef(ActorRef value) {
        super(value);
    }

    void stop(){
        getValue().tell(PoisonPill.getInstance(), ActorRef.noSender());
    }
}
