package fleet.components.registration.adapters.webserver.jackson;

import fleet.components.registration.adapters.webserver.format.ObjectMapperFactory;
import fleet.vocabulary.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ObjectMapperTest {

    @Test
    void SerializationIsInverseOfDeserialization() throws IOException {
        var mapper = ObjectMapperFactory.getMapper();
        var fuelingSample = getFuelingExample();

        var writtenJson = mapper.writeValueAsString(fuelingSample);
        var parsedValue = mapper.readValue(writtenJson, Fueling.class);

        assertEquals(fuelingSample, parsedValue, "Written and parsed fueling must equal the original");
    }

    private Fueling getFuelingExample() {
        return new Fueling(
                FuelingId.getExample(),
                Fuel.getExample(),
                Price.getExample(),
                Volume.getExample(),
                FuelingDate.getExample(),
                DriverId.getExample(),
                RegistrationTimestamp.now()
        );
    }

}
