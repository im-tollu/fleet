package fleet.components.registration;

import akka.actor.AbstractActor;
import akka.actor.AllForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.japi.pf.DeciderBuilder;
import akka.stream.ActorMaterializer;
import fleet.components.registration.adapters.drivers.DriversAdapterSupervisor;
import fleet.components.registration.adapters.eventbus.BusAdapterSupervisor;
import fleet.components.registration.adapters.storage.StorageAdapterSupervisor;
import fleet.components.registration.adapters.webserver.WebServerSupervisor;
import fleet.components.registration.config.RegistrationWebServerConfig;
import fleet.components.registration.config.RegistrationWebServerContext;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.config.RegistrationContext;
import fleet.components.registration.coreoperations.CoreSupervisorActor;
import fleet.components.registration.coreoperations.CoreSupervisorRef;

import java.time.Duration;

public class RegistrationComponentSupervisor extends AbstractActor {
    public static Props props(RegistrationConfig config, RegistrationContext context) {
        return Props.create(
                RegistrationComponentSupervisor.class,
                () -> new RegistrationComponentSupervisor(config, context)
        );
    }

    private RegistrationComponentSupervisor(RegistrationConfig config, RegistrationContext context) {
        this.config = config;
        this.context = context;
    }

    private final RegistrationConfig config;
    private final RegistrationContext context;

    @Override
    public void preStart() {
        var core = getContext().actorOf(CoreSupervisorActor.props(config), "Core");
        var ports = new CoreSupervisorRef(core).getAllPorts();

        var storageAdapter = getContext()
                .actorOf(StorageAdapterSupervisor.props(context.getStorageDataSource()), "StorageAdapter");
        var eventBusAdapter = getContext()
                .actorOf(BusAdapterSupervisor.props(context.getEventBus()), "EventBusAdapter");
        var driversAdapter = getContext()
                .actorOf(DriversAdapterSupervisor.props(context.getStorageDataSource()), "DriversAdapter");

        ports.getStoragePort().registerAdapter(storageAdapter);
        ports.getEventBusPort().registerAdapter(eventBusAdapter);
        ports.getDriversPort().registerAdapter(driversAdapter);

        var webServerConfig = RegistrationWebServerConfig.fromRegistrationConfig(config);
        var materializer = ActorMaterializer.create(context.getActorSystem());
        var webServerContext = new RegistrationWebServerContext(context.getActorSystem(), materializer, ports.getRegistrationPort());
        getContext()
                .actorOf(WebServerSupervisor.props(webServerConfig, webServerContext), "WebServerAdapter");
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new AllForOneStrategy(
                10,
                Duration.ofMinutes(1),
                DeciderBuilder.matchAny(err -> SupervisorStrategy.escalate()).build()
        );
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }
}
