package fleet.components.registration.adapters.storage;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import fleet.components.registration.ports.outstorage.StorageRequest;

import javax.sql.DataSource;

public class StorageAdapterSupervisor extends AbstractActor {
    public static Props props(DataSource dataSource) {
        return Props.create(StorageAdapterSupervisor.class, () -> new StorageAdapterSupervisor(dataSource));
    }

    private StorageAdapterSupervisor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final DataSource dataSource;
    private ActorRef storageFlowSource;

    @Override
    public void preStart() {
        this.storageFlowSource = materializeStorageFlow();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(StorageRequest.class, this::receiveRequest)
                .build();
    }

    private void receiveRequest(StorageRequest request) {
        storageFlowSource.forward(request, getContext());
    }

    private ActorRef materializeStorageFlow() {
        var serviceAdapter = new StorageFlowAdapter(100, dataSource);
        var storageFlow = serviceAdapter.getRunnableGraph();
        var materializer = ActorMaterializer.create(getContext());
        return storageFlow.run(materializer);
    }
}
