package fleet.components.registration.adapters.storage;

import akka.Done;
import akka.actor.ActorRef;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import fleet.aux.actors.WithReturnAddress;
import fleet.components.registration.ports.outstorage.StorageRequest;
import fleet.components.registration.ports.outstorage.StorageResponse;

import javax.sql.DataSource;
import java.util.concurrent.CompletionStage;

public class StorageFlowAdapter {
    private final int bufferSize;
    private final DataSource dataSource;

    public StorageFlowAdapter(int bufferSize, DataSource dataSource) {
        this.dataSource = dataSource;
        this.bufferSize = bufferSize;
    }

    public RunnableGraph<ActorRef> getRunnableGraph() {
        Source<StorageRequest, ActorRef> source = Source.actorRef(bufferSize, OverflowStrategy.dropHead());
        Sink<StorageResponse, CompletionStage<Done>> sink = Sink.foreach(WithReturnAddress::sendBack);
        var storageMapper = new StorageJdbcMapper(dataSource);
        return source
                .mapAsync(bufferSize, storageMapper)
                .toMat(sink, Keep.left());
    }
}
