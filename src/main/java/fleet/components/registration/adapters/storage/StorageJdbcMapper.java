package fleet.components.registration.adapters.storage;

import fleet.components.registration.corefunctions.RegistrationApprovedEvent;
import fleet.components.registration.ports.outstorage.*;

import javax.sql.DataSource;
import java.sql.*;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import akka.japi.function.Function;
import fleet.vocabulary.*;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class StorageJdbcMapper implements Function<StorageRequest, CompletionStage<StorageResponse>> {
    private static final String INSERT_FUELING_SQL = "" +
            "INSERT INTO                \n" +
            "   registration.fueling (  \n" +
            "       fuel,               \n" +
            "       price,              \n" +
            "       volume,             \n" +
            "       fueling_date,       \n" +
            "       driver_id,          \n" +
            "       created_at          \n" +
            "   )                       \n" +
            "VALUES                     \n" +
            "   (?, ?, ?, ?, ?, ?)      \n" +
            ";                          \n";

    private static final String SELECT_FUELING_BY_ID_SQL = "" +
            "SELECT                     \n" +
            "   fueling_key,            \n" +
            "   fuel,                   \n" +
            "   price,                  \n" +
            "   volume,                 \n" +
            "   fueling_date,           \n" +
            "   driver_id,              \n" +
            "   created_at              \n" +
            "FROM                       \n" +
            "   registration.fueling    \n" +
            "WHERE                      \n" +
            "   fueling_key = ?         \n" +
            ";                          \n";

    private final DataSource dataSource;

    public StorageJdbcMapper(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public CompletableFuture<StorageResponse> apply(StorageRequest request) {
        return CompletableFuture.supplyAsync(() -> applySync(request));
    }

    private StorageResponse applySync(StorageRequest request) {
        try {
            var fueling = storeEvent(request.getEvent());
            return buildSuccessResponse(request, fueling);
        } catch (Throwable e) {
            if (e.getCause().getClass().isAssignableFrom(SQLIntegrityConstraintViolationException.class)){
                return buildConstraintViolationResponse(request);
            } else {
                e.printStackTrace();
                return buildServiceFailureResponse(request);
            }
        }
    }

    private Fueling storeEvent(RegistrationApprovedEvent event) throws SQLException {
        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement insert = insertFueling(conn, event);
                ResultSet generatedKeys = insert.getGeneratedKeys();
                PreparedStatement select = prepareSelectInsertedFueling(
                        conn,
                        getInsertedFuelingId(generatedKeys)
                );
                ResultSet inserted = select.executeQuery()
        ) {
            return extractFueling(inserted);
        }
    }

    private PreparedStatement insertFueling(Connection conn, RegistrationApprovedEvent event) throws SQLException {
        var registration = event.getRegistration();
        var fuel = registration.getFuel();
        var price = registration.getPrice();
        var volume = registration.getVolume();
        var fuelingDate = registration.getFuelingDate();
        var driverId = registration.getDriverId();
        var registrationTimestamp = event.getRegistrationTimestamp();
        var createdAt = Timestamp.from(registrationTimestamp.getInstant());

        var statement = conn.prepareStatement(INSERT_FUELING_SQL, RETURN_GENERATED_KEYS);
        statement.setString(1, fuel.getValue());
        statement.setBigDecimal(2, price.getDecimal());
        statement.setBigDecimal(3, volume.getDecimal());
        statement.setObject(4, fuelingDate.getDate());
        statement.setObject(5, driverId.getUuid());
        statement.setTimestamp(6, createdAt);
        statement.executeUpdate();
        return statement;
    }

    private int getInsertedFuelingId(ResultSet generatedKeys) throws SQLException {
        generatedKeys.next();
        return generatedKeys.getInt(1);
    }

    private PreparedStatement prepareSelectInsertedFueling(Connection conn, int id) throws SQLException {
        var statement = conn.prepareStatement(SELECT_FUELING_BY_ID_SQL);
        statement.setInt(1, id);
        return statement;
    }

    private Fueling extractFueling(ResultSet resultSet) throws SQLException {
        resultSet.next();
        var fuelingId = FuelingId.wrap(resultSet.getLong(1));
        var fuel = Fuel.wrap(resultSet.getString(2));
        var price = new Price(resultSet.getBigDecimal(3));
        var volume = new Volume(resultSet.getBigDecimal(4));
        var fuelingDate = FuelingDate.fromLocalDate(resultSet.getDate(5).toLocalDate());
        var driverId = DriverId.fromUuid((UUID) resultSet.getObject(6));
        var createdAt = resultSet.getTimestamp(7);
        var registrationTimestamp = RegistrationTimestamp.fromInstant(createdAt.toInstant());
        return new Fueling(fuelingId, fuel, price, volume, fuelingDate, driverId, registrationTimestamp);
    }

    private StorageResponseSuccess buildSuccessResponse(StorageRequest request, Fueling fueling) {
        var response = new StorageResponseSuccess(fueling);
        response.setReturnAddress(request.getReturnAddress());
        return response;
    }

    private StorageResponseConstraintViolation buildConstraintViolationResponse(StorageRequest request) {
        var message = "Could not store fueling because of constraint violation";
        var response = new StorageResponseConstraintViolation(message);
        response.setReturnAddress(request.getReturnAddress());
        return response;
    }

    private StorageResponseServiceFailure buildServiceFailureResponse(StorageRequest request) {
        var message = "Could not store fueling because of storage malfunction";
        var response = new StorageResponseServiceFailure(message);
        response.setReturnAddress(request.getReturnAddress());
        return response;
    }
}
