package fleet.components.registration.adapters.webserver.format;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fleet.components.registration.ports.inregistration.RegistrationResponse;

public class RegistrationResponseFormat {
    private static final ObjectMapper mapper = ObjectMapperFactory.getMapper();

    public static String toJson(RegistrationResponse registrationResponse) {
        try {
            return mapper.writeValueAsString(registrationResponse);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
