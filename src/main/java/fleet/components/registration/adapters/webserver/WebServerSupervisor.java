package fleet.components.registration.adapters.webserver;

import akka.actor.Props;
import akka.util.Timeout;
import fleet.aux.webserver.AbstractWebServerSupervisor;
import fleet.aux.webserver.Controller;
import fleet.components.registration.adapters.webserver.controllers.BatchRegistrationController;
import fleet.components.registration.adapters.webserver.controllers.SingleRegistrationController;
import fleet.components.registration.config.RegistrationWebServerConfig;
import fleet.components.registration.config.RegistrationWebServerContext;
import fleet.components.registration.ports.inregistration.RegistrationPort;

import java.util.List;

public class WebServerSupervisor extends AbstractWebServerSupervisor {
    public static Props props(RegistrationWebServerConfig config, RegistrationWebServerContext context) {
        return Props.create(WebServerSupervisor.class, () -> new WebServerSupervisor(config, context));
    }

    private WebServerSupervisor(RegistrationWebServerConfig config, RegistrationWebServerContext context) {
        super(
                config.getHost(),
                config.getPort(),
                getControllers(context.getRegistrationPort(), config.getTimeout()),
                context.getActorSystem(),
                context.getMaterializer()
        );
    }

    private static List<Controller> getControllers(
            RegistrationPort registrationPort,
            Timeout timeout
    ) {
        return List.of(
                new SingleRegistrationController(registrationPort, timeout),
                new BatchRegistrationController(registrationPort, timeout)
        );
    }
}
