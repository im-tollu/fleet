package fleet.components.registration.adapters.webserver.controllers;

import akka.NotUsed;
import akka.http.javadsl.model.*;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.JsonFraming;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import akka.util.Timeout;
import fleet.aux.webserver.AbstractController;
import fleet.aux.webserver.ParsingFailure;
import fleet.aux.webserver.ParsingResult;
import fleet.aux.webserver.ParsingSuccess;
import fleet.components.registration.adapters.webserver.format.FuelingRegistrationParser;
import fleet.components.registration.adapters.webserver.format.ParsingFailureFormat;
import fleet.components.registration.adapters.webserver.format.RegistrationRequestBuilder;
import fleet.components.registration.adapters.webserver.format.RegistrationResponseFormat;
import fleet.components.registration.ports.inregistration.RegistrationPort;
import fleet.components.registration.ports.inregistration.RegistrationResponse;
import fleet.vocabulary.FuelingRegistration;

public class BatchRegistrationController
        extends AbstractController<Source<FuelingRegistration, NotUsed>, Source<RegistrationResponse, NotUsed>> {

    private static final String JSON_SEC = "application/json-seq"; // https://tools.ietf.org/html/rfc7464
    private static final int PARALLELISM = 100;
//    private static final long ENTITY_SIZE_LIMIT = BigInteger.valueOf(1024).pow(3).longValue();
    private static final int FRAME_SIZE_LIMIT = 1024;
    private final RegistrationPort registrationPort;
    private final Timeout timeout;

    public BatchRegistrationController(RegistrationPort registrationPort, Timeout timeout) {
        this.registrationPort = registrationPort;
        this.timeout = timeout;
    }

    @Override
    public boolean matchRequest(HttpRequest request) {
        var isPost = request.method().equals(HttpMethods.POST);
        var isRegistrationPath = request.getUri().path().equals("/registration");
        var contentType = request.entity().getContentType().toString();
        var isJsonSeq = contentType.equals(JSON_SEC);
        return isPost && isRegistrationPath && isJsonSeq;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Flow<HttpRequest, ParsingResult, NotUsed> getParsingFlow() {
        return Flow.of(HttpRequest.class)
                .map(request -> new ParsingSuccess(
                        request
                                .entity()
                                .getDataBytes()
                                .via(JsonFraming.objectScanner(FRAME_SIZE_LIMIT))
                                .map(ByteString::utf8String)
                                .map(FuelingRegistrationParser::parse)
                                .filter(ParsingResult::isSuccess)
                                .map(result -> ((ParsingSuccess<FuelingRegistration>) result).getParsedEntity())
                ));
    }

    @Override
    public Flow<Source<FuelingRegistration, NotUsed>, Source<RegistrationResponse, NotUsed>, NotUsed> getProcessingFlow() {
        return Flow.<Source<FuelingRegistration, NotUsed>>create()
                .map(
                        source -> source
                                .map(RegistrationRequestBuilder::from)
                                .ask(PARALLELISM, registrationPort.getValue(), RegistrationResponse.class, timeout)
                );
    }

    @Override
    public Flow<Source<RegistrationResponse, NotUsed>, HttpResponse, NotUsed> getResponseFormattingFlow() {
        var startArray = ByteString.fromString("[\n");
        var comma = ByteString.fromString(", \n\t");
        var endArray = ByteString.fromString("]");
        return Flow.<Source<RegistrationResponse, NotUsed>>create()
                .map(responses -> responses
                        .map(RegistrationResponseFormat::toJson)
                        .map(ByteString::fromString)
                        .intersperse(startArray, comma, endArray)
                )
                .map(jsonStream -> {
                    var entity = HttpEntities.create(ContentTypes.APPLICATION_JSON, jsonStream);
                    return HttpResponse.create().withStatus(StatusCodes.CREATED).withEntity(entity);
                });
    }

    @Override
    public Flow<ParsingFailure, HttpResponse, NotUsed> getParsingFailureFlow() {
        // in this caste parsing should always be successful; failed frames would be just skipped
        return Flow.of(ParsingFailure.class)
                .map(this::formatBadRequestResponse);
    }

    private HttpResponse formatBadRequestResponse(ParsingFailure parsingFailure) {
        var json = ParsingFailureFormat.toJson(parsingFailure);
        return HttpResponse.create()
                .withStatus(StatusCodes.BAD_REQUEST)
                .withEntity(ContentTypes.APPLICATION_JSON, json);
    }
}
