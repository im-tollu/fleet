package fleet.components.registration.adapters.webserver.format;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fleet.aux.webserver.ParsingFailure;

public class ParsingFailureFormat {
    private static final ObjectMapper mapper = ObjectMapperFactory.getMapper();

    public static String toJson(ParsingFailure parsingFailure) {
        try {
            return mapper.writeValueAsString(parsingFailure);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
