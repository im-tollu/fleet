package fleet.components.registration.adapters.webserver.controllers;

import akka.NotUsed;
import akka.http.javadsl.model.*;
import akka.stream.javadsl.Flow;
import akka.util.ByteString;
import akka.util.Timeout;
import fleet.aux.webserver.AbstractController;
import fleet.aux.webserver.ParsingFailure;
import fleet.aux.webserver.ParsingResult;
import fleet.components.registration.adapters.webserver.format.ParsingFailureFormat;
import fleet.components.registration.adapters.webserver.format.RegistrationRequestBuilder;
import fleet.components.registration.adapters.webserver.format.RegistrationResponseFormat;
import fleet.components.registration.adapters.webserver.format.FuelingRegistrationParser;
import fleet.components.registration.ports.inregistration.*;
import fleet.vocabulary.FuelingRegistration;

public class SingleRegistrationController extends AbstractController<FuelingRegistration, RegistrationResponse> {

    private static final int PARALLELISM = 100;
    private static final long SIZE_LIMIT = 1000;
    private final RegistrationPort registrationPort;
    private final Timeout timeout;

    public SingleRegistrationController(RegistrationPort registrationPort, Timeout timeout) {
        this.registrationPort = registrationPort;
        this.timeout = timeout;
    }

    @Override
    public boolean matchRequest(HttpRequest request) {
        var isPost = request.method().equals(HttpMethods.POST);
        var isRegistrationPath = request.getUri().path().equals("/registration");
        var isJson = request.entity().getContentType().equals(ContentTypes.APPLICATION_JSON);
        return isPost && isRegistrationPath && isJson;
    }

    @Override
    public Flow<HttpRequest, ParsingResult, NotUsed> getParsingFlow() {
        return Flow.of(HttpRequest.class)
                .flatMapConcat(request -> request.entity()
                        .withSizeLimit(SIZE_LIMIT)
                        .getDataBytes()
                        .fold(ByteString.empty(), ByteString::concat)
                        .map(ByteString::utf8String)
                        .map(FuelingRegistrationParser::parse));
    }

    @Override
    public Flow<FuelingRegistration, RegistrationResponse, NotUsed> getProcessingFlow() {
        return Flow.of(FuelingRegistration.class)
                .map(RegistrationRequestBuilder::from)
                .ask(PARALLELISM, registrationPort.getValue(), RegistrationResponse.class, timeout);
    }

    @Override
    public Flow<RegistrationResponse, HttpResponse, NotUsed> getResponseFormattingFlow() {
        return Flow.of(RegistrationResponse.class)
                .map(this::formatRegistrationResponse);
    }

    @Override
    public Flow<ParsingFailure, HttpResponse, NotUsed> getParsingFailureFlow() {
        return Flow.of(ParsingFailure.class)
                .map(this::formatBadRequestResponse);
    }

    private HttpResponse formatRegistrationResponse(RegistrationResponse response) {
        StatusCode status;
        if (response instanceof RegistrationSuccess) {
            status = StatusCodes.OK;
        } else if (response instanceof RegistrationFailureBusiness) {
            status = StatusCodes.PRECONDITION_FAILED;
        } else {
            status = StatusCodes.INTERNAL_SERVER_ERROR;
        }
        var json = RegistrationResponseFormat.toJson(response);
        return HttpResponse.create()
                .withStatus(status)
                .withEntity(ContentTypes.APPLICATION_JSON, json);
    }

    private HttpResponse formatBadRequestResponse(ParsingFailure parsingFailure) {
        var json = ParsingFailureFormat.toJson(parsingFailure);
        return HttpResponse.create()
                .withStatus(StatusCodes.BAD_REQUEST)
                .withEntity(ContentTypes.APPLICATION_JSON, json);
    }

}
