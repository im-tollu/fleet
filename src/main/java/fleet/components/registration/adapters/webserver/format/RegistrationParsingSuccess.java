package fleet.components.registration.adapters.webserver.format;

import fleet.aux.webserver.ParsingSuccess;
import fleet.vocabulary.FuelingRegistration;

public class RegistrationParsingSuccess extends ParsingSuccess<FuelingRegistration> {
    public RegistrationParsingSuccess(FuelingRegistration parsedEntity) {
        super(parsedEntity);
    }
}
