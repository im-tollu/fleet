package fleet.components.registration.adapters.webserver.format;

import com.fasterxml.jackson.databind.ObjectMapper;
import fleet.aux.webserver.ParsingFailure;
import fleet.aux.webserver.ParsingResult;
import fleet.aux.webserver.ParsingSuccess;
import fleet.vocabulary.FuelingRegistration;

public class FuelingRegistrationParser {
    private static final ObjectMapper mapper = ObjectMapperFactory.getMapper();

    public static ParsingResult parse(String json) {
        try {
            var registration = mapper.readValue(json, FuelingRegistration.class);
            return new ParsingSuccess<>(registration);
        } catch (Throwable e) {
            return new ParsingFailure(e.getMessage());
        }
    }
}
