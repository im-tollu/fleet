package fleet.components.registration.adapters.webserver.format;

import fleet.aux.correlation.CorrelationId;
import fleet.components.registration.ports.inregistration.RegistrationRequest;
import fleet.vocabulary.FuelingRegistration;
import fleet.vocabulary.RegistrationTimestamp;

public class RegistrationRequestBuilder {
    public static RegistrationRequest from(FuelingRegistration registration) {
        return new RegistrationRequest(
                CorrelationId.generate(),
                registration,
                RegistrationTimestamp.now()
        );
    }
}
