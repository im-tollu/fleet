package fleet.components.registration.adapters.webserver.format;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fleet.aux.jackson.*;
import fleet.components.registration.ports.inregistration.RegistrationFailureBusiness;
import fleet.components.registration.ports.inregistration.RegistrationFailureService;
import fleet.components.registration.ports.inregistration.RegistrationSuccess;
import fleet.vocabulary.*;

import java.util.List;
import java.util.function.Function;

public class ObjectMapperFactory {
    public static ObjectMapper getMapper() {
        var module = new SimpleModule("RegistrationWebServerJacksonModule");
        addDeserializers(module, getDeserializers());
        addSerializers(module, getSerializers());

        var objectMapper = new ObjectMapper();
        objectMapper.registerModules(module);
        return objectMapper;
    }

    private static void addDeserializers(SimpleModule module, JsonDeserializer[] deserializers) {
        for (JsonDeserializer deserializer : deserializers) {
            //noinspection unchecked
            module.addDeserializer(deserializer.handledType(), deserializer);
        }
    }

    private static void addSerializers(SimpleModule module, JsonSerializer[] serializers) {
        for (JsonSerializer serializer : serializers) {
            module.addSerializer(serializer);
        }
    }

    private static JsonDeserializer[] getDeserializers() {
        return new JsonDeserializer[]{
                new ObjectCastingDeserializer<>(FuelingRegistration.class, getFuelingRegistrationCast()),
                new ObjectCastingDeserializer<>(Fueling.class, getFuelingCast()),
                new WrapperDeserializer<>(FuelingId.class, FuelingId::wrap, ValueParser.forLong()),
                new WrapperDeserializer<>(Fuel.class, Fuel::wrap, ValueParser.forString()),
                new WrapperDeserializer<>(DriverId.class, DriverId::wrap, ValueParser.forString()),
                new WrapperDeserializer<>(Price.class, Price::wrap, ValueParser.forString()),
                new WrapperDeserializer<>(Volume.class, Volume::wrap, ValueParser.forString()),
                new WrapperDeserializer<>(FuelingDate.class, FuelingDate::wrap, ValueParser.forString()),
                new WrapperDeserializer<>(RegistrationTimestamp.class, RegistrationTimestamp::wrap, ValueParser.forString())
        };
    }

    private static JsonSerializer[] getSerializers() {
        return new JsonSerializer[]{
                new ObjectCastingSerializer<>(FuelingRegistration.class, getFuelingRegistrationCast()),
                new ObjectCastingSerializer<>(Fueling.class, getFuelingCast()),
                new ObjectCastingSerializer<>(RegistrationSuccess.class, getRegistrationSuccessCast()),
                new ObjectCastingSerializer<>(RegistrationFailureBusiness.class, getRegistrationFailureBusinessCast()),
                new ObjectCastingSerializer<>(RegistrationFailureService.class, getRegistrationFailureServiceCast()),
                new WrapperSerializer<>(FuelingId.class, ValueWriter.forLong()),
                new WrapperSerializer<>(Fuel.class, ValueWriter.forString()),
                new WrapperSerializer<>(DriverId.class, ValueWriter.forString()),
                new WrapperSerializer<>(Price.class, ValueWriter.forString()),
                new WrapperSerializer<>(Volume.class, ValueWriter.forString()),
                new WrapperSerializer<>(FuelingDate.class, ValueWriter.forString()),
                new WrapperSerializer<>(RegistrationTimestamp.class, ValueWriter.forString())
        };
    }

    private static RecordCast<FuelingRegistration> getFuelingRegistrationCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("fuel", Fuel.class),
                new FieldTyping<>("price", Price.class),
                new FieldTyping<>("volume", Volume.class),
                new FieldTyping<>("date", FuelingDate.class),
                new FieldTyping<>("driverId", DriverId.class),
        };
        Function<Object[], FuelingRegistration> valueFactory = fieldValues -> new FuelingRegistration(
                (Fuel) fieldValues[0],
                (Price) fieldValues[1],
                (Volume) fieldValues[2],
                (FuelingDate) fieldValues[3],
                (DriverId) fieldValues[4]
        );
        Function<FuelingRegistration, Object[]> valueExtractor = registration -> new Object[]{
                registration.getFuel(),
                registration.getPrice(),
                registration.getVolume(),
                registration.getFuelingDate(),
                registration.getDriverId()
        };
        return new RecordCast<>(definition, valueFactory, valueExtractor);
    }

    private static RecordCast<Fueling> getFuelingCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("fuelingId", FuelingId.class),
                new FieldTyping<>("fuel", Fuel.class),
                new FieldTyping<>("price", Price.class),
                new FieldTyping<>("volume", Volume.class),
                new FieldTyping<>("date", FuelingDate.class),
                new FieldTyping<>("driverId", DriverId.class),
                new FieldTyping<>("registrationTimestamp", RegistrationTimestamp.class)
        };
        Function<Object[], Fueling> valueFactory = fieldValues -> new Fueling(
                (FuelingId) fieldValues[0],
                (Fuel) fieldValues[1],
                (Price) fieldValues[2],
                (Volume) fieldValues[3],
                (FuelingDate) fieldValues[4],
                (DriverId) fieldValues[5],
                (RegistrationTimestamp) fieldValues[6]
        );
        Function<Fueling, Object[]> valueExtractor = fueling -> new Object[]{
                fueling.getFuelingId(),
                fueling.getFuel(),
                fueling.getPrice(),
                fueling.getVolume(),
                fueling.getFuelingDate(),
                fueling.getDriverId(),
                fueling.getRegistrationTimestamp()
        };
        return new RecordCast<>(definition, valueFactory, valueExtractor);
    }

    private static RecordCast<RegistrationSuccess> getRegistrationSuccessCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("status", String.class),
                new FieldTyping<>("fueling", Fueling.class)
        };
        Function<Object[], RegistrationSuccess> valueFactory = fieldValues -> {
            throw new UnsupportedOperationException("RegistrationSuccess is not supposed to be parsed from JSON");
        };
        Function<RegistrationSuccess, Object[]> valueExtractor = registrationSuccess -> new Object[]{
                "Success",
                registrationSuccess.getFueling()
        };
        return new RecordCast<>(definition, valueFactory, valueExtractor);
    }

    private static RecordCast<RegistrationFailureBusiness> getRegistrationFailureBusinessCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("status", String.class),
                new FieldTyping<>("message", String.class),
                new FieldTyping<>("details", List.class)
        };
        Function<Object[], RegistrationFailureBusiness> valueFactory = fieldValues -> {
            throw new UnsupportedOperationException("RegistrationFailureBusiness is not supposed to be parsed from JSON");
        };
        Function<RegistrationFailureBusiness, Object[]> valueExtractor = registrationFailure -> new Object[]{
                "BusinessFailure",
                registrationFailure.getMessage(),
                registrationFailure.getDetails()
        };
        return new RecordCast<>(definition, valueFactory, valueExtractor);
    }
    private static RecordCast<RegistrationFailureService> getRegistrationFailureServiceCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("status", String.class),
                new FieldTyping<>("message", String.class),
                new FieldTyping<>("details", List.class)
        };
        Function<Object[], RegistrationFailureService> valueFactory = fieldValues -> {
            throw new UnsupportedOperationException("RegistrationFailureService is not supposed to be parsed from JSON");
        };
        Function<RegistrationFailureService, Object[]> valueExtractor = registrationFailure -> new Object[]{
                "ServiceFailure",
                registrationFailure.getMessage(),
                registrationFailure.getDetails()
        };
        return new RecordCast<>(definition, valueFactory, valueExtractor);
    }
}
