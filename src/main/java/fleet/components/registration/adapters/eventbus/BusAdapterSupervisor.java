package fleet.components.registration.adapters.eventbus;

import akka.actor.AbstractActor;
import akka.actor.Props;
import fleet.components.eventbus.EventBusRef;
import fleet.components.registration.ports.outeventbus.BusRequest;
import fleet.components.registration.ports.outeventbus.BusResponseSuccess;

public class BusAdapterSupervisor extends AbstractActor {
    public static Props props(EventBusRef eventBus) {
        return Props.create(BusAdapterSupervisor.class, () -> new BusAdapterSupervisor(eventBus));
    }

    private final EventBusRef eventBus;

    private BusAdapterSupervisor(EventBusRef eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(BusRequest.class, this::receiveBusRequest)
                .build();
    }

    private void receiveBusRequest(BusRequest request) {
        var message = BusMessageConverter.convert(request);
        eventBus.publish(message);
        // Here we emulate acknowledge from the message bus
        // In real life fuel acknowledgements should be tracked and notifications retries in the background
        var response = new BusResponseSuccess();
        response.setReturnAddress(request.getReturnAddress());
        response.sendBack();
    }
}
