package fleet.components.registration.adapters.eventbus;

import fleet.components.eventbus.Message;
import fleet.components.registration.ports.outeventbus.BusRequest;

class BusMessageConverter {
    static Message convert(BusRequest request) {
        return new Message(request.getFueling(), request.getCorrelationId());
    }
}
