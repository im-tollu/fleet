package fleet.components.registration.adapters.drivers;

import akka.japi.function.Function;
import fleet.components.registration.ports.outdrivers.DriversFailure;
import fleet.components.registration.ports.outdrivers.DriversQuery;
import fleet.components.registration.ports.outdrivers.DriversResponse;
import fleet.components.registration.ports.outdrivers.DriversSuccess;
import fleet.vocabulary.DriverId;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class DriversJdbcMapper implements Function<DriversQuery, CompletionStage<DriversResponse>> {
    private static final String LOOKUP_DRIVER = "" +
            "SELECT                 \n" +
            "   count(1) as found   \n" +
            "FROM                   \n" +
            "   drivers.driver      \n" +
            "WHERE                  \n" +
            "   driver_id = ?       \n" +
            ";                      \n";
    private final DataSource dataSource;

    DriversJdbcMapper(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public CompletionStage<DriversResponse> apply(DriversQuery query) {
        return CompletableFuture.supplyAsync(() -> applySync(query));
    }

    private DriversResponse applySync(DriversQuery query) {
        try {
            boolean isDriverFound = lookupDriver(query.getDriverId());
            return buildSuccessResponse(isDriverFound, query);
        } catch (Throwable e) {
            e.printStackTrace();
            return buildServiceFailureResponse(query);
        }
    }

    private boolean lookupDriver(DriverId driverId) throws SQLException {
        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement lookup = countDriver(conn, driverId);
                ResultSet resultSet = lookup.getResultSet()
        ) {
            resultSet.next();
            var foundDriversCount = resultSet.getInt("found");
            return foundDriversCount > 0;
        }
    }

    private PreparedStatement countDriver(Connection conn, DriverId driverId) throws SQLException {
        var statement = conn.prepareStatement(LOOKUP_DRIVER);
        statement.setObject(1, driverId.getUuid());
        statement.executeQuery();
        return statement;
    }

    private DriversResponse buildSuccessResponse(boolean isDriverFound, DriversQuery query) {
        var response =  new DriversSuccess(isDriverFound, query.getCorrelationId());
        response.setReturnAddress(query.getReturnAddress());
        return response;
    }

    private DriversResponse buildServiceFailureResponse(DriversQuery query) {
        var response = new DriversFailure("Could not lookup driver", query.getCorrelationId());
        response.setReturnAddress(query.getReturnAddress());
        return response;
    }
}
