package fleet.components.registration.config;

import java.time.Duration;

public class RegistrationConfig {
    private final Duration processingTimeout;
    private final long fuelingExpirationDays;
    private final String host;
    private final int port;

    public RegistrationConfig(
            String host,
            int port,
            Duration processingTimeout,
            long fuelingExpirationMinutes
    ) {
        this.processingTimeout = processingTimeout;
        this.fuelingExpirationDays = fuelingExpirationMinutes;
        this.host = host;
        this.port = port;
    }

    public long getFuelingExpirationDays() {
        return fuelingExpirationDays;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public Duration getProcessingTimeout() {
        return processingTimeout;
    }
}
