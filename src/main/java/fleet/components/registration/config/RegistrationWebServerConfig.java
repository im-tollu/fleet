package fleet.components.registration.config;

import akka.util.Timeout;

public class RegistrationWebServerConfig {
    private final String host;
    private final int port;
    private final Timeout timeout;

    public RegistrationWebServerConfig(String host, int port, Timeout timeout) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public Timeout getTimeout() {
        return timeout;
    }

    public static RegistrationWebServerConfig fromRegistrationConfig(RegistrationConfig registrationConfig) {
        // Processing timeout should be handled by the business core.
        // That's why for web server we set twice processing timeout - it should never be reached.
        var timeout = Timeout.create(registrationConfig.getProcessingTimeout().multipliedBy(2));
        return new RegistrationWebServerConfig(
                registrationConfig.getHost(),
                registrationConfig.getPort(),
                timeout
        );
    }
}
