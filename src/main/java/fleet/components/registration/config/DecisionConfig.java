package fleet.components.registration.config;

public class DecisionConfig {
    // Fueling registration should be requested within this many days since fueling,
    // otherwise registration request should be rejected
    private final long fuelingExpirationDays;

    public DecisionConfig(long fuelingExpirationDays) {
        this.fuelingExpirationDays = fuelingExpirationDays;
    }

    public long getFuelingExpirationDays() {
        return fuelingExpirationDays;
    }

    public static DecisionConfig fromRegistrationConfig(RegistrationConfig registrationConfig) {
        return new DecisionConfig(registrationConfig.getFuelingExpirationDays());
    }
}
