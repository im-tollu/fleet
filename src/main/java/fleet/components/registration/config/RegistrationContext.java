package fleet.components.registration.config;

import akka.actor.ActorSystem;
import fleet.components.eventbus.EventBusRef;

import javax.sql.DataSource;

public class RegistrationContext {
    private final ActorSystem actorSystem;
    private final DataSource storageDataSource;
    private final EventBusRef eventBus;

    public RegistrationContext(ActorSystem actorSystem, DataSource storageDataSource, EventBusRef eventBus) {
        this.actorSystem = actorSystem;
        this.storageDataSource = storageDataSource;
        this.eventBus = eventBus;
    }

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public DataSource getStorageDataSource() {
        return storageDataSource;
    }

    public EventBusRef getEventBus() {
        return eventBus;
    }
}
