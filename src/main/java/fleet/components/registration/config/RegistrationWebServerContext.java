package fleet.components.registration.config;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import fleet.components.registration.ports.inregistration.RegistrationPort;

public class RegistrationWebServerContext {
    private final ActorSystem actorSystem;
    private final RegistrationPort registrationPort;
    private final Materializer materializer;

    public RegistrationWebServerContext(
            ActorSystem actorSystem,
            Materializer materializer,
            RegistrationPort registrationPort
    ) {
        this.actorSystem = actorSystem;
        this.materializer = materializer;
        this.registrationPort = registrationPort;
    }

    public RegistrationPort getRegistrationPort() {
        return registrationPort;
    }

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public Materializer getMaterializer() {
        return materializer;
    }
}
