package fleet.components.registration.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import fleet.aux.actors.OutPort.AdapterPlug;
import fleet.components.registration.ports.outeventbus.BusRequest;

public class EventBusPortActor extends AbstractActor {
    public static Props props() {
        return Props.create(EventBusPortActor.class);
    }

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private ActorRef adapter = getContext().getSystem().deadLetters();

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AdapterPlug.class, plug -> this.adapter = plug.getValue())
                .match(BusRequest.class, request -> adapter.forward(request, getContext()))
                .build();
    }

}
