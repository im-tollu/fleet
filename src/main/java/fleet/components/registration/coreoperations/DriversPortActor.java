package fleet.components.registration.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import fleet.aux.actors.OutPort.AdapterPlug;
import fleet.components.registration.ports.outdrivers.DriversQuery;

public class DriversPortActor extends AbstractActor {
    public static Props props() {
        return Props.create(DriversPortActor.class);
    }

    private ActorRef adapter = getContext().getSystem().deadLetters();

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AdapterPlug.class, plug -> this.adapter = plug.getValue())
                .match(DriversQuery.class, this::receiveDriversQuery)
                .build();
    }

    private void receiveDriversQuery(DriversQuery query) {
        adapter.tell(query, ActorRef.noSender());
    }
}
