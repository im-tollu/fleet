package fleet.components.registration.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import fleet.aux.actors.OutPort.AdapterPlug;
import fleet.components.registration.ports.outstorage.StorageRequest;

public class StoragePortActor extends AbstractActor {
    public static Props props() {
        return Props.create(StoragePortActor.class);
    }

    private ActorRef adapter = getContext().getSystem().deadLetters();

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AdapterPlug.class, plug -> this.adapter = plug.getValue())
                .match(StorageRequest.class, request -> adapter.forward(request, getContext()))
                .build();
    }

}
