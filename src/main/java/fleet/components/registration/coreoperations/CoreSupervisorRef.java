package fleet.components.registration.coreoperations;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;
import fleet.components.registration.ports.AllPorts;

public class CoreSupervisorRef extends ActorRefWrapper {
    public static final String GET_ALL_PORTS = "Get all ports";

    public CoreSupervisorRef(ActorRef value) {
        super(value);
    }

    public AllPorts getAllPorts() {
        return (AllPorts) ask(GET_ALL_PORTS, 1000L);
    }
}
