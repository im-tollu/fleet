package fleet.components.registration.coreoperations;

import akka.actor.AbstractActorWithTimers;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import fleet.aux.actors.TimeoutNotification;
import fleet.components.registration.config.DecisionConfig;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.corefunctions.DecisionMaker;
import fleet.components.registration.corefunctions.RegistrationWorkflow;
import fleet.components.registration.ports.OutPorts;
import fleet.components.registration.ports.inregistration.RegistrationRequest;
import fleet.components.registration.ports.outdrivers.DriversResponse;
import fleet.components.registration.ports.outeventbus.BusResponse;
import fleet.components.registration.ports.outstorage.StorageResponse;

public class RegistrationWorkerActor extends AbstractActorWithTimers {

    public static Props props(
            RegistrationRequest request,
            RegistrationConfig config,
            OutPorts outPorts
    ) {
        return Props.create(RegistrationWorkerActor.class, () -> new RegistrationWorkerActor(request, config, outPorts));
    }

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private final RegistrationRequest request;
    private final RegistrationConfig config;

    private final OutPorts outPorts;
    private final DecisionMaker decisionMaker;

    private final RegistrationWorkflow workflow;

    private RegistrationWorkerActor(
            RegistrationRequest request,
            RegistrationConfig config,
            OutPorts outPorts
    ) {
        this.request = request;
        this.config = config;
        this.outPorts = outPorts;
        var decisionConfig = DecisionConfig.fromRegistrationConfig(config);
        this.decisionMaker = new DecisionMaker(decisionConfig);
        this.workflow = new RegistrationWorkflow(request);
    }

    @Override
    public void preStart() {
        setProcessingTimeout();
        proceedWithValidateRequest();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(DriversResponse.class, this::receiveExternalData)
                .match(StorageResponse.class, this::receiveStorageResponse)
                .match(BusResponse.class, this::receiveEventBusResponse)
                .match(TimeoutNotification.class, this::receiveTimeout)
                .build();
    }

    private void setProcessingTimeout() {
        var timeout = config.getProcessingTimeout();
        var notification = new TimeoutNotification("RegistrationWorkerTimeout");
        getTimers().startSingleTimer(notification.getKey(), notification, timeout);
    }

    private void proceedWithValidateRequest() {
        var validation = request.validate();
        workflow.acceptRequestValidation(validation);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case RequestValid:
                proceedWithExternalDataRequests();
                break;
            case RequestInvalid:
                finishWithRegistrationFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void proceedWithExternalDataRequests() {
        workflow.accumulateExternalData();
        var drivers = outPorts.getDriversPort();
        var driversQuery = workflow.getDriverQuery();
        driversQuery.setReturnAddress(getSelf());
        drivers.queryDriversData(driversQuery);
    }


    private void receiveExternalData(DriversResponse response) {
        workflow.acceptExternalData(response);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case AccumulatingExternalData:
                break;
            case ExternalDataSuccess:
                proceedWithCommand();
                break;
            case ExternalDataFailure:
                finishWithRegistrationFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void proceedWithCommand() {
        workflow.buildCommand();
        var result = decisionMaker.process(workflow.getCommand());
        workflow.acceptCommandResult(result);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case CommandSuccess:
                proceedWithExecution();
                break;
            case CommandFailure:
                finishWithRegistrationFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void proceedWithExecution() {
        workflow.executeDecision();

        var storage = outPorts.getStoragePort();
        var request = workflow.getStorageRequest();
        request.setReturnAddress(getSelf());
        storage.storeApprovedRegistration(request);
    }

    private void receiveStorageResponse(StorageResponse response) {
        workflow.acceptStorageResult(response);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case StorageSuccess:
                proceedWithNotification();
                break;
            case StorageFailure:
                finishWithRegistrationFailure();
                break;
            case StorageConstraintViolation:
                finishWithRegistrationFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void proceedWithNotification() {
        workflow.notifyResult();
        var eventBus = outPorts.getEventBusPort();
        var request = workflow.getEventBusRequest();
        request.setReturnAddress(getSelf());
        eventBus.publishEvent(request);
    }

    private void receiveEventBusResponse(BusResponse response) {
        workflow.acceptNotificationResult(response);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case NotificationSuccess:
                finishWithRegistrationSuccess();
                break;
            case NotificationFailure:
                finishWithRegistrationFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void receiveTimeout(@SuppressWarnings("unused") TimeoutNotification timeout) {
        workflow.acceptTimeout();
        finishWithRegistrationFailure();
    }

    private void finishWithRegistrationFailure() {
        workflow.buildFailureResponse();
        var response = workflow.getRegistrationResponse();
        response.setReturnAddress(request.getReturnAddress());
        response.sendBack();
        stop();
    }

    private void finishWithRegistrationSuccess() {
        workflow.acceptSuccess();
        workflow.buildSuccessfulResponse();
        var response = workflow.getRegistrationResponse();
        response.setReturnAddress(request.getReturnAddress());
        response.sendBack();
        stop();
    }

    private void stop() {
        getContext().stop(getSelf());
    }

}
