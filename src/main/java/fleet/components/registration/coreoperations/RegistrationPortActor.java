package fleet.components.registration.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.japi.pf.DeciderBuilder;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.ports.OutPorts;
import fleet.components.registration.ports.inregistration.RegistrationRequest;

public class RegistrationPortActor extends AbstractActor {

    public static Props props(RegistrationConfig config, OutPorts outPorts) {
        return Props.create(RegistrationPortActor.class, () -> new RegistrationPortActor(config, outPorts));
    }

    private RegistrationPortActor(RegistrationConfig config, OutPorts outPorts) {
        this.config = config;
        this.outPorts = outPorts;
    }

    private final RegistrationConfig config;
    private final OutPorts outPorts;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RegistrationRequest.class, this::receiveFuelingRegistrationRequest)
                .build();
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new OneForOneStrategy(DeciderBuilder.matchAny(err -> SupervisorStrategy.stop()).build());
    }

    private void receiveFuelingRegistrationRequest(RegistrationRequest request) {
        request.setReturnAddress(sender());
        var workerProps = RegistrationWorkerActor.props(request, config, outPorts);
        getContext().actorOf(workerProps);
    }

}
