package fleet.components.registration.coreoperations;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.ports.*;
import fleet.components.registration.ports.inregistration.RegistrationPort;
import fleet.components.registration.ports.outdrivers.DriversPort;
import fleet.components.registration.ports.outeventbus.BusPort;
import fleet.components.registration.ports.outstorage.StoragePort;

import java.time.Duration;

import static fleet.components.registration.coreoperations.CoreSupervisorRef.GET_ALL_PORTS;

public class CoreSupervisorActor extends AbstractActor {

    public static Props props(RegistrationConfig config) {
        return Props.create(CoreSupervisorActor.class, () -> new CoreSupervisorActor(config));
    }

    private final RegistrationConfig config;

    private AllPorts allPorts;

    public CoreSupervisorActor(RegistrationConfig config) {
        this.config = config;
    }

    @Override
    public void preStart() {
        startChildren();
    }

    private void startChildren() {
        var driversPort = new DriversPort(childOf(DriversPortActor.props()));
        var eventBusPort = new BusPort(childOf(EventBusPortActor.props()));
        var storagePort = new StoragePort(childOf(StoragePortActor.props()));

        var outPorts = new OutPorts(driversPort, storagePort, eventBusPort);

        var registrationPort = new RegistrationPort(childOf(RegistrationPortActor.props(config, outPorts)));

        this.allPorts = new AllPorts(registrationPort, outPorts);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals(GET_ALL_PORTS, (msg) -> receiveGetAllPorts())
                .build();
    }

    private void receiveGetAllPorts() {
        getSender().tell(this.allPorts, getSelf());
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new AllForOneStrategy(
                10,
                Duration.ofMinutes(1),
                DeciderBuilder
                        .matchAny(o -> SupervisorStrategy.escalate())
                        .build());
    }

    private ActorRef childOf(Props props) {
        return getContext().actorOf(props);
    }
}
