package fleet.components.registration.corefunctions;

import fleet.components.registration.config.DecisionConfig;
import fleet.vocabulary.BusinessCommandProcessingException;

public class DecisionMaker {
    private final DecisionConfig config;

    public DecisionMaker(DecisionConfig config) {
        this.config = config;
    }

    public DecisionResult process(RegisterFuelingCommand command) {
        try {
            return processHappyPath(command);
        } catch (BusinessCommandProcessingException reason) {
            return new DecisionFailure(reason.getMessage());
        }
    }

    private DecisionResult processHappyPath(RegisterFuelingCommand command) {
        ensureDriverValid(command);
        ensureFuelingNotExpired(command);
        ensureFuelingNotInFuture(command);
        var event = new RegistrationApprovedEvent(command.getRegistration(), command.getRegistrationTimestamp());
        return new DecisionSuccess(command, event);
    }

    private void ensureDriverValid(RegisterFuelingCommand command) {
        if (!command.isDriverValid()) {
            throw new BusinessCommandProcessingException("Driver is invalid");
        }
    }

    private void ensureFuelingNotExpired(RegisterFuelingCommand command) {
        var fuelingDate = command.getFuelingLocalDate();
        var registrationDateTime = command.getRegistrationLocalDateTime();
        var expirationDateTime = fuelingDate
                .plusDays(config.getFuelingExpirationDays())
                .atStartOfDay();
        var isExpired = registrationDateTime.isAfter(expirationDateTime);
        if (isExpired) {
            throw new BusinessCommandProcessingException("Fueling registration period is expired");
        }
    }

    private void ensureFuelingNotInFuture(RegisterFuelingCommand command) {
        var fuelingDate = command.getFuelingLocalDate();
        var registrationDateTime = command.getRegistrationLocalDateTime();
        var isInFuture = fuelingDate.isAfter(registrationDateTime.toLocalDate());
        if (isInFuture) {
            throw new BusinessCommandProcessingException("Fueling registration is in future");
        }
    }

}
