package fleet.components.registration.corefunctions;

public class DecisionSuccess implements DecisionResult {
    private final RegisterFuelingCommand command;
    private final RegistrationApprovedEvent event;

    public DecisionSuccess(RegisterFuelingCommand command, RegistrationApprovedEvent event) {
        this.command = command;
        this.event = event;
    }

    public RegisterFuelingCommand getCommand() {
        return command;
    }

    public RegistrationApprovedEvent getEvent() {
        return event;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }
}
