package fleet.components.registration.corefunctions;

import fleet.vocabulary.FuelingRegistration;
import fleet.vocabulary.RegistrationTimestamp;

public class RegistrationApprovedEvent {
    private final FuelingRegistration registration;
    private final RegistrationTimestamp registrationTimestamp;

    public RegistrationApprovedEvent(FuelingRegistration registration, RegistrationTimestamp registrationTimestamp) {
        this.registration = registration;
        this.registrationTimestamp = registrationTimestamp;
    }

    public FuelingRegistration getRegistration() {
        return registration;
    }

    public RegistrationTimestamp getRegistrationTimestamp() {
        return registrationTimestamp;
    }
}
