package fleet.components.registration.corefunctions;

import fleet.aux.result.WithDetails;
import fleet.aux.statemachine.StateMachine;
import fleet.aux.validation.ValidationResult;
import fleet.components.registration.ports.inregistration.*;
import fleet.components.registration.ports.outdrivers.DriversResponse;
import fleet.components.registration.ports.outdrivers.DriversQuery;
import fleet.components.registration.ports.outdrivers.DriversSuccess;
import fleet.components.registration.ports.outeventbus.BusRequest;
import fleet.components.registration.ports.outeventbus.BusResponse;
import fleet.components.registration.ports.outeventbus.BusResponseFailure;
import fleet.components.registration.ports.outstorage.*;

import java.util.Collections;
import java.util.List;

import static fleet.components.registration.corefunctions.RegistrationStatus.*;

public class RegistrationWorkflow extends StateMachine<RegistrationStatus> {
    private final RegistrationRequest request;
    private ValidationResult requestValidation;

    private DriversQuery driverQuery;
    private DriversResponse driverResponse;

    private RegisterFuelingCommand command;
    private DecisionResult decisionResult;

    private StorageRequest storageRequest;
    private StorageResponse storageResponse;

    private BusRequest eventBusRequest;
    private BusResponse eventBusResponse;

    private RegistrationResponse registrationResponse;

    public RegistrationWorkflow(RegistrationRequest request) {
        this.request = request;
    }

    @Override
    protected RegistrationStatus getInitialState() {
        return Created;
    }

    public void acceptRequestValidation(ValidationResult requestValidation) {
        ensureStatus(RegistrationStatus.Created);
        this.requestValidation = requestValidation;
        if (requestValidation.isValid()) {
            transitionTo(RequestValid);
        } else {
            transitionTo(RequestInvalid);
        }
    }

    public void accumulateExternalData() {
        ensureStatus(RequestValid);
        this.driverQuery = new DriversQuery(request.getRegistration(), request.getCorrelationId());
        transitionTo(AccumulatingExternalData);
    }

    public void acceptExternalData(DriversResponse response) {
        ensureStatus(AccumulatingExternalData);
        this.driverResponse = response;
        if (response.isSuccess()) {
            transitionTo(ExternalDataSuccess);
        } else {
            transitionTo(ExternalDataFailure);
        }
    }

    public void buildCommand() {
        ensureStatus(ExternalDataSuccess);
        command = new RegisterFuelingCommand();
        command.setRegistration(request.getRegistration());
        command.setRegistrationTimestamp(request.getRegistrationTimestamp());
        command.setDriverValid(((DriversSuccess) driverResponse).isDriverValid());
        transitionTo(CommandBuilt);
    }

    public void acceptCommandResult(DecisionResult commandResult) {
        ensureStatus(CommandBuilt);
        this.decisionResult = commandResult;
        if (commandResult.isSuccess()) {
            transitionTo(CommandSuccess);
        } else {
            transitionTo(CommandFailure);
        }
    }

    public void executeDecision() {
        ensureStatus(CommandSuccess);
        var approvedRegistration = ((DecisionSuccess) decisionResult).getEvent();
        this.storageRequest = new StorageRequest(approvedRegistration);
        transitionTo(StoringResult);
    }

    public void acceptStorageResult(StorageResponse result) {
        ensureStatus(StoringResult);
        this.storageResponse = result;
        if (result.isSuccess()) {
            transitionTo(StorageSuccess);
        } else if (result instanceof StorageResponseServiceFailure) {
            transitionTo(StorageFailure);
        } else if (result instanceof StorageResponseConstraintViolation) {
            transitionTo(StorageConstraintViolation);
        }
    }

    public void notifyResult() {
        ensureStatus(StorageSuccess);
        var fueling = ((StorageResponseSuccess) storageResponse).getFueling();
        this.eventBusRequest = new BusRequest(fueling, request.getCorrelationId());
        transitionTo(Notifying);
    }

    public void acceptNotificationResult(BusResponse result) {
        ensureStatus(Notifying);
        this.eventBusResponse = result;
        if (result.isSuccess()) {
            transitionTo(NotificationSuccess);
        } else {
            transitionTo(NotificationFailure);
        }
    }

    public void acceptTimeout() {
        transitionTo(RegistrationStatus.Timeout);
    }

    public void acceptSuccess() {
        ensureStatus(NotificationSuccess);
        transitionTo(RegistrationStatus.Success);
    }

    public void buildFailureResponse() {
        ensureStatus(RequestInvalid, ExternalDataFailure, CommandFailure, StorageFailure, StorageConstraintViolation, NotificationFailure, Timeout);
        String message;
        List<String> details;
        switch (getStatus()) {
            case RequestInvalid:
                message = "Invalid request.";
                details = requestValidation.getMessages();
                this.registrationResponse = new RegistrationFailureBusiness(request, message, details);
                break;
            case ExternalDataFailure:
                message = "StorageRequest processing failed because of failed dependency.";
                details = ((WithDetails) driverResponse).getDetails();
                this.registrationResponse = new RegistrationFailureService(request, message, details);
                break;
            case CommandFailure:
                message = "StorageRequest processing failed because of business rules violation.";
                details = ((DecisionFailure) decisionResult).getDetails();
                this.registrationResponse = new RegistrationFailureBusiness(request, message, details);
                break;
            case StorageFailure:
                message = "StorageRequest processing failed because of failed storage.";
                details = ((StorageResponseServiceFailure) storageResponse).getDetails();
                this.registrationResponse = new RegistrationFailureService(request, message, details);
                break;
            case StorageConstraintViolation:
                message = "StorageRequest violates storage constraints.";
                details = ((StorageResponseConstraintViolation) storageResponse).getDetails();
                this.registrationResponse = new RegistrationFailureBusiness(request, message, details);
                break;
            case NotificationFailure:
                message = "StorageRequest processing failed because of failed event bus.";
                details = ((BusResponseFailure) eventBusResponse).getDetails();
                this.registrationResponse = new RegistrationFailureService(request, message, details);
                break;
            case Timeout:
                message = "StorageRequest processing failed because of timeout.";
                details = Collections.emptyList();
                this.registrationResponse = new RegistrationFailureService(request, message, details);
                break;
        }
    }

    public void buildSuccessfulResponse() {
        ensureStatus(RegistrationStatus.Success);
        var fueling = ((StorageResponseSuccess)storageResponse).getFueling();
        this.registrationResponse = new RegistrationSuccess(request, fueling);
    }

    public RegistrationRequest getRequest() {
        return request;
    }

    public DriversQuery getDriverQuery() {
        return driverQuery;
    }

    public RegisterFuelingCommand getCommand() {
        return command;
    }

    public StorageRequest getStorageRequest() {
        return storageRequest;
    }

    public BusRequest getEventBusRequest() {
        return eventBusRequest;
    }

    public RegistrationResponse getRegistrationResponse() {
        return registrationResponse;
    }

}
