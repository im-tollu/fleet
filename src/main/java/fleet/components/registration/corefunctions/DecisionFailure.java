package fleet.components.registration.corefunctions;

import fleet.aux.result.WithDetails;

import java.util.List;

public class DecisionFailure implements DecisionResult, WithDetails {
    private final String message;

    public DecisionFailure(String message) {
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public List<String> getDetails() {
        return List.of(message);
    }
}
