package fleet.components.registration.corefunctions;

import fleet.vocabulary.FuelingRegistration;
import fleet.vocabulary.RegistrationTimestamp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class RegisterFuelingCommand {
    private FuelingRegistration registration;
    private RegistrationTimestamp registrationTimestamp;
    private boolean isDriverValid;

    public FuelingRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(FuelingRegistration fueling) {
        this.registration = fueling;
    }

    public RegistrationTimestamp getRegistrationTimestamp() {
        return registrationTimestamp;
    }

    public void setRegistrationTimestamp(RegistrationTimestamp registrationTimestamp) {
        this.registrationTimestamp = registrationTimestamp;
    }

    public boolean isDriverValid() {
        return isDriverValid;
    }

    public void setDriverValid(boolean driverValid) {
        isDriverValid = driverValid;
    }

    public LocalDate getFuelingLocalDate() {
        return registration.getFuelingDate().getDate();
    }

    public LocalDateTime getRegistrationLocalDateTime() {
        var instant = registrationTimestamp.getInstant();
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }
}
