package fleet.components.registration.corefunctions;

import fleet.aux.result.Result;

public interface DecisionResult extends Result {

    boolean isSuccess();

}
