package fleet.components.registration.corefunctions;

public enum RegistrationStatus {
    Created,
    RequestValid,
    RequestInvalid,
    AccumulatingExternalData,
    ExternalDataSuccess,
    ExternalDataFailure,
    CommandBuilt,
    CommandSuccess,
    CommandFailure,
    StoringResult,
    StorageSuccess,
    StorageFailure,
    StorageConstraintViolation,
    Notifying,
    NotificationSuccess,
    NotificationFailure,
    Success,
    Timeout
}
