package fleet.components.registration;

import akka.actor.ActorRef;
import fleet.aux.Component;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.registration.config.RegistrationContext;

public class RegistrationComponent implements Component {
    private final RegistrationConfig config;
    private final RegistrationContext context;
    private ActorRef registration;

    public RegistrationComponent(RegistrationConfig config, RegistrationContext context) {
        this.config = config;
        this.context = context;
    }

    @Override
    public void start() {
        var system = context.getActorSystem();
        registration = system.actorOf(
                RegistrationComponentSupervisor.props(config, context),
                "Registration"
        );
    }

    @Override
    public void stop() {
        var system = context.getActorSystem();
        system.stop(registration);
    }
}
