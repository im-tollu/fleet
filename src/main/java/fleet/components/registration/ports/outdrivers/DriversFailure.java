package fleet.components.registration.ports.outdrivers;

import fleet.aux.correlation.CorrelationId;
import fleet.aux.result.WithDetails;

import java.util.List;

public class DriversFailure extends DriversResponse implements WithDetails {
    private final String message;

    public DriversFailure(String message, CorrelationId correlationId) {
        super(correlationId);
        this.message = message;
    }

    @Override
    public List<String> getDetails() {
        return List.of(message);
    }

    @Override
    public boolean isSuccess() {
        return false;
    }
}
