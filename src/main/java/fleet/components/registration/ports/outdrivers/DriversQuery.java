package fleet.components.registration.ports.outdrivers;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.vocabulary.DriverId;
import fleet.vocabulary.FuelingDate;
import fleet.vocabulary.FuelingRegistration;

public class DriversQuery implements WithReturnAddress, WithCorrelationId {
    private final DriverId driverId;
    private final FuelingDate fuelingDate;
    private final CorrelationId correlationId;
    private ActorRef returnAddress;

    public DriversQuery(DriverId driverId, FuelingDate fuelingDate, CorrelationId correlationId) {
        this.driverId = driverId;
        this.fuelingDate = fuelingDate;
        this.correlationId = correlationId;
    }

    public DriversQuery(FuelingRegistration registration, CorrelationId correlationId) {
        this(registration.getDriverId(), registration.getFuelingDate(), correlationId);
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public FuelingDate getFuelingDate() {
        return fuelingDate;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return correlationId;
    }

    @Override
    public void setReturnAddress(ActorRef returnAddress) {
        this.returnAddress = returnAddress;
    }

    @Override
    public ActorRef getReturnAddress() {
        return returnAddress;
    }
}
