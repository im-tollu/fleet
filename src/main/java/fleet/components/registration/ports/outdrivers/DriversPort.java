package fleet.components.registration.ports.outdrivers;

import akka.actor.ActorRef;
import fleet.aux.actors.OutPort;

public class DriversPort extends OutPort {
    public DriversPort(ActorRef value) {
        super(value);
    }

    public void queryDriversData(DriversQuery request) {
        tell(request);
    }

}
