package fleet.components.registration.ports.outdrivers;

import fleet.aux.correlation.CorrelationId;

public class DriversSuccess extends DriversResponse {
    private final boolean driverValid;

    public DriversSuccess(boolean isDriverValid, CorrelationId correlationId) {
        super(correlationId);
        this.driverValid = isDriverValid;
    }

    public boolean isDriverValid() {
        return driverValid;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

}
