package fleet.components.registration.ports.outstorage;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.components.registration.corefunctions.RegistrationApprovedEvent;

public class StorageRequest implements WithReturnAddress {
    private final RegistrationApprovedEvent event;
    private ActorRef returnAddress;

    public StorageRequest(RegistrationApprovedEvent event) {
        this.event = event;
    }

    public RegistrationApprovedEvent getEvent() {
        return event;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return returnAddress;
    }
}
