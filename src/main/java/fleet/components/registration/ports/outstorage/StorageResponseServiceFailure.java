package fleet.components.registration.ports.outstorage;

import fleet.aux.result.WithDetails;

import java.util.List;

public class StorageResponseServiceFailure extends StorageResponse implements WithDetails {
    private final String message;

    public StorageResponseServiceFailure(String message) {
        this.message = message;
    }

    @Override
    public List<String> getDetails() {
        return List.of(message);
    }

    @Override
    public boolean isSuccess() {
        return false;
    }
}
