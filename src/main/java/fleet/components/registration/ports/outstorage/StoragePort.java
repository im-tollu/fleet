package fleet.components.registration.ports.outstorage;

import akka.actor.ActorRef;
import fleet.aux.actors.OutPort;

public class StoragePort extends OutPort {
    public StoragePort(ActorRef value) {
        super(value);
    }

    public void storeApprovedRegistration(StorageRequest request) {
        tell(request);
    }

}
