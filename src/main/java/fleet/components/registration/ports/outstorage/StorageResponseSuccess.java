package fleet.components.registration.ports.outstorage;

import fleet.vocabulary.Fueling;

public class StorageResponseSuccess extends StorageResponse {
    private final Fueling fueling;

    public StorageResponseSuccess(Fueling fueling) {
        this.fueling = fueling;
    }

    public Fueling getFueling() {
        return fueling;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }
}
