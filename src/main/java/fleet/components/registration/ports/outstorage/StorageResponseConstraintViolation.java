package fleet.components.registration.ports.outstorage;

import fleet.aux.result.WithDetails;

import java.util.List;

public class StorageResponseConstraintViolation extends StorageResponse implements WithDetails {
    private final String message;

    public StorageResponseConstraintViolation(String message) {
        this.message = message;
    }

    @Override
    public List<String> getDetails() {
        return List.of(message);
    }

    @Override
    public boolean isSuccess() {
        return false;
    }
}
