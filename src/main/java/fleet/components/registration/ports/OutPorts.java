package fleet.components.registration.ports;

import fleet.components.registration.ports.outdrivers.DriversPort;
import fleet.components.registration.ports.outeventbus.BusPort;
import fleet.components.registration.ports.outstorage.StoragePort;

public class OutPorts {
    private final DriversPort driversPort;
    private final StoragePort storagePort;
    private final BusPort eventBusPort;

    public OutPorts(
            DriversPort driversPort,
            StoragePort storagePort,
            BusPort eventBusPort
    ) {
        this.driversPort = driversPort;
        this.storagePort = storagePort;
        this.eventBusPort = eventBusPort;
    }

    public DriversPort getDriversPort() {
        return driversPort;
    }

    public StoragePort getStoragePort() {
        return storagePort;
    }

    public BusPort getEventBusPort() {
        return eventBusPort;
    }
}
