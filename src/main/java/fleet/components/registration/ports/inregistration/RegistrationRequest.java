package fleet.components.registration.ports.inregistration;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.aux.validation.ValidationError;
import fleet.aux.validation.ValidationFailure;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.Validator;
import fleet.vocabulary.FuelingRegistration;
import fleet.vocabulary.RegistrationTimestamp;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class RegistrationRequest implements Validator, WithReturnAddress, WithCorrelationId {
    private final CorrelationId correlationId;
    private final FuelingRegistration registration;
    private final RegistrationTimestamp registrationTimestamp;
    private ActorRef returnAddress;

    public RegistrationRequest(
            CorrelationId correlationId,
            FuelingRegistration registration,
            RegistrationTimestamp registrationTimestamp
    ) {
        this.correlationId = correlationId;
        this.registration = registration;
        this.registrationTimestamp = registrationTimestamp;
    }

    public FuelingRegistration getRegistration() {
        return registration;
    }

    public RegistrationTimestamp getRegistrationTimestamp() {
        return registrationTimestamp;
    }

    @Override
    public ValidationResult validate() {
        var hasNulls = Stream.of(correlationId, registration, registrationTimestamp).anyMatch(Objects::isNull);
        if (hasNulls) {
            return new ValidationFailure(List.of(new ValidationError("StorageRequest members must not be nulls")));
        } else {
            return registration.validate();
        }
    }

    @Override
    public CorrelationId getCorrelationId() {
        return correlationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationRequest request = (RegistrationRequest) o;

        if (!correlationId.equals(request.correlationId)) return false;
        if (!registration.equals(request.registration)) return false;
        return registrationTimestamp.equals(request.registrationTimestamp);
    }

    @Override
    public int hashCode() {
        int result = correlationId.hashCode();
        result = 31 * result + registration.hashCode();
        result = 31 * result + registrationTimestamp.hashCode();
        return result;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return this.returnAddress;
    }
}
