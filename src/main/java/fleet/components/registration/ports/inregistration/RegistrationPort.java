package fleet.components.registration.ports.inregistration;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;

public class RegistrationPort extends ActorRefWrapper {
    public RegistrationPort(ActorRef value) {
        super(value);
    }

    public void applyForRegistration(RegistrationRequest request) {
        tell(request);
    }

}
