package fleet.components.registration.ports.inregistration;

import java.util.List;

public class RegistrationFailureBusiness extends RegistrationFailure {
    public RegistrationFailureBusiness(RegistrationRequest request, String message, List<String> details) {
        super(request, message, details);
    }
}
