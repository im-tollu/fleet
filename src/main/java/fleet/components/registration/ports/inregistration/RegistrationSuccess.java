package fleet.components.registration.ports.inregistration;

import fleet.vocabulary.Fueling;

public class RegistrationSuccess extends RegistrationResponse {
    private final Fueling fueling;

    public RegistrationSuccess(RegistrationRequest request, Fueling fueling) {
        super(request);
        this.fueling = fueling;
    }

    public Fueling getFueling() {
        return fueling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationSuccess otherRegistration = (RegistrationSuccess) o;

        return getRequest().equals(otherRegistration.getRequest());
    }

    @Override
    public int hashCode() {
        return getRequest().hashCode();
    }
}
