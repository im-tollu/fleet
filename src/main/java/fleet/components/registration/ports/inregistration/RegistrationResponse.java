package fleet.components.registration.ports.inregistration;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;

public abstract class RegistrationResponse implements WithReturnAddress, WithCorrelationId {
    private final RegistrationRequest request;
    private ActorRef returnAddress;

    RegistrationResponse(RegistrationRequest request) {
        this.request = request;
    }

    protected RegistrationRequest getRequest() {
        return request;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return getRequest().getCorrelationId();
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return this.returnAddress;
    }
}
