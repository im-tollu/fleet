package fleet.components.registration.ports.inregistration;

import java.util.List;

abstract class RegistrationFailure extends RegistrationResponse {
    private final String message;
    private final List<String> details;

    RegistrationFailure(RegistrationRequest request, String message, List<String> details) {
        super(request);
        this.message = message;
        this.details = details;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getDetails() {
        return details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationFailure other = (RegistrationFailure) o;

        if (!getRequest().equals(other.getRequest())) return false;
        if (!getMessage().equals(other.getMessage())) return false;
        return getDetails().equals(other.getDetails());
    }

    @Override
    public int hashCode() {
        int result = getRequest().hashCode();
        result = 31 * result + getMessage().hashCode();
        result = 31 * result + getDetails().hashCode();
        return result;
    }

    @Override
    public String toString() {
        var sb = new StringBuilder()
                .append("RegistrationFailure{\n")
                .append("\tclass: {")
                .append(getClass().getCanonicalName())
                .append("}\n")
                .append("\tmessage: {")
                .append(getMessage())
                .append("}\n")
                .append("\tdetails:\n");
        for (String detail : getDetails()) {
            sb
                    .append("\t\t{")
                    .append(detail)
                    .append("}\n");
        }
        return sb.toString();
    }
}
