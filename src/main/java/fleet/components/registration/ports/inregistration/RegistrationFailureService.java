package fleet.components.registration.ports.inregistration;

import java.util.List;

public class RegistrationFailureService extends RegistrationFailure {
    public RegistrationFailureService(RegistrationRequest request, String message, List<String> details) {
        super(request, message, details);
    }
}
