package fleet.components.registration.ports;

import fleet.components.registration.ports.inregistration.RegistrationPort;
import fleet.components.registration.ports.outdrivers.DriversPort;
import fleet.components.registration.ports.outeventbus.BusPort;
import fleet.components.registration.ports.outstorage.StoragePort;

public class AllPorts {
    private final RegistrationPort registrationPort;
    private final OutPorts outPorts;

    public AllPorts(RegistrationPort registrationPort, OutPorts outPorts) {
        this.registrationPort = registrationPort;
        this.outPorts = outPorts;
    }

    public RegistrationPort getRegistrationPort() {
        return registrationPort;
    }

    public DriversPort getDriversPort() {
        return outPorts.getDriversPort();
    }

    public StoragePort getStoragePort() {
        return outPorts.getStoragePort();
    }

    public BusPort getEventBusPort() {
        return outPorts.getEventBusPort();
    }

}
