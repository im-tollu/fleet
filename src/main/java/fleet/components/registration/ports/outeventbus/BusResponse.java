package fleet.components.registration.ports.outeventbus;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.result.Result;

public abstract class BusResponse implements Result, WithReturnAddress {
    private ActorRef returnAddress;

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return this.returnAddress;
    }
}
