package fleet.components.registration.ports.outeventbus;

import fleet.aux.result.WithDetails;

import java.util.List;

public class BusResponseFailure extends BusResponse implements WithDetails {
    private final String message;

    public BusResponseFailure(String message) {
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public List<String> getDetails() {
        return List.of(message);
    }
}
