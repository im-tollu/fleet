package fleet.components.registration.ports.outeventbus;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.vocabulary.Fueling;

public class BusRequest implements WithReturnAddress, WithCorrelationId {
    private final Fueling fueling;
    private final CorrelationId correlationId;
    private ActorRef returnAddress;

    public BusRequest(Fueling fueling, CorrelationId correlationId) {
        this.fueling = fueling;
        this.correlationId = correlationId;
    }

    public Fueling getFueling() {
        return fueling;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return returnAddress;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return this.correlationId;
    }
}
