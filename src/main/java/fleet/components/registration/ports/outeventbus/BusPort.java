package fleet.components.registration.ports.outeventbus;

import akka.actor.ActorRef;
import fleet.aux.actors.OutPort;

public class BusPort extends OutPort {
    public BusPort(ActorRef value) {
        super(value);
    }

    public void publishEvent(BusRequest eventBusRequest) {
        tell(eventBusRequest);
    }

}
