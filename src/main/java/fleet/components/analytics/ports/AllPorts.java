package fleet.components.analytics.ports;

import fleet.components.analytics.ports.indatamart.DataMartPort;
import fleet.components.analytics.ports.inevents.EventsPort;
import fleet.components.analytics.ports.outdrivers.DriversPort;
import fleet.components.analytics.ports.outstorage.StoragePort;

public class AllPorts {
    private final DataMartPort dataMartPort;
    private final CorePorts corePorts;

    public AllPorts(DataMartPort dataMartPort, CorePorts corePorts) {
        this.dataMartPort = dataMartPort;
        this.corePorts = corePorts;
    }

    public DataMartPort getDataMartPort() {
        return dataMartPort;
    }

    public EventsPort getEventsPort() {
        return corePorts.getEventsPort();
    }

    public DriversPort getDriversPort() {
        return corePorts.getDriversPort();
    }

    public StoragePort getStoragePort() {
        return corePorts.getStoragePort();
    }

}
