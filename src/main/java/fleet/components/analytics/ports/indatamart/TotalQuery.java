package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.DriverId;

import java.util.Optional;

public class TotalQuery {
    private final Optional<DriverId> filterByDriver;

    public TotalQuery(Optional<DriverId> filterByDriver) {
        this.filterByDriver = filterByDriver;
    }

    public Optional<DriverId> getFilterByDriver() {
        return filterByDriver;
    }
}
