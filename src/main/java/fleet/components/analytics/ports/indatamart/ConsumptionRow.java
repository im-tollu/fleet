package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.*;

public class ConsumptionRow {
    private final Fuel fuel;
    private final Volume volume;
    private final FuelingDate fuelingDate;
    private final Price price;
    private final Price total_price;
    private final DriverId driverId;

    public ConsumptionRow(Fuel fuel, Volume volume, FuelingDate fuelingDate, Price price, Price total_price, DriverId driverId) {
        this.fuel = fuel;
        this.volume = volume;
        this.fuelingDate = fuelingDate;
        this.price = price;
        this.total_price = total_price;
        this.driverId = driverId;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public Volume getVolume() {
        return volume;
    }

    public FuelingDate getFuelingDate() {
        return fuelingDate;
    }

    public Price getPrice() {
        return price;
    }

    public Price getTotalPrice() {
        return total_price;
    }

    public DriverId getDriverId() {
        return driverId;
    }
}
