package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.Fueling;
import fleet.vocabulary.FuelingMonth;
import fleet.vocabulary.Price;

import java.time.YearMonth;

public class TotalRow {
    private final FuelingMonth fuelingMonth;
    private final Price total_price;

    public TotalRow(FuelingMonth fuelingMonth, Price total_price) {
        this.fuelingMonth = fuelingMonth;
        this.total_price = total_price;
    }

    public FuelingMonth getFuelingMonth() {
        return fuelingMonth;
    }

    public Price getTotal_price() {
        return total_price;
    }
}
