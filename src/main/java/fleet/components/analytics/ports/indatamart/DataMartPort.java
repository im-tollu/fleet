package fleet.components.analytics.ports.indatamart;

import akka.NotUsed;
import akka.stream.javadsl.Source;

public interface DataMartPort {

    Source<ConsumptionRow, NotUsed> retrieveFuelConsumption(ConsumptionQuery query);

    Source<StatisticsRow, NotUsed> retrieveStatistics(StatisticsQuery query);

    Source<TotalRow, NotUsed> retrieveTotalSpentAmount(TotalQuery query);
}
