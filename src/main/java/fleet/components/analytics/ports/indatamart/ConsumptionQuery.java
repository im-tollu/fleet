package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.DriverId;

import java.time.YearMonth;
import java.util.Optional;

public class ConsumptionQuery {
    private final YearMonth yearMonth;
    private final Optional<DriverId> filterByDriver;

    public ConsumptionQuery(YearMonth yearMonth, Optional<DriverId> filterByDriver) {
        this.yearMonth = yearMonth;
        this.filterByDriver = filterByDriver;
    }

    public YearMonth getYearMonth() {
        return yearMonth;
    }

    public Optional<DriverId> getFilterByDriver() {
        return filterByDriver;
    }
}
