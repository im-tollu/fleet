package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.Fuel;
import fleet.vocabulary.FuelingMonth;
import fleet.vocabulary.Price;
import fleet.vocabulary.Volume;

public class StatisticsRow {
    private final FuelingMonth fuelingMonth;
    private final Fuel fuelType;
    private final Volume volume;
    private final Price averagePrice;
    private final Price totalPrice;

    public StatisticsRow(FuelingMonth fuelingMonth, Fuel fuelType, Volume volume, Price averagePrice, Price totalPrice) {
        this.fuelingMonth = fuelingMonth;
        this.fuelType = fuelType;
        this.volume = volume;
        this.averagePrice = averagePrice;
        this.totalPrice = totalPrice;
    }

    public FuelingMonth getFuelingMonth() {
        return fuelingMonth;
    }

    public Fuel getFuelType() {
        return fuelType;
    }

    public Volume getVolume() {
        return volume;
    }

    public Price getAveragePrice() {
        return averagePrice;
    }

    public Price getTotalPrice() {
        return totalPrice;
    }
}
