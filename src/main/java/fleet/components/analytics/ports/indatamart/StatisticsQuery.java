package fleet.components.analytics.ports.indatamart;

import fleet.vocabulary.DriverId;

import java.util.Optional;

public class StatisticsQuery {
    private final Optional<DriverId> filterByDriver;

    public StatisticsQuery(Optional<DriverId> filterByDriver) {
        this.filterByDriver = filterByDriver;
    }

    public Optional<DriverId> getFilterByDriver() {
        return filterByDriver;
    }
}
