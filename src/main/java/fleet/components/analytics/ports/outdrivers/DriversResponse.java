package fleet.components.analytics.ports.outdrivers;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.aux.result.Result;

public abstract class DriversResponse implements Result, WithReturnAddress, WithCorrelationId {
    private final CorrelationId correlationId;
    private ActorRef returnAddress;

    protected DriversResponse(CorrelationId correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return correlationId;
    }

    @Override
    public ActorRef getReturnAddress() {
        return returnAddress;
    }
}
