package fleet.components.analytics.ports.outdrivers;

import fleet.aux.correlation.CorrelationId;
import fleet.vocabulary.Driver;

public class DriversSuccess extends DriversResponse{
    private final Driver driver;

    public DriversSuccess(Driver driver, CorrelationId correlationId) {
        super(correlationId);
        this.driver = driver;
    }

    public Driver getDriver() {
        return driver;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }
}
