package fleet.components.analytics.ports.outdrivers;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.vocabulary.DriverId;

public class DriversQuery implements WithReturnAddress, WithCorrelationId {
    private final DriverId driverId;
    private final CorrelationId correlationId;
    private ActorRef returnAddress;

    public DriversQuery(DriverId driverId, CorrelationId correlationId) {
        this.driverId = driverId;
        this.correlationId = correlationId;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return this.returnAddress;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return this.correlationId;
    }
}
