package fleet.components.analytics.ports.outdrivers;

import fleet.aux.correlation.CorrelationId;

public class DriversFailure extends DriversResponse{
    private final String message;

    public DriversFailure(CorrelationId correlationId, String message) {
        super(correlationId);
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    public String getMessage() {
        return message;
    }
}
