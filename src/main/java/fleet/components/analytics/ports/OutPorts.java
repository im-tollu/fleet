package fleet.components.analytics.ports;

import fleet.components.analytics.ports.outdrivers.DriversPort;
import fleet.components.analytics.ports.outstorage.StoragePort;

public class OutPorts {
    private final DriversPort driversPort;
    private final StoragePort storagePort;

    public OutPorts(DriversPort driversPort, StoragePort storagePort) {
        this.driversPort = driversPort;
        this.storagePort = storagePort;
    }

    public DriversPort getDriversPort() {
        return driversPort;
    }

    public StoragePort getStoragePort() {
        return storagePort;
    }
}
