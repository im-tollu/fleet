package fleet.components.analytics.ports.outstorage;

import akka.actor.ActorRef;
import fleet.aux.actors.WithReturnAddress;
import fleet.components.analytics.corefunctions.TransformedData;

public class StorageRequest implements WithReturnAddress {
    private final TransformedData data;
    private ActorRef returnAddress;

    public StorageRequest(TransformedData data) {
        this.data = data;
    }

    public TransformedData getData() {
        return data;
    }

    @Override
    public void setReturnAddress(ActorRef address) {
        this.returnAddress = address;
    }

    @Override
    public ActorRef getReturnAddress() {
        return this.returnAddress;
    }
}
