package fleet.components.analytics.ports.outstorage;

public class StorageFailure extends StorageResponse{
    @Override
    public boolean isSuccess() {
        return false;
    }
}
