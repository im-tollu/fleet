package fleet.components.analytics.ports;

import fleet.components.analytics.ports.indatamart.DataMartPort;
import fleet.components.analytics.ports.inevents.EventsPort;
import fleet.components.analytics.ports.outdrivers.DriversPort;
import fleet.components.analytics.ports.outstorage.StoragePort;

public class CorePorts {
    private final EventsPort eventsPort;
    private final OutPorts outPorts;

    public CorePorts(EventsPort eventsPort, OutPorts outPorts) {
        this.eventsPort = eventsPort;
        this.outPorts = outPorts;
    }

    public EventsPort getEventsPort() {
        return eventsPort;
    }

    public DriversPort getDriversPort() {
        return outPorts.getDriversPort();
    }

    public StoragePort getStoragePort() {
        return outPorts.getStoragePort();
    }

}
