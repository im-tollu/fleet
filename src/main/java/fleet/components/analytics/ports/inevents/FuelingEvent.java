package fleet.components.analytics.ports.inevents;

import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;
import fleet.vocabulary.Fueling;

public class FuelingEvent implements WithCorrelationId {
    private final Fueling fueling;
    private final CorrelationId correlationId;

    public FuelingEvent(Fueling fueling, CorrelationId correlationId) {
        this.fueling = fueling;
        this.correlationId = correlationId;
    }

    public Fueling getFueling() {
        return fueling;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return this.correlationId;
    }
}
