package fleet.components.analytics.ports.inevents;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;

public class EventsPort extends ActorRefWrapper {
    public EventsPort(ActorRef value) {
        super(value);
    }

    public void receiveEvent(FuelingEvent fuelingEvent) {
        tell(fuelingEvent);
    }
}
