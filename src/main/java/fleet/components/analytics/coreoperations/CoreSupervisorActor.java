package fleet.components.analytics.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.Props;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.analytics.ports.CorePorts;
import fleet.components.analytics.ports.OutPorts;
import fleet.components.analytics.ports.inevents.EventsPort;
import fleet.components.analytics.ports.outdrivers.DriversPort;
import fleet.components.analytics.ports.outstorage.StoragePort;

import static fleet.components.analytics.coreoperations.CoreSupervisorRef.GET_CORE_PORTS;

public class CoreSupervisorActor extends AbstractActor {
    public static Props props(AnalyticsConfig config) {
        return Props.create(CoreSupervisorActor.class, () -> new CoreSupervisorActor(config));
    }

    private CoreSupervisorActor(AnalyticsConfig config) {
        this.config = config;
    }

    private final AnalyticsConfig config;

    private CorePorts corePorts;

    @Override
    public void preStart() {
        var driversPort = new DriversPort(getContext().actorOf(DriversPortActor.props(), "DriversPort"));
        var storagePort = new StoragePort(getContext().actorOf(StoragePortActor.props(), "StoragePort"));
        var outPorts = new OutPorts(driversPort, storagePort);
        var eventsPort = new EventsPort(getContext().actorOf(EventsPortActor.props(config, outPorts), "EventsPort"));
        this.corePorts = new CorePorts(eventsPort, outPorts);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals(GET_CORE_PORTS, (msg) -> receiveGetCorePorts())
                .build();
    }

    private void receiveGetCorePorts() {
        getSender().tell(this.corePorts, getSelf());
    }

}
