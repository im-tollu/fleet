package fleet.components.analytics.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.Props;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.analytics.ports.OutPorts;
import fleet.components.analytics.ports.inevents.FuelingEvent;

public class EventsPortActor extends AbstractActor {
    public static Props props(AnalyticsConfig config, OutPorts outPorts) {
        return Props.create(EventsPortActor.class, () -> new EventsPortActor(config, outPorts));
    }

    private EventsPortActor(AnalyticsConfig config, OutPorts outPorts) {
        this.config = config;
        this.outPorts = outPorts;
    }

    private final AnalyticsConfig config;
    private final OutPorts outPorts;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FuelingEvent.class, this::receiveFuelingEvent)
                .build();
    }

    private void receiveFuelingEvent(FuelingEvent event) {
        var workerProps = EtlWorkerActor.props(event, config, outPorts);
        getContext().actorOf(workerProps);
    }
}
