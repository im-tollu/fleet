package fleet.components.analytics.coreoperations;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import fleet.aux.actors.OutPort;
import fleet.components.analytics.ports.outstorage.StorageRequest;

public class StoragePortActor extends AbstractActor {
    public static Props props() {
        return Props.create(StoragePortActor.class, () -> new StoragePortActor());
    }

    public StoragePortActor() {
    }

    private ActorRef adapter = getContext().getSystem().deadLetters();

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(OutPort.AdapterPlug.class, plug -> this.adapter = plug.getValue())
                .match(StorageRequest.class, request -> adapter.forward(request, getContext()))
                .build();
    }
}
