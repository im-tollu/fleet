package fleet.components.analytics.coreoperations;

import akka.actor.AbstractActorWithTimers;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import fleet.aux.actors.TimeoutNotification;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.analytics.corefunctions.EtlWorkflow;
import fleet.components.analytics.corefunctions.Transform;
import fleet.components.analytics.ports.OutPorts;
import fleet.components.analytics.ports.inevents.FuelingEvent;
import fleet.components.analytics.ports.outdrivers.DriversResponse;
import fleet.components.analytics.ports.outstorage.StorageResponse;

public class EtlWorkerActor extends AbstractActorWithTimers {
    public static Props props(
            FuelingEvent fuelingEvent,
            AnalyticsConfig config,
            OutPorts outPorts
    ) {
        return Props.create(EtlWorkerActor.class, () -> new EtlWorkerActor(fuelingEvent, config, outPorts));
    }

    private final LoggingAdapter log;

    private final AnalyticsConfig config;
    private final EtlWorkflow workflow;
    private final Transform transform;
    private final OutPorts outPorts;

    private EtlWorkerActor(FuelingEvent fuelingEvent, AnalyticsConfig config, OutPorts outPorts) {
        this.config = config;
        this.outPorts = outPorts;
        this.workflow = new EtlWorkflow(fuelingEvent);
        this.transform = new Transform();
        this.log = Logging.getLogger(getContext().getSystem(), this);
    }

    @Override
    public void preStart() {
        setProcessingTimeout();
        proceedWithExtractData();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(TimeoutNotification.class, timeout -> this.receiveTimeout())
                .match(DriversResponse.class, this::receiveExternalData)
                .match(StorageResponse.class, this::receiveStorageResponse)
                .build();
    }

    private void setProcessingTimeout() {
        var timeout = config.getProcessingTimeout();
        var notification = new TimeoutNotification("EtlWorkerTimeout");
        getTimers().startSingleTimer(notification.getKey(), notification, timeout);
    }

    private void proceedWithExtractData() {
        workflow.accumulateExternalData();
        var drivers = outPorts.getDriversPort();
        var query = workflow.getDriversQuery();
        query.setReturnAddress(getSelf());
        drivers.queryDriversData(query);
    }

    private void receiveExternalData(DriversResponse response) {
        workflow.acceptExternalData(response);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case ExtractingData:
                break;
            case ExtractSuccess:
                proceedWithTransform();
                break;
            case ExtractFailure:
                finishWithEtlFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void proceedWithTransform() {
        workflow.buildTransformCommand();
        var command = workflow.getCommand();
        var transformedData = transform.transform(command);
        workflow.acceptTransformedData(transformedData);
        proceedWithDataLoad();
    }

    private void proceedWithDataLoad() {
        workflow.loadData();
        var storage = outPorts.getStoragePort();
        var request = workflow.getStorageRequest();
        request.setReturnAddress(getSelf());
        storage.storeData(request);
    }

    private void receiveStorageResponse(StorageResponse response) {
        workflow.acceptLoadResult(response);
        var workflowStatus = workflow.getStatus();
        switch (workflowStatus) {
            case LoadSuccess:
                finishWithEtlSuccess();
                break;
            case LoadFailure:
                finishWithEtlFailure();
                break;
            default:
                throw new IllegalStateException(workflowStatus.name());
        }
    }

    private void receiveTimeout() {
        workflow.acceptTimeout();
        finishWithEtlFailure();
    }

    private void finishWithEtlFailure() {
        log.error(workflow.getEtlLogEntry());
        stop();
    }

    private void finishWithEtlSuccess() {
        workflow.acceptSuccess();
        log.info(workflow.getEtlLogEntry());
        stop();
    }

    private void stop() {
        getContext().stop(getSelf());
    }
}
