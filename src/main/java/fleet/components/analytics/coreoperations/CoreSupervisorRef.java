package fleet.components.analytics.coreoperations;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;
import fleet.components.analytics.ports.CorePorts;

public class CoreSupervisorRef extends ActorRefWrapper {
    public static final String GET_CORE_PORTS = "Get core ports";

    public CoreSupervisorRef(ActorRef value) {
        super(value);
    }

    public CorePorts getCorePorts() {
        return (CorePorts) ask(GET_CORE_PORTS, 1000L);
    }
}
