package fleet.components.analytics.config;

import akka.util.Timeout;

public class AnalyticsWebServerConfig {
    private final String host;
    private final int port;
    private final Timeout timeout;

    public AnalyticsWebServerConfig(String host, int port, Timeout timeout) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public Timeout getTimeout() {
        return timeout;
    }

    public static AnalyticsWebServerConfig fromAnalyticsConfig(AnalyticsConfig config) {
        // Processing timeout should be handled by the business core.
        // That's why for web server we set twice processing timeout - it should never be reached.
        var timeout = Timeout.create(config.getProcessingTimeout().multipliedBy(2));
        return new AnalyticsWebServerConfig(config.getHost(), config.getPort(), timeout);
    }
}
