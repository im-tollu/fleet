package fleet.components.analytics.config;

import java.time.Duration;

public class AnalyticsConfig {
    private final Duration processingTimeout;
    private final String host;
    private final int port;

    public AnalyticsConfig(Duration processingTimeout, String host, int port) {
        this.processingTimeout = processingTimeout;
        this.host = host;
        this.port = port;
    }

    public Duration getProcessingTimeout() {
        return processingTimeout;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
