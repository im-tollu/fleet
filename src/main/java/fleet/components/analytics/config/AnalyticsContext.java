package fleet.components.analytics.config;

import akka.actor.ActorSystem;
import fleet.components.eventbus.EventBusRef;

import javax.sql.DataSource;

public class AnalyticsContext {
    private final ActorSystem actorSystem;
    private final DataSource dataSource;
    private final EventBusRef eventBus;

    public AnalyticsContext(ActorSystem actorSystem, DataSource dataSource, EventBusRef eventBus) {
        this.actorSystem = actorSystem;
        this.dataSource = dataSource;
        this.eventBus = eventBus;
    }

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public EventBusRef getEventBus() {
        return eventBus;
    }
}
