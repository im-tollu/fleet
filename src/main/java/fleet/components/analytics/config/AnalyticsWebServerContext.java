package fleet.components.analytics.config;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import fleet.components.analytics.ports.indatamart.DataMartPort;

public class AnalyticsWebServerContext {
    private final DataMartPort dataMartPort;
    private final ActorSystem actorSystem;
    private final Materializer materializer;

    public AnalyticsWebServerContext(DataMartPort dataMartPort, ActorSystem actorSystem, Materializer materializer) {
        this.dataMartPort = dataMartPort;
        this.actorSystem = actorSystem;
        this.materializer = materializer;
    }

    public DataMartPort getDataMartPort() {
        return dataMartPort;
    }

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public Materializer getMaterializer() {
        return materializer;
    }
}
