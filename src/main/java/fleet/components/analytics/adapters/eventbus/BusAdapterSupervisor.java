package fleet.components.analytics.adapters.eventbus;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import fleet.components.analytics.ports.inevents.EventsPort;
import fleet.components.eventbus.EventBusRef;
import fleet.components.eventbus.Message;
import fleet.components.eventbus.Subscriber;

public class BusAdapterSupervisor extends AbstractActor {
    public static Props props(EventBusRef eventBus, EventsPort eventsPort) {
        return Props.create(BusAdapterSupervisor.class, () -> new BusAdapterSupervisor(eventBus, eventsPort));
    }

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private final EventBusRef eventBus;
    private final EventsPort eventsPort;

    private BusAdapterSupervisor(EventBusRef eventBus, EventsPort eventsPort) {
        this.eventBus = eventBus;
        this.eventsPort = eventsPort;
    }

    @Override
    public void preStart() {
        var meSubscriber = new Subscriber(getSelf());
        eventBus.subscribe(meSubscriber);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Message.class, this::receiveMessage)
                .build();
    }

    private void receiveMessage(Message message){
        try {
            var fuelingEvent = BusMessageConverter.convert(message);
            eventsPort.receiveEvent(fuelingEvent);
        } catch (IllegalArgumentException e) {
            finishWithInvalidPayload(e.getMessage());
        }
    }

    private void finishWithInvalidPayload(String errorMessage) {
        log.error(errorMessage);
    }
}
