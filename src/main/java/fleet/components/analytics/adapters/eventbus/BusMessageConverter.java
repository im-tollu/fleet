package fleet.components.analytics.adapters.eventbus;

import fleet.components.analytics.ports.inevents.FuelingEvent;
import fleet.components.eventbus.Message;
import fleet.vocabulary.Fueling;

class BusMessageConverter {
    static FuelingEvent convert(Message message) {
        var payload = message.getPayload();
        var isValidPayload = payload instanceof Fueling;
        if (isValidPayload) {
            return new FuelingEvent((Fueling)payload, message.getCorrelationId());
        } else {
            var errMessage = String.format(
                    "Expected payload of class fleet.vocabulary.Fueling. Got %s instead.",
                    payload.getClass().getCanonicalName()
                    );
            throw new IllegalArgumentException(errMessage);
        }
    }
}
