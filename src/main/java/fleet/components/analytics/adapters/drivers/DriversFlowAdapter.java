package fleet.components.analytics.adapters.drivers;

import akka.Done;
import akka.actor.ActorRef;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import fleet.aux.actors.WithReturnAddress;
import fleet.components.analytics.ports.outdrivers.DriversQuery;
import fleet.components.analytics.ports.outdrivers.DriversResponse;

import javax.sql.DataSource;
import java.util.concurrent.CompletionStage;

public class DriversFlowAdapter {
    private final int bufferSize;
    private final DataSource dataSource;

    public DriversFlowAdapter(int bufferSize, DataSource dataSource) {
        this.bufferSize = bufferSize;
        this.dataSource = dataSource;
    }

    public RunnableGraph<ActorRef> getRunnableGraph() {
        Source<DriversQuery, ActorRef> source = Source.actorRef(bufferSize, OverflowStrategy.dropHead());
        Sink<DriversResponse, CompletionStage<Done>> sink = Sink.foreach(WithReturnAddress::sendBack);
        var driversMapper = new DriversJdbcMapper(dataSource);
        return source
                .mapAsync(bufferSize, driversMapper)
                .toMat(sink, Keep.left());
    }
}
