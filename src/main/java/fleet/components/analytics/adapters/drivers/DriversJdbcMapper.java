package fleet.components.analytics.adapters.drivers;

import akka.japi.function.Function;
import fleet.components.analytics.ports.outdrivers.DriversFailure;
import fleet.components.analytics.ports.outdrivers.DriversQuery;
import fleet.components.analytics.ports.outdrivers.DriversResponse;
import fleet.components.analytics.ports.outdrivers.DriversSuccess;
import fleet.vocabulary.Driver;
import fleet.vocabulary.DriverId;
import fleet.vocabulary.DriverName;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class DriversJdbcMapper implements Function<DriversQuery, CompletionStage<DriversResponse>> {
    private static final String LOOKUP_DRIVER = "" +
            "SELECT                         \n" +
            "   driver_id as driver_id,     \n" +
            "   driver_name as driver_name  \n" +
            "FROM                           \n" +
            "   drivers.driver              \n" +
            "WHERE                          \n" +
            "   driver_id = ?               \n" +
            ";                              \n";
    private final DataSource dataSource;

    DriversJdbcMapper(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public CompletionStage<DriversResponse> apply(DriversQuery query) {
        return CompletableFuture.supplyAsync(() -> applySync(query));
    }

    private DriversResponse applySync(DriversQuery query) {
        try {
            var maybeDriver = lookupDriver(query.getDriverId());
            return maybeDriver
                    .map(driver -> buildSuccessResponse(driver, query))
                    .orElseGet(() -> buildServiceFailureResponse("Driver not found", query));
        } catch (Throwable e) {
            e.printStackTrace();
            return buildServiceFailureResponse("Could not execute database query", query);
        }
    }

    private Optional<Driver> lookupDriver(DriverId driverId) throws SQLException {
        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement lookup = countDriver(conn, driverId);
                ResultSet resultSet = lookup.getResultSet()
        ) {
            if (resultSet.next()) {
                return Optional.of(parseRow(resultSet));
            } else {
                return Optional.empty();
            }
        }
    }

    private Driver parseRow(ResultSet resultSet) throws SQLException {
        var driverId = DriverId.fromUuid((UUID) resultSet.getObject("driver_id"));
        var driverName = DriverName.wrap(resultSet.getString("driver_name"));
        return new Driver(driverId, driverName);
    }

    private PreparedStatement countDriver(Connection conn, DriverId driverId) throws SQLException {
        var statement = conn.prepareStatement(LOOKUP_DRIVER);
        statement.setObject(1, driverId.getUuid());
        statement.executeQuery();
        return statement;
    }

    private DriversResponse buildSuccessResponse(Driver driver, DriversQuery query) {
        var response = new DriversSuccess(driver, query.getCorrelationId());
        response.setReturnAddress(query.getReturnAddress());
        return response;
    }

    private DriversResponse buildServiceFailureResponse(String message, DriversQuery query) {
        var response = new DriversFailure(query.getCorrelationId(), message);
        response.setReturnAddress(query.getReturnAddress());
        return response;
    }
}
