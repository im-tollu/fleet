package fleet.components.analytics.adapters.drivers;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import fleet.components.analytics.ports.outdrivers.DriversQuery;

import javax.sql.DataSource;

public class DriversAdapterSupervisor extends AbstractActor {
    public static Props props(DataSource dataSource) {
        return Props.create(DriversAdapterSupervisor.class, () -> new DriversAdapterSupervisor(dataSource));
    }

    private DriversAdapterSupervisor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final DataSource dataSource;
    private ActorRef driversFlowSource;

    @Override
    public void preStart() {
        this.driversFlowSource = materializeDriversFlow();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(DriversQuery.class, this::receiveRequest)
                .build();
    }

    private void receiveRequest(DriversQuery request) {
        driversFlowSource.forward(request, getContext());
    }

    private ActorRef materializeDriversFlow() {
        var serviceAdapter = new DriversFlowAdapter(100, dataSource);
        var driversFlow = serviceAdapter.getRunnableGraph();
        var materializer = ActorMaterializer.create(getContext());
        return driversFlow.run(materializer);
    }
}