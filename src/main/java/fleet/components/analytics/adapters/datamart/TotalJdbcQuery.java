package fleet.components.analytics.adapters.datamart;

import fleet.aux.ThrowingFunction;
import fleet.aux.ThrowingSupplier;
import fleet.components.analytics.ports.indatamart.TotalQuery;
import fleet.components.analytics.ports.indatamart.TotalRow;
import fleet.vocabulary.DriverId;
import fleet.vocabulary.FuelingMonth;
import fleet.vocabulary.Price;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;

public class TotalJdbcQuery extends ClosableJdbcQuery<TotalRow> {
    private static final String SELECT = "" +
            "SELECT                                             \n" +
            "   date.year as year,                              \n" +
            "   date.month as month,                            \n" +
            "   sum(                                            \n" +
            "       cast(fact.price as DECIMAL)                 \n" +
            "       * cast(fact.volume as DECIMAL)              \n" +
            "   ) as total_price                                \n";
    private static final String FROM = "" +
            "FROM                                               \n" +
            "   analytics.fueling_fact fact                     \n" +
            "   JOIN analytics.date_dim date                    \n" +
            "       ON fact.date_sk = date.date_sk              \n" +
            "   JOIN analytics.driver_dim driver                \n" +
            "       ON fact.driver_sk = driver.driver_sk        \n" +
            "   JOIN analytics.fuel_dim fuel                    \n" +
            "       ON fact.fuel_sk = fuel.fuel_sk              \n";
    private static final String WHERE = "" +
            "WHERE                                              \n" +
            "   driver.driver_id = ?                            \n";
    private static final String GROUP_BY = "" +
            "GROUP BY                                           \n" +
            "   year,                                           \n" +
            "   month                                           \n";
    private static final String ORDER_BY = "" +
            "ORDER BY                                           \n" +
            "   year ASC,                                       \n" +
            "   month ASC                                       \n";
    private static final String END = ";                        \n";

    private final TotalQuery query;

    TotalJdbcQuery(DataSource dataSource, TotalQuery query) {
        super(dataSource);
        this.query = query;
        init();
    }

    @Override
    protected PreparedStatement prepareStatement(Connection connection) {
        var selectSpecificDriver =
                ThrowingFunction.wrap((DriverId driverId) -> prepareForSpecificDriver(connection, driverId));
        var selectAllDrivers =
                ThrowingSupplier.wrap(() -> prepareForAllDrivers(connection));
        return query.getFilterByDriver()
                .map(selectSpecificDriver)
                .orElseGet(selectAllDrivers);
    }

    private PreparedStatement prepareForSpecificDriver(
            Connection connection,
            DriverId driverId
    ) throws SQLException {
        var sql = SELECT + FROM + WHERE + GROUP_BY + ORDER_BY + END;
        var statement = connection.prepareStatement(sql);
        statement.setString(1, driverId.getValue());
        return statement;
    }

    private PreparedStatement prepareForAllDrivers(
            Connection connection
    ) throws SQLException {
        var sql = SELECT + FROM + GROUP_BY + ORDER_BY + END;
        return connection.prepareStatement(sql);
    }

    @Override
    protected TotalRow parseRow(ResultSet resultSet) throws SQLException {
        var year = resultSet.getInt("year");
        var month = resultSet.getInt("month");
        var fuelingMonth = new FuelingMonth(YearMonth.of(year, month));
        var total_price = new Price(resultSet.getBigDecimal("total_price"));
        return new TotalRow(fuelingMonth, total_price);
    }
}
