package fleet.components.analytics.adapters.datamart;

import fleet.aux.ThrowingFunction;
import fleet.aux.ThrowingSupplier;
import fleet.components.analytics.ports.indatamart.ConsumptionQuery;
import fleet.components.analytics.ports.indatamart.ConsumptionRow;
import fleet.vocabulary.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;

public class ConsumptionJdbcQuery extends ClosableJdbcQuery<ConsumptionRow> {
    private static final String SELECT = "" +
            "SELECT                                             \n" +
            "   date.year as year,                              \n" +
            "   date.month as month,                            \n" +
            "   date.date as date,                              \n" +
            "   fuel.fuel_type as fuel_type,                    \n" +
            "   driver.driver_id as driver_id,                  \n" +
            "   fact.volume as volume,                          \n" +
            "   fact.price as price,                            \n" +
            "   fact.price * fact.volume as total_price         \n";
    private static final String FROM = "" +
            "FROM                                               \n" +
            "   analytics.fueling_fact fact                     \n" +
            "   JOIN analytics.date_dim date                    \n" +
            "       ON fact.date_sk = date.date_sk              \n" +
            "   JOIN analytics.driver_dim driver                \n" +
            "       ON fact.driver_sk = driver.driver_sk        \n" +
            "   JOIN analytics.fuel_dim fuel                    \n" +
            "       ON fact.fuel_sk = fuel.fuel_sk              \n";
    private static final String WHERE = "" +
            "WHERE                                              \n" +
            "   date.year = ?                                   \n" +
            "   AND                                             \n" +
            "   date.month = ?                                  \n";
    private static final String WHERE_DRIVER = "" +
            "   AND                                             \n" +
            "   driver.driver_id = ?                            \n";
    private static final String ORDER_BY = "" +
            "ORDER BY                                           \n" +
            "   date.date ASC                                   \n";
    private static final String END = "" +
            ";                                                  \n";

    private final ConsumptionQuery query;

    ConsumptionJdbcQuery(DataSource dataSource, ConsumptionQuery query) {
        super(dataSource);
        this.query = query;
        init();
    }

    @Override
    protected PreparedStatement prepareStatement(Connection connection) {
        var selectSpecificDriver =
                ThrowingFunction.wrap((DriverId driverId) -> prepareForSpecificDriver(connection, driverId));
        var selectAllDrivers =
                ThrowingSupplier.wrap(() -> prepareForAllDrivers(connection));
        return query.getFilterByDriver()
                .map(selectSpecificDriver)
                .orElseGet(selectAllDrivers);
    }

    private PreparedStatement prepareForSpecificDriver(
            Connection connection,
            DriverId driverId
    ) throws SQLException {
        var sql = SELECT + FROM + WHERE + WHERE_DRIVER + ORDER_BY + END;
        var statement = connection.prepareStatement(sql);
        setYearMonth(statement, query.getYearMonth());
        statement.setString(3, driverId.getValue());
        return statement;
    }

    private PreparedStatement prepareForAllDrivers(
            Connection connection
    ) throws SQLException {
        var sql = SELECT + FROM + WHERE + ORDER_BY + END;
        var statement = connection.prepareStatement(sql);
        setYearMonth(statement, query.getYearMonth());
        return statement;
    }

    private void setYearMonth(PreparedStatement statement, YearMonth yearMonth) throws SQLException {
        statement.setInt(1, yearMonth.getYear());
        statement.setInt(2, yearMonth.getMonthValue());
    }

    @Override
    protected ConsumptionRow parseRow(ResultSet resultSet) throws SQLException {
        var date = resultSet.getDate("date");
        var fuelingDate = FuelingDate.fromLocalDate(date.toLocalDate());
        var fuel = Fuel.wrap(resultSet.getString("fuel_type"));
        var volume = new Volume(resultSet.getBigDecimal("volume"));
        var price = new Price(resultSet.getBigDecimal("price"));
        var total_price = new Price(resultSet.getBigDecimal("total_price"));
        var driverId = DriverId.wrap(resultSet.getString("driver_id"));
        return new ConsumptionRow(fuel, volume, fuelingDate, price, total_price, driverId);
    }
}
