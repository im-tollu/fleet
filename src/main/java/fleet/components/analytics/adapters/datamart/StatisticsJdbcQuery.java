package fleet.components.analytics.adapters.datamart;

import fleet.aux.ThrowingFunction;
import fleet.aux.ThrowingSupplier;
import fleet.components.analytics.ports.indatamart.StatisticsQuery;
import fleet.components.analytics.ports.indatamart.StatisticsRow;
import fleet.vocabulary.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;

public class StatisticsJdbcQuery extends ClosableJdbcQuery<StatisticsRow> {

    private static final String SELECT = "" +
            "SELECT                                                         \n" +
            "   date.year as year,                                          \n" +
            "   date.month as month,                                        \n" +
            "   fuel.fuel_type as fuel_type,                                \n" +
            "   fact.volume as volume,                                      \n" +
            "   avg(fact.price) as average_price,                           \n" +
            "   sum(fact.price * fact.volume) as total_price                \n";
    private static final String FROM = "" +
            "FROM                                                           \n" +
            "   analytics.fueling_fact fact                                 \n" +
            "   JOIN analytics.date_dim date                                \n" +
            "       ON fact.date_sk = date.date_sk                          \n" +
            "   JOIN analytics.driver_dim driver                            \n" +
            "       ON fact.driver_sk = driver.driver_sk                    \n" +
            "   JOIN analytics.fuel_dim fuel                                \n" +
            "       ON fact.fuel_sk = fuel.fuel_sk                          \n";
    private static final String WHERE = "" +
            "WHERE                                                          \n" +
            "   driver.driver_id = ?                                        \n";
    private static final String GROUP_BY = "" +
            "GROUP BY                                                       \n" +
            "   year,                                                       \n" +
            "   month,                                                      \n" +
            "   fuel_type                                                   \n";
    private static final String ORDER_BY = "" +
            "ORDER BY                                                       \n" +
            "   year ASC,                                                   \n" +
            "   month ASC,                                                  \n" +
            "   fuel_type ASC                                               \n";
    private static final String END = ";                                    \n";


    private final StatisticsQuery query;

    StatisticsJdbcQuery(DataSource dataSource, StatisticsQuery query) {
        super(dataSource);
        this.query = query;
        init();
    }

    @Override
    protected PreparedStatement prepareStatement(Connection connection) {
        var selectSpecificDriver =
                ThrowingFunction.wrap((DriverId driverId) -> prepareForSpecificDriver(connection, driverId));
        var selectAllDrivers =
                ThrowingSupplier.wrap(() -> prepareForAllDrivers(connection));
        return query.getFilterByDriver()
                .map(selectSpecificDriver)
                .orElseGet(selectAllDrivers);
    }

    private PreparedStatement prepareForSpecificDriver(
            Connection connection,
            DriverId driverId
    ) throws SQLException {
        var sql = SELECT + FROM + WHERE + GROUP_BY + ORDER_BY + END;
        var statement = connection.prepareStatement(sql);
        statement.setString(1, driverId.getValue());
        return statement;
    }

    private PreparedStatement prepareForAllDrivers(
            Connection connection
    ) throws SQLException {
        var sql = SELECT + FROM + GROUP_BY + ORDER_BY + END;
        return connection.prepareStatement(sql);
    }

    @Override
    protected StatisticsRow parseRow(ResultSet resultSet) throws SQLException {
        var year = resultSet.getInt("year");
        var month = resultSet.getInt("month");
        var fuelingMonth = new FuelingMonth(YearMonth.of(year, month));
        var fuel = Fuel.wrap(resultSet.getString("fuel_type"));
        var volume = new Volume(resultSet.getBigDecimal("volume"));
        var average_price = new Price(resultSet.getBigDecimal("average_price"));
        var total_price = new Price(resultSet.getBigDecimal("total_price"));
        return new StatisticsRow(fuelingMonth, fuel, volume, average_price, total_price);
    }
}
