package fleet.components.analytics.adapters.datamart;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public abstract class ClosableJdbcQuery<Row> {
    private final DataSource dataSource;
    private Connection connection;
    private PreparedStatement statement;
    private ResultSet resultSet;

    protected ClosableJdbcQuery(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    protected void init() {
        try {
            this.connection = dataSource.getConnection();
            this.statement = prepareStatement(connection);
            this.resultSet = statement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract PreparedStatement prepareStatement(Connection connection);

    protected abstract Row parseRow(ResultSet resultSet) throws SQLException;

    public Optional<Row> read() {
        try {
            if (resultSet.next()) {
                return Optional.of(parseRow(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try (
                var connection = this.connection;
                var statement = this.statement;
                var resultSet = this.resultSet
        ) {
            // do nothing - just close all declared resources
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
