package fleet.components.analytics.adapters.datamart;

import akka.Done;
import akka.NotUsed;
import akka.japi.function.Creator;
import akka.japi.function.Function;
import akka.stream.javadsl.Source;
import fleet.components.analytics.ports.indatamart.*;

import javax.sql.DataSource;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class DatamartAdapter implements DataMartPort {
    private final DataSource dataSource;

    public DatamartAdapter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Source<ConsumptionRow, NotUsed> retrieveFuelConsumption(ConsumptionQuery query) {
        Creator<CompletionStage<ConsumptionJdbcQuery>> create =
                () -> CompletableFuture.supplyAsync(() -> new ConsumptionJdbcQuery(dataSource, query));
        Function<ConsumptionJdbcQuery, CompletionStage<Optional<ConsumptionRow>>> read =
                sqlQuery -> CompletableFuture.supplyAsync(sqlQuery::read);
        Function<ConsumptionJdbcQuery, CompletionStage<Done>> close =
                sqlQuery -> CompletableFuture.runAsync(sqlQuery::close).thenApply(v -> Done.done());
        return Source.unfoldResourceAsync(create, read, close);
    }

    @Override
    public Source<StatisticsRow, NotUsed> retrieveStatistics(StatisticsQuery query) {
        Creator<CompletionStage<StatisticsJdbcQuery>> create =
                () -> CompletableFuture.supplyAsync(() -> new StatisticsJdbcQuery(dataSource, query));
        Function<StatisticsJdbcQuery, CompletionStage<Optional<StatisticsRow>>> read =
                sqlQuery -> CompletableFuture.supplyAsync(sqlQuery::read);
        Function<StatisticsJdbcQuery, CompletionStage<Done>> close =
                sqlQuery -> CompletableFuture.runAsync(sqlQuery::close).thenApply(v -> Done.done());
        return Source.unfoldResourceAsync(create, read, close);
    }

    @Override
    public Source<TotalRow, NotUsed> retrieveTotalSpentAmount(TotalQuery query) {
        Creator<CompletionStage<TotalJdbcQuery>> create =
                () -> CompletableFuture.supplyAsync(() -> new TotalJdbcQuery(dataSource, query));
        Function<TotalJdbcQuery, CompletionStage<Optional<TotalRow>>> read =
                sqlQuery -> CompletableFuture.supplyAsync(sqlQuery::read);
        Function<TotalJdbcQuery, CompletionStage<Done>> close =
                sqlQuery -> CompletableFuture.runAsync(sqlQuery::close).thenApply(v -> Done.done());
        return Source.unfoldResourceAsync(create, read, close);
    }
}
