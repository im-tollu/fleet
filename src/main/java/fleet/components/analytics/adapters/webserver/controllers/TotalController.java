package fleet.components.analytics.adapters.webserver.controllers;

import akka.NotUsed;
import akka.http.javadsl.model.*;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fleet.aux.webserver.AbstractController;
import fleet.aux.webserver.ParsingFailure;
import fleet.aux.webserver.ParsingResult;
import fleet.aux.webserver.ParsingSuccess;
import fleet.components.analytics.adapters.webserver.format.ObjectMapperFactory;
import fleet.components.analytics.ports.indatamart.DataMartPort;
import fleet.components.analytics.ports.indatamart.TotalQuery;
import fleet.components.analytics.ports.indatamart.TotalRow;
import fleet.vocabulary.DriverId;

public class TotalController extends AbstractController<TotalQuery, Source<TotalRow, NotUsed>> {
    private final DataMartPort dataMartPort;
    private final Materializer materializer;
    private final ObjectMapper mapper;

    public TotalController(DataMartPort dataMartPort, Materializer materializer) {
        this.dataMartPort = dataMartPort;
        this.materializer = materializer;
        this.mapper = ObjectMapperFactory.getMapper();
    }

    @Override
    public boolean matchRequest(HttpRequest request) {
        var methodIsGet = request.method().equals(HttpMethods.GET);
        var pathIsTotal = request.getUri().path().equals("/total");
        return methodIsGet && pathIsTotal;
    }

    @Override
    public Flow<HttpRequest, ParsingResult, NotUsed> getParsingFlow() {
        return Flow.of(HttpRequest.class)
                .map(this::parseTotalQuery);
    }

    @Override
    public Flow<TotalQuery, Source<TotalRow, NotUsed>, NotUsed> getProcessingFlow() {
        return Flow.of(TotalQuery.class)
                .map(dataMartPort::retrieveTotalSpentAmount);
    }

    @Override
    public Flow<Source<TotalRow, NotUsed>, HttpResponse, NotUsed> getResponseFormattingFlow() {
        return Flow.<Source<TotalRow, NotUsed>>create()
                .map(this::rowsToJson)
                .map(bytes -> HttpEntities.create(ContentTypes.APPLICATION_JSON, bytes))
                .map(entity -> HttpResponse.create().withEntity(entity));

    }

    @Override
    public Flow<ParsingFailure, HttpResponse, NotUsed> getParsingFailureFlow() {
        return Flow.of(ParsingFailure.class)
                .map(this::formatBadRequestResponse);
    }

    private ParsingResult parseTotalQuery(HttpRequest request) {
        try {
            var httpQuery = request.getUri().query();
            var filterByDriver = httpQuery
                    .get("driver")
                    .map(DriverId::wrap);
            return new ParsingSuccess<>(new TotalQuery(filterByDriver));
        } catch (Throwable e) {
            return new ParsingFailure(e.getMessage());
        } finally {
            request.discardEntityBytes(materializer);
        }
    }

    private Source<ByteString, NotUsed> rowsToJson(Source<TotalRow, NotUsed> source) {
        var startArray = ByteString.fromString("[\n");
        var comma = ByteString.fromString(", \n\t");
        var endArray = ByteString.fromString("]");
        return source.map(this::toJson).intersperse(startArray, comma, endArray);
    }

    private ByteString toJson(TotalRow row) {
        try {
            return ByteString.fromArray(mapper.writeValueAsBytes(row));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String toJson(ParsingFailure parsingFailure) {
        try {
            return mapper.writeValueAsString(parsingFailure);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpResponse formatBadRequestResponse(ParsingFailure parsingFailure) {
        var json = toJson(parsingFailure);
        return HttpResponse.create()
                .withStatus(StatusCodes.BAD_REQUEST)
                .withEntity(ContentTypes.APPLICATION_JSON, json);
    }

}