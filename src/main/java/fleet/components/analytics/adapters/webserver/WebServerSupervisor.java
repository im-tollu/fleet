package fleet.components.analytics.adapters.webserver;

import akka.actor.Props;
import akka.stream.Materializer;
import fleet.aux.webserver.AbstractWebServerSupervisor;
import fleet.aux.webserver.Controller;
import fleet.components.analytics.adapters.webserver.controllers.ConsumptionController;
import fleet.components.analytics.adapters.webserver.controllers.StatisticsController;
import fleet.components.analytics.adapters.webserver.controllers.TotalController;
import fleet.components.analytics.config.AnalyticsWebServerConfig;
import fleet.components.analytics.config.AnalyticsWebServerContext;
import fleet.components.analytics.ports.indatamart.DataMartPort;

import java.util.List;

public class WebServerSupervisor extends AbstractWebServerSupervisor {
    public static Props props(AnalyticsWebServerConfig config, AnalyticsWebServerContext context) {
        return Props.create(WebServerSupervisor.class, () -> new WebServerSupervisor(config, context));
    }

    private WebServerSupervisor(AnalyticsWebServerConfig config, AnalyticsWebServerContext context) {
        super(
                config.getHost(),
                config.getPort(),
                getControllers(context.getDataMartPort(), context.getMaterializer()),
                context.getActorSystem(),
                context.getMaterializer()
        );
    }

    private static List<Controller> getControllers(
            DataMartPort dataMartPort,
            Materializer materializer
    ) {
        return List.of(
                new ConsumptionController(dataMartPort, materializer),
                new StatisticsController(dataMartPort, materializer),
                new TotalController(dataMartPort, materializer)
        );
    }
}
