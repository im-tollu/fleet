package fleet.components.analytics.adapters.webserver.format;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fleet.aux.jackson.*;
import fleet.components.analytics.ports.indatamart.ConsumptionRow;
import fleet.components.analytics.ports.indatamart.StatisticsRow;
import fleet.components.analytics.ports.indatamart.TotalRow;
import fleet.vocabulary.*;

import java.util.function.Function;

public class ObjectMapperFactory {
    public static ObjectMapper getMapper() {
        var module = new SimpleModule("AnalyticsWebServerJacksonModule");
        addSerializers(module, getSerializers());

        var objectMapper = new ObjectMapper();
        objectMapper.registerModules(module);
        return objectMapper;
    }

    private static void addSerializers(SimpleModule module, JsonSerializer[] serializers) {
        for (JsonSerializer serializer : serializers) {
            module.addSerializer(serializer);
        }
    }

    private static JsonSerializer[] getSerializers() {
        return new JsonSerializer[]{
                new ObjectCastingSerializer<>(ConsumptionRow.class, getConsumptionRowCast()),
                new ObjectCastingSerializer<>(StatisticsRow.class, getStatisticsRowCast()),
                new ObjectCastingSerializer<>(TotalRow.class, getTotalRowCast()),
                new WrapperSerializer<>(FuelingId.class, ValueWriter.forLong()),
                new WrapperSerializer<>(Fuel.class, ValueWriter.forString()),
                new WrapperSerializer<>(DriverId.class, ValueWriter.forString()),
                new WrapperSerializer<>(Price.class, ValueWriter.forString()),
                new WrapperSerializer<>(Volume.class, ValueWriter.forString()),
                new WrapperSerializer<>(FuelingDate.class, ValueWriter.forString()),
                new WrapperSerializer<>(RegistrationTimestamp.class, ValueWriter.forString()),
                new WrapperSerializer<>(FuelingMonth.class, ValueWriter.forString())
        };
    }

    private static RecordCast<ConsumptionRow> getConsumptionRowCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("date", FuelingDate.class),
                new FieldTyping<>("fuel", Fuel.class),
                new FieldTyping<>("driverId", DriverId.class),
                new FieldTyping<>("price", Price.class),
                new FieldTyping<>("volume", Volume.class),
                new FieldTyping<>("totalPrice", Price.class)
        };
        Function<ConsumptionRow, Object[]> valueExtractor = row -> new Object[]{
                row.getFuelingDate(),
                row.getFuel(),
                row.getDriverId(),
                row.getPrice(),
                row.getVolume(),
                row.getTotalPrice()
        };
        return new RecordCast<>(definition, null, valueExtractor);
    }

    private static RecordCast<StatisticsRow> getStatisticsRowCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("fuelingMonth", FuelingMonth.class),
                new FieldTyping<>("fuel", Fuel.class),
                new FieldTyping<>("volume", Volume.class),
                new FieldTyping<>("averagePrice", Price.class),
                new FieldTyping<>("totalPrice", Price.class)
        };
        Function<StatisticsRow, Object[]> valueExtractor = row -> new Object[]{
                row.getFuelingMonth(),
                row.getFuelType(),
                row.getVolume(),
                row.getAveragePrice(),
                row.getTotalPrice()
        };
        return new RecordCast<>(definition, null, valueExtractor);
    }

    private static RecordCast<TotalRow> getTotalRowCast() {
        var definition = new FieldTyping<?>[]{
                new FieldTyping<>("fuelingMonth", FuelingMonth.class),
                new FieldTyping<>("totalPrice", Price.class)
        };
        Function<TotalRow, Object[]> valueExtractor = row -> new Object[]{
                row.getFuelingMonth(),
                row.getTotal_price()
        };
        return new RecordCast<>(definition, null, valueExtractor);
    }








}
