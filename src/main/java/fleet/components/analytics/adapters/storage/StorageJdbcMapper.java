package fleet.components.analytics.adapters.storage;

import akka.japi.function.Function;
import fleet.components.analytics.corefunctions.TransformedData;
import fleet.components.analytics.corefunctions.data.DateDim;
import fleet.components.analytics.corefunctions.data.DriverDim;
import fleet.components.analytics.corefunctions.data.FuelDim;
import fleet.components.analytics.ports.outstorage.StorageFailure;
import fleet.components.analytics.ports.outstorage.StorageRequest;
import fleet.components.analytics.ports.outstorage.StorageResponse;
import fleet.components.analytics.ports.outstorage.StorageSuccess;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class StorageJdbcMapper implements Function<StorageRequest, CompletionStage<StorageResponse>> {
    private static final String INSERT_FUELING_FACT = "" +
            "INSERT INTO                    \n" +
            "   analytics.fueling_fact (    \n" +
            "       date_sk,                \n" +
            "       driver_sk,              \n" +
            "       fuel_sk,                \n" +
            "       fueling_key,            \n" +
            "       price,                  \n" +
            "       volume                  \n" +
            "   )                           \n" +
            "VALUES (                       \n" +
            "   (SELECT date_sk             \n" +
            "    FROM analytics.date_dim    \n" +
            "    WHERE date = ?),           \n" +
            "   (SELECT driver_sk           \n" +
            "    FROM analytics.driver_dim  \n" +
            "    WHERE driver_id = ?),      \n" +
            "   (SELECT fuel_sk             \n" +
            "    FROM analytics.fuel_dim    \n" +
            "    WHERE fuel_type = ?),      \n" +
            "   ?, ?, ?                     \n" +
            ")                              \n" +
            ";                              \n";
    private static final String MERGE_DATE_DIM = "" +
            "MERGE INTO                     \n" +
            "   analytics.date_dim (        \n" +
            "       date,                   \n" +
            "       month_name,             \n" +
            "       month,                  \n" +
            "       quarter,                \n" +
            "       year                    \n" +
            "   )                           \n" +
            "KEY (                          \n" +
            "   date                        \n" +
            ")                              \n" +
            "VALUES (                       \n" +
            "   ?, ?, ?, ?, ?               \n" +
            ")                              \n" +
            ";                              \n";
    private static final String MERGE_DRIVER_DIM = "" +
            "MERGE INTO                     \n" +
            "   analytics.driver_dim (      \n" +
            "       driver_id,              \n" +
            "       driver_name             \n" +
            "   )                           \n" +
            "KEY (                          \n" +
            "   driver_id                   \n" +
            ")                              \n" +
            "VALUES (                       \n" +
            "   ?, ?                        \n" +
            ")                              \n" +
            ";                              \n";
    private static final String MERGE_FUEL_DIM = "" +
            "MERGE INTO                     \n" +
            "   analytics.fuel_dim (        \n" +
            "       fuel_type               \n" +
            "   )                           \n" +
            "KEY (                          \n" +
            "   fuel_type                   \n" +
            ")                              \n" +
            "VALUES (                       \n" +
            "   ?                           \n" +
            ")                              \n" +
            ";                              \n";
    private final DataSource dataSource;

    StorageJdbcMapper(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public CompletionStage<StorageResponse> apply(StorageRequest request) {
        return CompletableFuture.supplyAsync(() -> applySync(request));
    }

    private StorageResponse applySync(StorageRequest request) {
        try {
            storeTransformedData(request.getData());
            return buildSuccessResponse(request);
        } catch (Throwable e) {
            e.printStackTrace();
            return buildServiceFailureResponse(request);

        }
    }

    private void storeTransformedData(TransformedData data) throws SQLException {
        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement mergeDateDim = mergeDateDim(conn, data.getDateDim());
                PreparedStatement mergeDriverDim = mergeDriverDim(conn, data.getDriverDim());
                PreparedStatement mergeFuelDim = mergeFuelDim(conn, data.getFuelDim());
                PreparedStatement insertFuelingFact = insertFuelingFact(conn, data)
        ) {
            mergeDateDim.executeUpdate();
            mergeDriverDim.executeUpdate();
            mergeFuelDim.executeUpdate();
            insertFuelingFact.executeUpdate();
        }
    }

    private PreparedStatement mergeDateDim(Connection conn, DateDim dateDim) throws SQLException {
        var date = dateDim.getFuelingDate().getDate();
        var monthName = dateDim.getMonthName();
        var month = dateDim.getMonth();
        var quarter = dateDim.getQuarter();
        var year = dateDim.getYear();

        var statement = conn.prepareStatement(MERGE_DATE_DIM);
        statement.setObject(1, date);
        statement.setString(2, monthName);
        statement.setInt(3, month);
        statement.setInt(4, quarter);
        statement.setInt(5, year);
        return statement;

    }

    private PreparedStatement mergeDriverDim(Connection conn, DriverDim driverDim) throws SQLException {
        var driverId = driverDim.getDriverId();
        var driverName = driverDim.getDriverName();

        var statement = conn.prepareStatement(MERGE_DRIVER_DIM);
        statement.setObject(1, driverId.getUuid());
        statement.setString(2, driverName.getValue());
        return statement;

    }

    private PreparedStatement mergeFuelDim(Connection conn, FuelDim fuelDim) throws SQLException {
        var fuel = fuelDim.getFuel();

        var statement = conn.prepareStatement(MERGE_FUEL_DIM);
        statement.setString(1, fuel.getValue());
        return statement;
    }

    private PreparedStatement insertFuelingFact(Connection conn, TransformedData data) throws SQLException {
        var date = data.getFuelingDate().getDate();
        var driverId = data.getDriverId();
        var fuel = data.getFuel();
        var fuelingFact = data.getFuelingFact();
        var price = fuelingFact.getPrice();
        var volume = fuelingFact.getVolume();
        var fuelingId = fuelingFact.getFuelingId();

        var statement = conn.prepareStatement(INSERT_FUELING_FACT);
        statement.setObject(1, date);
        statement.setObject(2, driverId.getUuid());
        statement.setString(3, fuel.getValue());
        statement.setLong(4, fuelingId.getValue());
        statement.setBigDecimal(5, price.getDecimal());
        statement.setBigDecimal(6, volume.getDecimal());
        return statement;
    }

    private StorageResponse buildSuccessResponse(StorageRequest request) {
        var response = new StorageSuccess();
        response.setReturnAddress(request.getReturnAddress());
        return response;
    }

    private StorageFailure buildServiceFailureResponse(StorageRequest request) {
        var response = new StorageFailure();
        response.setReturnAddress(request.getReturnAddress());
        return response;
    }
}
