package fleet.components.analytics;

import akka.actor.AbstractActor;
import akka.actor.AllForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.japi.pf.DeciderBuilder;
import akka.stream.ActorMaterializer;
import fleet.components.analytics.adapters.datamart.DatamartAdapter;
import fleet.components.analytics.adapters.drivers.DriversAdapterSupervisor;
import fleet.components.analytics.adapters.eventbus.BusAdapterSupervisor;
import fleet.components.analytics.adapters.storage.StorageAdapterSupervisor;
import fleet.components.analytics.adapters.webserver.WebServerSupervisor;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.analytics.config.AnalyticsContext;
import fleet.components.analytics.config.AnalyticsWebServerConfig;
import fleet.components.analytics.config.AnalyticsWebServerContext;
import fleet.components.analytics.coreoperations.CoreSupervisorActor;
import fleet.components.analytics.coreoperations.CoreSupervisorRef;

import java.time.Duration;

public class AnalyticsComponentSupervisor extends AbstractActor {
    public static Props props(AnalyticsConfig config, AnalyticsContext context){
        return Props.create(
                AnalyticsComponentSupervisor.class,
                () -> new AnalyticsComponentSupervisor(config, context)
        );
    }

    private AnalyticsComponentSupervisor(AnalyticsConfig config, AnalyticsContext context) {
        this.config = config;
        this.context = context;
    }

    private final AnalyticsConfig config;
    private final AnalyticsContext context;


    @Override
    public void preStart() {
        var core = getContext().actorOf(CoreSupervisorActor.props(config), "Core");
        var corePorts = new CoreSupervisorRef(core).getCorePorts();

        var storageAdapter = getContext()
                .actorOf(StorageAdapterSupervisor.props(context.getDataSource()), "StorageAdapter");
        var driversAdapter = getContext()
                .actorOf(DriversAdapterSupervisor.props(context.getDataSource()), "DriversAdapter");
        var dataMartAdapter = new DatamartAdapter(context.getDataSource());

        corePorts.getStoragePort().registerAdapter(storageAdapter);
        corePorts.getDriversPort().registerAdapter(driversAdapter);

        getContext()
                .actorOf(BusAdapterSupervisor.props(context.getEventBus(), corePorts.getEventsPort()), "EventBusAdapter");

        var webServerConfig = AnalyticsWebServerConfig.fromAnalyticsConfig(config);
        var materializer = ActorMaterializer.create(context.getActorSystem());
        var webServerContext = new AnalyticsWebServerContext(dataMartAdapter, context.getActorSystem(), materializer);
        getContext()
                .actorOf(WebServerSupervisor.props(webServerConfig, webServerContext), "WebServerAdapter");
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new AllForOneStrategy(
                10,
                Duration.ofMinutes(1),
                DeciderBuilder.matchAny(err -> SupervisorStrategy.escalate()).build()
        );
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }
}
