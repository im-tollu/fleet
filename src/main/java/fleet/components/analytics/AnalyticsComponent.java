package fleet.components.analytics;

import akka.actor.ActorRef;
import fleet.aux.Component;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.analytics.config.AnalyticsContext;

public class AnalyticsComponent implements Component {
    private final AnalyticsConfig config;
    private final AnalyticsContext context;
    private ActorRef analytics;

    public AnalyticsComponent(AnalyticsConfig config, AnalyticsContext context) {
        this.config = config;
        this.context = context;
    }

    @Override
    public void start() {
        var system = context.getActorSystem();
        analytics = system.actorOf(
                AnalyticsComponentSupervisor.props(config, context),
                "Analytics"
        );
    }

    @Override
    public void stop() {
        var system = context.getActorSystem();
        system.stop(analytics);
    }
}
