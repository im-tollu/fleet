package fleet.components.analytics.corefunctions;

import fleet.components.analytics.corefunctions.data.DateDim;
import fleet.components.analytics.corefunctions.data.DriverDim;
import fleet.components.analytics.corefunctions.data.FuelDim;
import fleet.components.analytics.corefunctions.data.FuelingFact;
import fleet.vocabulary.DriverId;
import fleet.vocabulary.Fuel;
import fleet.vocabulary.FuelingDate;

public class TransformedData {
    private final FuelingFact fuelingFact;
    private final FuelDim fuelDim;
    private final DriverDim driverDim;
    private final DateDim dateDim;

    public TransformedData(FuelingFact fuelingFact, FuelDim fuelDim, DriverDim driverDim, DateDim dateDim) {
        this.fuelingFact = fuelingFact;
        this.fuelDim = fuelDim;
        this.driverDim = driverDim;
        this.dateDim = dateDim;
    }

    public FuelingFact getFuelingFact() {
        return fuelingFact;
    }

    public FuelDim getFuelDim() {
        return fuelDim;
    }

    public DriverDim getDriverDim() {
        return driverDim;
    }

    public DateDim getDateDim() {
        return dateDim;
    }

    public FuelingDate getFuelingDate() {
        return dateDim.getFuelingDate();
    }

    public DriverId getDriverId() {
        return driverDim.getDriverId();
    }

    public Fuel getFuel() {
        return fuelDim.getFuel();
    }
}
