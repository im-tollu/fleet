package fleet.components.analytics.corefunctions.data;

import fleet.vocabulary.Fuel;

public class FuelDim {
    private final Fuel fuel;

    public FuelDim(Fuel fuel) {
        this.fuel = fuel;
    }

    public Fuel getFuel() {
        return fuel;
    }
}
