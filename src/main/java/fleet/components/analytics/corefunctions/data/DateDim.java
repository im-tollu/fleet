package fleet.components.analytics.corefunctions.data;

import fleet.vocabulary.FuelingDate;

import java.time.Month;

import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.IsoFields.QUARTER_OF_YEAR;

public class DateDim {
    private final FuelingDate fuelingDate;

    public DateDim(FuelingDate fuelingDate) {
        this.fuelingDate = fuelingDate;
    }

    public FuelingDate getFuelingDate() {
        return fuelingDate;
    }

    public String getMonthName() {
        return getMonthObject().toString();
    }

    public int getMonth() {
        return getMonthObject().get(MONTH_OF_YEAR);
    }

    public int getQuarter() {
        return getMonthObject().get(QUARTER_OF_YEAR);
    }

    public int getYear() {
        var localDate = fuelingDate.getDate();
        return localDate.getYear();
    }

    private Month getMonthObject() {
        var localDate = fuelingDate.getDate();
        return localDate.getMonth();
    }

}
