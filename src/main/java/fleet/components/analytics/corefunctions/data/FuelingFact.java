package fleet.components.analytics.corefunctions.data;

import fleet.vocabulary.FuelingId;
import fleet.vocabulary.Price;
import fleet.vocabulary.Volume;

public class FuelingFact {
    private final FuelingId fuelingId;
    private final Price price;
    private final Volume volume;

    public FuelingFact(FuelingId fuelingId, Price price, Volume volume) {
        this.fuelingId = fuelingId;
        this.price = price;
        this.volume = volume;
    }

    public FuelingId getFuelingId() {
        return fuelingId;
    }

    public Price getPrice() {
        return price;
    }

    public Volume getVolume() {
        return volume;
    }
}
