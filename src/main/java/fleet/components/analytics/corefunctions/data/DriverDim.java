package fleet.components.analytics.corefunctions.data;

import fleet.vocabulary.Driver;
import fleet.vocabulary.DriverId;
import fleet.vocabulary.DriverName;

public class DriverDim {
    private final Driver driver;

    public DriverDim(Driver driver) {
        this.driver = driver;
    }

    public DriverId getDriverId() {
        return driver.getId();
    }

    public DriverName getDriverName() {
        return driver.getName();
    }
}
