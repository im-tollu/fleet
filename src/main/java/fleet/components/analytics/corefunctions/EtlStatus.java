package fleet.components.analytics.corefunctions;

public enum EtlStatus {
    Created,
    ExtractingData,
    ExtractSuccess,
    ExtractFailure,
    TransformCommandBuilt,
    DataTransformed,
    LoadingData,
    LoadSuccess,
    LoadFailure,
    Success,
    Timeout
}
