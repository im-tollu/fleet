package fleet.components.analytics.corefunctions;

import fleet.aux.statemachine.StateMachine;
import fleet.components.analytics.ports.inevents.FuelingEvent;
import fleet.components.analytics.ports.outdrivers.DriversQuery;
import fleet.components.analytics.ports.outdrivers.DriversResponse;
import fleet.components.analytics.ports.outdrivers.DriversSuccess;
import fleet.components.analytics.ports.outstorage.StorageRequest;
import fleet.components.analytics.ports.outstorage.StorageResponse;

import static fleet.components.analytics.corefunctions.EtlStatus.*;

public class EtlWorkflow extends StateMachine<EtlStatus> {
    private final FuelingEvent fuelingEvent;

    private DriversQuery driversQuery;
    private DriversResponse driversResponse;

    private TransformCommand command;
    private TransformedData data;

    private StorageRequest storageRequest;
    private StorageResponse storageResponse;

    private String etlLogEntry;

    public EtlWorkflow(FuelingEvent fuelingEvent) {
        this.fuelingEvent = fuelingEvent;
    }

    @Override
    protected EtlStatus getInitialState() {
        return EtlStatus.Created;
    }

    public void accumulateExternalData() {
        ensureStatus(EtlStatus.Created);
        var fueling = this.fuelingEvent.getFueling();
        this.driversQuery = new DriversQuery(fueling.getDriverId(), fuelingEvent.getCorrelationId());
        transitionTo(EtlStatus.ExtractingData);
    }

    public void acceptExternalData(DriversResponse response) {
        ensureStatus(ExtractingData);
        this.driversResponse = response;
        if (response.isSuccess()) {
            transitionTo(ExtractSuccess);
        } else {
            transitionTo(ExtractFailure);
            buildFailureLogEntry();
        }
    }

    public void buildTransformCommand() {
        ensureStatus(ExtractSuccess);
        command = new TransformCommand();
        command.setFueling(this.fuelingEvent.getFueling());
        var driver = ((DriversSuccess) driversResponse).getDriver();
        command.setDriver(driver);
        transitionTo(TransformCommandBuilt);
    }

    public void acceptTransformedData(TransformedData data) {
        ensureStatus(TransformCommandBuilt);
        this.data = data;
        transitionTo(DataTransformed);
    }

    public void loadData() {
        ensureStatus(DataTransformed);
        this.storageRequest = new StorageRequest(data);
        transitionTo(LoadingData);
    }

    public void acceptLoadResult(StorageResponse result) {
        ensureStatus(LoadingData);
        this.storageResponse = result;
        if (result.isSuccess()) {
            transitionTo(LoadSuccess);
        } else {
            transitionTo(LoadFailure);
            buildFailureLogEntry();
        }
    }

    public void acceptSuccess() {
        ensureStatus(LoadSuccess);
        transitionTo(Success);
        buildSuccessLogEntry();
    }

    public void acceptTimeout() {
        transitionTo(EtlStatus.Timeout);
        buildFailureLogEntry();
    }

    private void buildSuccessLogEntry() {
        ensureStatus(Success);
        etlLogEntry = "Data successfully loaded";
    }

    private void buildFailureLogEntry() {
        ensureStatus(ExtractFailure, LoadFailure, Timeout);
        etlLogEntry = "Failed to load data";
    }

    public String getEtlLogEntry() {
        return etlLogEntry;
    }

    public DriversQuery getDriversQuery() {
        return driversQuery;
    }

    public TransformCommand getCommand() {
        return command;
    }

    public StorageRequest getStorageRequest() {
        return storageRequest;
    }
}
