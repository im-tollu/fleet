package fleet.components.analytics.corefunctions;

import fleet.components.analytics.corefunctions.data.DateDim;
import fleet.components.analytics.corefunctions.data.DriverDim;
import fleet.components.analytics.corefunctions.data.FuelDim;
import fleet.components.analytics.corefunctions.data.FuelingFact;

public class Transform {
    public TransformedData transform(TransformCommand command) {
        var fueling = command.getFueling();
        var driver = command.getDriver();
        var fuelingFact = new FuelingFact(
                fueling.getFuelingId(),
                fueling.getPrice(),
                fueling.getVolume()
        );
        var fuelDim = new FuelDim(fueling.getFuel());
        var driverDim = new DriverDim(driver);
        var dateDim = new DateDim(fueling.getFuelingDate());
        return new TransformedData(fuelingFact, fuelDim, driverDim, dateDim);
    }
}
