package fleet.components.analytics.corefunctions;

import fleet.vocabulary.Driver;
import fleet.vocabulary.Fueling;

public class TransformCommand {
    private Fueling fueling;
    private Driver driver;

    public Fueling getFueling() {
        return fueling;
    }

    public void setFueling(Fueling fueling) {
        this.fueling = fueling;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
