package fleet.components.storage;

import org.h2.api.Trigger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.UUID;

public class PreventDuplicateFuelingTrigger implements Trigger {
    private static final String SCHEMA_NAME = "registration";
    private static final String TABLE_NAME = "fueling";
    private static final String COUNT_DUPLICATES_SQL = "" +
            "SELECT                     \n" +
            "   count(1) AS duplicates  \n" +
            "FROM                       \n" +
            "   registration.fueling    \n" +
            "WHERE                      \n" +
            "   fuel = ?                \n" +
            "   AND                     \n" +
            "   price = ?               \n" +
            "   AND                     \n" +
            "   volume = ?              \n" +
            "   AND                     \n" +
            "   fueling_date = ?        \n" +
            "   AND                     \n" +
            "   driver_id = ?           \n" +
            ";                          \n";

    @Override
    public void init(Connection conn, String schemaName, String triggerName, String tableName, boolean before, int type) throws SQLException {
        boolean isTriggerDeclarationValid =
                before
                        && type == INSERT
                        && schemaName.equalsIgnoreCase(SCHEMA_NAME)
                        && tableName.equalsIgnoreCase(TABLE_NAME);
        if (!isTriggerDeclarationValid) {
            var message = "PreventDuplicateFuelingTrigger can only be fired BEFORE INSERT " +
                    "on table REGISTRATION.FUELING";
            throw new SQLException(message);
        }
    }

    @Override
    public void fire(Connection conn, Object[] oldRow, Object[] newRow) throws SQLException {
        try (
                var statement = conn.prepareStatement(COUNT_DUPLICATES_SQL);
                var resultSet = queryForDuplicate(statement, newRow)
        ) {
            resultSet.next();
            var duplicates = resultSet.getInt(1);
            if (duplicates > 0) {
                var message = "No fueling duplicates allowed!";
                throw new SQLIntegrityConstraintViolationException(message);
            }
        }
    }

    private ResultSet queryForDuplicate(PreparedStatement statement, Object[] newRow) throws SQLException {
        String fuel = (String) newRow[1];
        BigDecimal price = (BigDecimal) newRow[2];
        BigDecimal volume = (BigDecimal) newRow[3];
        Date fueling_date = (Date) newRow[4];
        UUID driver_id = (UUID) newRow[5];

        statement.setString(1, fuel);
        statement.setBigDecimal(2, price);
        statement.setBigDecimal(3, volume);
        statement.setDate(4, fueling_date);
        statement.setObject(5, driver_id);

        return statement.executeQuery();
    }

    @Override
    public void close() {

    }

    @Override
    public void remove() {

    }
}
