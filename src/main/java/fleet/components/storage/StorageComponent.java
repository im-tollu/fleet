package fleet.components.storage;

import fleet.aux.Component;
import org.flywaydb.core.Flyway;
import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.Server;

import javax.sql.DataSource;

import static java.util.Objects.nonNull;

public class StorageComponent implements Component {
    private final StorageConfig config;
    private Server server;
    private JdbcConnectionPool connectionPool;


    public StorageComponent(StorageConfig config) {
        this.config = config;
    }

    @Override
    public void start() {
        try {
            org.h2.Driver.load();
            server = Server.createTcpServer(config.getParams());
            server.start();
            connectionPool = JdbcConnectionPool.create(
                    config.getUrl(),
                    config.getUser(),
                    config.getPassword()
            );
            applyMigrations();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public DataSource getDataSource() {
        return connectionPool;
    }

    @Override
    public void stop() {
        try (
                var conn = getDataSource().getConnection();
                var statement = conn.createStatement()
        ) {
            statement.execute("SHUTDOWN");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (nonNull(connectionPool)) {
            connectionPool.dispose();
        }
        if (nonNull(server)) {
            server.stop();
            server = null;
        }
    }

    private void applyMigrations() {
        var flyway = new Flyway();
        flyway.setDataSource(connectionPool);
        flyway.migrate();
    }
}
