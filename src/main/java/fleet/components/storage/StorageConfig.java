package fleet.components.storage;

public class StorageConfig {
    private static final String H2_URL_PREFIX = "jdbc:h2";
    private final String url;
    private final String user;
    private final String password;
    private final String[] params = new String[]{};

    public StorageConfig(String fileName, String user, String password) {
        this.url = buildConnectionUrl(fileName);
        this.user = user;
        this.password = password;
    }

    private String buildConnectionUrl(String fileName) {
        return String.format("%s:%s;DB_CLOSE_ON_EXIT=FALSE", H2_URL_PREFIX, fileName);
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String[] getParams() {
        return params;
    }
}
