package fleet.components.eventbus;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;

public class Subscriber extends ActorRefWrapper {
    public Subscriber(ActorRef value) {
        super(value);
    }

    void notify(Message message) {
        tell(message);
    }

    @Override
    public int hashCode() {
        return getValue().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        } else if (!this.getClass().equals(obj.getClass())){
            return false;
        } else {
            Subscriber that = (Subscriber) obj;
            return this.getValue().equals(that.getValue());
        }
    }
}

