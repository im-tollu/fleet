package fleet.components.eventbus;

import fleet.aux.Component;

public class EventBusComponent implements Component {
    private final EventBusContext context;
    private EventBusRef eventBus;

    public EventBusComponent(EventBusContext context) {
        this.context = context;
    }

    @Override
    public void start() {
        var system = context.getActorSystem();
        var eventBusActor = system.actorOf(EventBusActor.props(), "EventBus");
        eventBus = new EventBusRef(eventBusActor);
    }

    @Override
    public void stop() {
        var system = context.getActorSystem();
        system.stop(eventBus.getValue());
        eventBus = null;
    }

    public EventBusRef getEventBusRef() {
        return eventBus;
    }
}
