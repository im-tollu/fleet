package fleet.components.eventbus;

import akka.actor.ActorRef;
import fleet.aux.actors.ActorRefWrapper;

public class EventBusRef extends ActorRefWrapper {
    protected EventBusRef(ActorRef value) {
        super(value);
    }

    public void subscribe(Subscriber subscriber) {
        tell(subscriber);
    }

    public void publish(Message message) {
        tell(message);
    }
}
