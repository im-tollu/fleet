package fleet.components.eventbus;

import fleet.aux.correlation.CorrelationId;
import fleet.aux.correlation.WithCorrelationId;

public class Message implements WithCorrelationId {
    private final Object payload;
    private final CorrelationId correlationId;

    public Message(Object payload, CorrelationId correlationId) {
        this.payload = payload;
        this.correlationId = correlationId;
    }

    public Object getPayload() {
        return payload;
    }

    @Override
    public CorrelationId getCorrelationId() {
        return correlationId;
    }
}
