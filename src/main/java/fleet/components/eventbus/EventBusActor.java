package fleet.components.eventbus;

import akka.actor.AbstractActor;
import akka.actor.Props;

import java.util.HashSet;
import java.util.Set;

public class EventBusActor extends AbstractActor {
    public static Props props() {
        return Props.create(EventBusActor.class);
    }

    private final Set<Subscriber> subscribers;

    public EventBusActor() {
        subscribers = new HashSet<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Subscriber.class, this::receiveSubscriber)
                .match(Message.class, this::receiveMessage)
                .build();
    }

    private void receiveSubscriber(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    private void receiveMessage(Message message) {
        subscribers.forEach(subscriber -> subscriber.notify(message));
    }
}
