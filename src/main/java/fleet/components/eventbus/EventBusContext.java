package fleet.components.eventbus;

import akka.actor.ActorSystem;

public class EventBusContext {
    private final ActorSystem actorSystem;

    public EventBusContext(ActorSystem actorSystem) {
        this.actorSystem = actorSystem;
    }

    public ActorSystem getActorSystem() {
        return actorSystem;
    }
}
