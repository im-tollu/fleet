package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.ValidationSuccess;
import fleet.aux.validation.Validator;

import java.util.Random;

public enum Fuel implements Wrapper<String>, Validator {
    A92, A95, A98, D;

    public String getValue() {
        return this.name();
    }

    @Override
    public ValidationResult validate() {
        return new ValidationSuccess();
    }

    public static Fuel wrap(String s) {
        try {
            return valueOf(s);
        } catch (IllegalArgumentException e) {
            String message = String.format("Could not construct Fuel from String [%s]", s);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static Fuel getExample() {
        var fuels = Fuel.values();
        var randomizer = new Random();
        var randomIndex = randomizer.nextInt(fuels.length);
        return fuels[randomIndex];
    }

}
