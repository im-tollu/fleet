package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

public class Volume implements Wrapper<String>, Validator {
    private final BigDecimal volume;

    public Volume(BigDecimal volume) {
        this.volume = volume.setScale(3, RoundingMode.HALF_UP);
    }

    public String getValue() {
        return volume.toString();
    }

    public BigDecimal getDecimal() {
        return volume;
    }

    public boolean isGreaterThanZero() {
        return volume.compareTo(BigDecimal.ZERO) > 0;
    }

    @Override
    public ValidationResult validate() {
        if (volume.compareTo(BigDecimal.ZERO) < 0){
            var message = "Volume must not be negative";
            var error = new ValidationError(message);
            return new ValidationFailure(List.of(error));
        } else {
            return new ValidationSuccess();
        }
    }

    public static Volume wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        try {
            var d = new BigDecimal(nonNullString);
            return new Volume(d);
        } catch (IllegalArgumentException e) {
            String message = String.format("Could not construct Volume from String [%s]", nonNullString);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static Volume getExample() {
        var randomizer = new java.util.Random();
        var randomValue = BigDecimal.valueOf(1.0 * randomizer.nextInt(100000) / 1000);
        return new Volume(randomValue);
    }

    @Override
    public String toString() {
        return "Volume{" +
                volume +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Volume volume1 = (Volume) o;

        return volume != null ? volume.equals(volume1.volume) : volume1.volume == null;
    }

    @Override
    public int hashCode() {
        return volume != null ? volume.hashCode() : 0;
    }
}
