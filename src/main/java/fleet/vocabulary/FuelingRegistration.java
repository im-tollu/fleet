package fleet.vocabulary;

import fleet.aux.validation.*;

import java.util.List;
import java.util.Objects;

public class FuelingRegistration implements Validator {
    private final Fuel fuel;
    private final Price price;
    private final Volume volume;
    private final FuelingDate fuelingDate;
    private final DriverId driverId;

    public FuelingRegistration(
            Fuel fuel,
            Price price,
            Volume volume,
            FuelingDate fuelingDate,
            DriverId driverId
    ) {
        this.fuel = fuel;
        this.price = price;
        this.volume = volume;
        this.fuelingDate = fuelingDate;
        this.driverId = driverId;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public Price getPrice() {
        return price;
    }

    public Volume getVolume() {
        return volume;
    }

    public FuelingDate getFuelingDate() {
        return fuelingDate;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    @Override
    public ValidationResult validate() {
        var nonNullMemberValidations = validateNonNullMembers();
        if (!nonNullMemberValidations.isValid()) {
            return nonNullMemberValidations;
        } else {
            return new ValidationSuccess()
                    .flatMap(validateMembers())
                    .flatMap(validatePriceIsGreaterThanZero())
                    .flatMap(validateVolumeIsGreaterThanZero());
        }
    }

    private ValidationResult validateNonNullMembers() {
        return new ValidationSuccess()
                .flatMap(validateNonNull(driverId, "driverId"))
                .flatMap(validateNonNull(fuel, "fuel"))
                .flatMap(validateNonNull(price, "price"))
                .flatMap(validateNonNull(volume, "volume"))
                .flatMap(validateNonNull(fuelingDate, "fuelingDate"));

    }

    private ValidationResult validateNonNull(Validator member, String name) {
        if (Objects.isNull(member)) {
            String message = String.format("Fueling member must not be null: %s", name);
            var error = new ValidationError(message);
            return new ValidationFailure(List.of(error));
        } else {
            return new ValidationSuccess();
        }
    }

    private ValidationResult validateMembers() {
        return new ValidationSuccess()
                .flatMap(driverId.validate())
                .flatMap(fuel.validate())
                .flatMap(price.validate())
                .flatMap(volume.validate())
                .flatMap(fuelingDate.validate());
    }

    private ValidationResult validatePriceIsGreaterThanZero() {
        if (price.isGreaterThanZero()) {
            return new ValidationSuccess();
        } else {
            var error = new ValidationError("Price must be greater than zero");
            return new ValidationFailure(List.of(error));
        }
    }

    private ValidationResult validateVolumeIsGreaterThanZero() {
        if (volume.isGreaterThanZero()) {
            return new ValidationSuccess();
        } else {
            var error = new ValidationError("Volume must be greater than zero");
            return new ValidationFailure(List.of(error));
        }
    }

    public static FuelingRegistration getExample() {
        return new FuelingRegistration(
                Fuel.getExample(),
                Price.getExample(),
                Volume.getExample(),
                FuelingDate.getExample(),
                DriverId.getExample()
        );
    }

    @Override
    public String toString() {
        return "FuelingRegistration{" +
                " fuel=" + fuel +
                ", price=" + price +
                ", volume=" + volume +
                ", fuelingDate=" + fuelingDate +
                ", driverId=" + driverId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FuelingRegistration registration = (FuelingRegistration) o;

        if (driverId != null ? !driverId.equals(registration.driverId) : registration.driverId != null) return false;
        if (fuel != registration.fuel) return false;
        if (price != null ? !price.equals(registration.price) : registration.price != null) return false;
        if (volume != null ? !volume.equals(registration.volume) : registration.volume != null) return false;
        return fuelingDate != null ? fuelingDate.equals(registration.fuelingDate) : registration.fuelingDate == null;
    }

    @Override
    public int hashCode() {
        int result = driverId != null ? driverId.hashCode() : 0;
        result = 31 * result + (fuel != null ? fuel.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (fuelingDate != null ? fuelingDate.hashCode() : 0);
        return result;
    }
}
