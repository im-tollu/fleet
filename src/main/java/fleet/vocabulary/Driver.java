package fleet.vocabulary;

public class Driver {
    private final DriverId id;
    private final DriverName name;

    public Driver(DriverId id, DriverName name) {
        this.id = id;
        this.name = name;
    }

    public DriverId getId() {
        return id;
    }

    public DriverName getName() {
        return name;
    }
}
