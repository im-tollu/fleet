package fleet.vocabulary;

import fleet.aux.Wrapper;

import java.time.YearMonth;

public class FuelingMonth implements Wrapper<String> {
    private final YearMonth yearMonth;

    public FuelingMonth(YearMonth yearMonth) {
        this.yearMonth = yearMonth;
    }

    @Override
    public String getValue() {
        return yearMonth.toString();
    }
}
