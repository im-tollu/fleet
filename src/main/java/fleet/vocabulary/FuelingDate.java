package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.ValidationSuccess;
import fleet.aux.validation.Validator;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class FuelingDate implements Wrapper<String>, Validator {

    private final LocalDate date;

    private FuelingDate(LocalDate date) {
        this.date = date;
    }

    public String getValue() {
        return date.toString();
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public ValidationResult validate() {
        return new ValidationSuccess();
    }

    public static FuelingDate wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        try {
            var date = LocalDate.parse(nonNullString);
            return new FuelingDate(date);
        } catch (DateTimeParseException e) {
            String message = String.format("Could not construct FuelingDate from String [%s]", nonNullString);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static FuelingDate fromLocalDate(LocalDate date) {
        return new FuelingDate(date);
    }

    public static FuelingDate getExample() {
        var randomizer = new java.util.Random();
        var maxDay = Instant.now().getEpochSecond() / (60 * 60 * 24) * 2;
        var randomDay = randomizer.nextLong() % maxDay;
        var randomDate = LocalDate.ofEpochDay(randomDay);
        return new FuelingDate(randomDate);
    }

    @Override
    public String toString() {
        return "FuelingDate{" +
                date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FuelingDate that = (FuelingDate) o;

        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        return date != null ? date.hashCode() : 0;
    }
}