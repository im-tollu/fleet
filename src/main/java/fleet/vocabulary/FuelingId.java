package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.ValidationSuccess;
import fleet.aux.validation.Validator;

import java.util.Objects;
import java.util.Random;

public class FuelingId implements Wrapper<Long>, Validator {
    private final Long id;

    private FuelingId(Long id) {
        this.id = id;
    }

    public Long getValue() {
        return id;
    }

    @Override
    public ValidationResult validate() {
        return new ValidationSuccess();
    }

    public static FuelingId wrap(Long id) {
        if (Objects.isNull(id)) {
            var message = "FuelingId must not wrap null value";
            throw new BusinessValueConstructionException(message);
        }
        return new FuelingId(id);
    }

    public static FuelingId getExample() {
        var randomId = new Random().nextLong();
        return new FuelingId(randomId);
    }

    @Override
    public String toString() {
        return "FuelingId{" +
                id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FuelingId fuelingId = (FuelingId) o;

        return id != null ? id.equals(fuelingId.id) : fuelingId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
