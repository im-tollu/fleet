package fleet.vocabulary;

public class BusinessCommandProcessingException extends RuntimeException {
    public BusinessCommandProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessCommandProcessingException(String message) {
        super(message);
    }
}
