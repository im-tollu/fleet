package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

public class Price implements Wrapper<String>, Validator {
    private final BigDecimal price;

    public Price(BigDecimal price) {
        this.price = price.setScale(2, RoundingMode.HALF_EVEN);
    }

    public String getValue() {
        return price.toString();
    }

    public BigDecimal getDecimal() {
        return price;
    }

    public boolean isGreaterThanZero() {
        return price.compareTo(BigDecimal.ZERO) > 0;
    }

    @Override
    public ValidationResult validate() {
        if (price.compareTo(BigDecimal.ZERO) < 0){
            var message = "Price must not be negative";
            var error = new ValidationError(message);
            return new ValidationFailure(List.of(error));
        } else {
            return new ValidationSuccess();
        }
    }

    public static Price wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        try {
            var d = new BigDecimal(nonNullString);
            return new Price(d);
        } catch (IllegalArgumentException e) {
            String message = String.format("Could not construct Price from String [%s]", nonNullString);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static Price getExample() {
        var randomizer = new java.util.Random();
        var randomValue = BigDecimal.valueOf(1.0 * randomizer.nextInt(100000) / 1000);
        return new Price(randomValue);
    }

    @Override
    public String toString() {
        return "Price{" +
                price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Price price1 = (Price) o;

        return price != null ? price.equals(price1.price) : price1.price == null;
    }

    @Override
    public int hashCode() {
        return price != null ? price.hashCode() : 0;
    }
}
