package fleet.vocabulary;

public class Fueling {
    private final FuelingId fuelingId;
    private final Fuel fuel;
    private final Price price;
    private final Volume volume;
    private final FuelingDate fuelingDate;
    private final DriverId driverId;
    private final RegistrationTimestamp registrationTimestamp;


    public Fueling(
            FuelingId fuelingId,
            Fuel fuel,
            Price price,
            Volume volume,
            FuelingDate fuelingDate,
            DriverId driverId,
            RegistrationTimestamp registrationTimestamp
    ) {
        this.fuelingId = fuelingId;
        this.fuel = fuel;
        this.price = price;
        this.volume = volume;
        this.fuelingDate = fuelingDate;
        this.driverId = driverId;
        this.registrationTimestamp = registrationTimestamp;
    }

    public Fueling(
            FuelingRegistration registration,
            FuelingId fuelingId,
            RegistrationTimestamp registrationTimestamp
    ) {
        this(
                fuelingId,
                registration.getFuel(),
                registration.getPrice(),
                registration.getVolume(),
                registration.getFuelingDate(),
                registration.getDriverId(),
                registrationTimestamp
        );
    }

    public FuelingId getFuelingId() {
        return fuelingId;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public Price getPrice() {
        return price;
    }

    public Volume getVolume() {
        return volume;
    }

    public FuelingDate getFuelingDate() {
        return fuelingDate;
    }

    public RegistrationTimestamp getRegistrationTimestamp() {
        return registrationTimestamp;
    }

    @Override
    public String toString() {
        return "Fueling{" +
                " fuelingId=" + fuelingId +
                ", fuel=" + fuel +
                ", price=" + price +
                ", volume=" + volume +
                ", fuelingDate=" + fuelingDate +
                ", driverId=" + driverId +
                ", registrationTimestamp=" + registrationTimestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fueling fueling = (Fueling) o;

        if (fuelingId != null ? !fuelingId.equals(fueling.fuelingId) : fueling.fuelingId != null) return false;
        if (fuel != fueling.fuel) return false;
        if (price != null ? !price.equals(fueling.price) : fueling.price != null) return false;
        if (volume != null ? !volume.equals(fueling.volume) : fueling.volume != null) return false;
        if (fuelingDate != null ? !fuelingDate.equals(fueling.fuelingDate) : fueling.fuelingDate != null) return false;
        if (driverId != null ? !driverId.equals(fueling.driverId) : fueling.driverId != null) return false;
        return registrationTimestamp != null ? registrationTimestamp.equals(fueling.registrationTimestamp) : fueling.registrationTimestamp == null;
    }

    @Override
    public int hashCode() {
        int result = fuelingId != null ? fuelingId.hashCode() : 0;
        result = 31 * result + (fuel != null ? fuel.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (fuelingDate != null ? fuelingDate.hashCode() : 0);
        result = 31 * result + (driverId != null ? driverId.hashCode() : 0);
        result = 31 * result + (registrationTimestamp != null ? registrationTimestamp.hashCode() : 0);
        return result;
    }
}
