package fleet.vocabulary;

public class BusinessValueConstructionException extends RuntimeException {
    public BusinessValueConstructionException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessValueConstructionException(String message) {
        super(message);
    }
}
