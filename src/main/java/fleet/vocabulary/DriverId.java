package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.ValidationSuccess;
import fleet.aux.validation.Validator;

import java.util.Objects;
import java.util.UUID;

public class DriverId implements Wrapper<String>, Validator {
    private final UUID id;

    private DriverId(UUID id) {
        this.id = id;
    }

    public String getValue() {
        return id.toString();
    }

    public UUID getUuid() {
        return id;
    }

    @Override
    public ValidationResult validate() {
        return new ValidationSuccess();
    }

    public static DriverId wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        try {
            var uuid = UUID.fromString(nonNullString);
            return new DriverId(uuid);
        } catch (IllegalArgumentException e) {
            String message = String.format("Could not construct DriverId from String [%s]", nonNullString);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static DriverId fromUuid(UUID id) {
        return new DriverId(id);
    }

    public static DriverId getExample() {
        var randomUUID = UUID.randomUUID();
        return new DriverId(randomUUID);
    }

    @Override
    public String toString() {
        return "DriverId{" +
                id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DriverId driverId = (DriverId) o;

        return id != null ? id.equals(driverId.id) : driverId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
