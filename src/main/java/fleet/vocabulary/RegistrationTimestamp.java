package fleet.vocabulary;

import fleet.aux.Wrapper;
import fleet.aux.validation.ValidationResult;
import fleet.aux.validation.ValidationSuccess;
import fleet.aux.validation.Validator;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

import static java.time.ZoneOffset.UTC;

public class RegistrationTimestamp implements Wrapper<String>, Validator {

    private final Instant timestamp;

    public RegistrationTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getValue() {
        return timestamp.toString();
    }

    public Instant getInstant() {
        return timestamp;
    }

    public LocalDateTime getDateTime() {
        return LocalDateTime.ofInstant(timestamp, UTC);
    }

    public static RegistrationTimestamp wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        try {
            var instant = Instant.parse(nonNullString);
            return new RegistrationTimestamp(instant);
        } catch (DateTimeParseException e) {
            String message = String.format("Could not construct RegistrationTimestamp from String [%s]", nonNullString);
            throw new BusinessValueConstructionException(message, e);
        }
    }

    public static RegistrationTimestamp fromInstant(Instant instant) {
        return new RegistrationTimestamp(instant);
    }

    public static RegistrationTimestamp now() {
        return new RegistrationTimestamp(Instant.now());
    }

    @Override
    public ValidationResult validate() {
        return new ValidationSuccess();
    }

    @Override
    public String toString() {
        return "RegistrationTimestamp{" +
                timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationTimestamp that = (RegistrationTimestamp) o;

        return timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return timestamp.hashCode();
    }
}
