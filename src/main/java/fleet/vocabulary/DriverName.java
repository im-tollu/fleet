package fleet.vocabulary;

import fleet.aux.Wrapper;

import java.util.Objects;

public class DriverName implements Wrapper<String> {
    private final String name;

    private DriverName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return name;
    }

    public static DriverName wrap(String s) {
        var isNullOrEmpty = Objects.isNull(s) || s.isEmpty();
        if (isNullOrEmpty) {
            var message = "Driver name must not be null";
            throw new BusinessValueConstructionException(message);
        }
        return new DriverName(s);
    }
}
