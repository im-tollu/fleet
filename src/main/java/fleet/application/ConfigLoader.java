package fleet.application;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.storage.StorageConfig;

import java.time.Duration;

class ConfigLoader {
    static FleetConfig load() {
        var applicationConfig = ConfigFactory.load();
        var rawConfig = applicationConfig.getConfig("fleet");
        var registrationConfig = parseRegistrationConfig(rawConfig.getConfig("registration"));
        var analyticsConfig = parseAnalyticsConfig(rawConfig.getConfig("analytics"));
        var storageConfig = parseStorageConfig(rawConfig.getConfig("storage.h2"));
        return new FleetConfig(analyticsConfig, registrationConfig, storageConfig);
    }

    private static RegistrationConfig parseRegistrationConfig(Config rawConfig) {
        var host = rawConfig.getString("host");
        var port = rawConfig.getInt("port");
        var processingTimeout = Duration.ofMillis(rawConfig.getLong("processingTimeoutMillis"));
        var fuelingExpiration = rawConfig.getLong("fuelingExpirationDays");
        return new RegistrationConfig(host, port, processingTimeout, fuelingExpiration);
    }

    private static AnalyticsConfig parseAnalyticsConfig(Config rawConfig) {
        var processingTimeout = Duration.ofMillis(rawConfig.getLong("processingTimeoutMillis"));
        var host = rawConfig.getString("host");
        var port = rawConfig.getInt("port");
        return new AnalyticsConfig(processingTimeout, host, port);
    }

    private static StorageConfig parseStorageConfig(Config rawConfig) {
        var filename = rawConfig.getString("filename");
        var user = rawConfig.getString("user");
        var password = rawConfig.getString("password");
        return new StorageConfig(filename, user, password);
    }


}
