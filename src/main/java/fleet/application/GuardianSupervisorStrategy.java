package fleet.application;

import akka.actor.AllForOneStrategy;
import akka.actor.SupervisorStrategy;
import akka.actor.SupervisorStrategyConfigurator;
import akka.japi.pf.DeciderBuilder;

public class GuardianSupervisorStrategy implements SupervisorStrategyConfigurator {
    @Override
    public SupervisorStrategy create() {
        return new AllForOneStrategy(true, DeciderBuilder.matchAny(err -> SupervisorStrategy.escalate()).build());
    }
}
