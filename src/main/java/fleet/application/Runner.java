package fleet.application;

import akka.actor.ActorSystem;
import fleet.components.analytics.AnalyticsComponent;
import fleet.components.analytics.config.AnalyticsContext;
import fleet.components.eventbus.EventBusComponent;
import fleet.components.eventbus.EventBusContext;
import fleet.components.registration.RegistrationComponent;
import fleet.components.registration.config.RegistrationContext;
import fleet.components.storage.StorageComponent;

import java.util.concurrent.ExecutionException;

public class Runner {
    private StorageComponent storage;
    private EventBusComponent eventBus;
    private AnalyticsComponent analytics;
    private RegistrationComponent registration;
    private FleetConfig config;
    private ActorSystem system;

    public static void main(String[] args) {
        var runner = new Runner();
        runner.startApplication();
    }

    private void startApplication() {
        System.out.println("Starting Fleet Management System...");
        loadConfig();
        startActorSystem();
        startStorage();
        startEventBus();
        startAnalytics();
        startRegistration();
        registerShutdownHook();
        System.out.println("Fleet Management System started");
        try {
            system.getWhenTerminated().toCompletableFuture().get();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void stopApplication() {
        System.out.println("Shutting down Fleet Management System...");
        registration.stop();
        analytics.stop();
        eventBus.stop();
        storage.stop();
        System.out.println("All components were shut down.");
    }

    private void loadConfig() {
        System.out.println("Loading config...");
        config = ConfigLoader.load();
    }

    private void startActorSystem() {
        System.out.println("Starting Actor System...");
        system = ActorSystem.create("fleet");
        system.registerOnTermination(() -> System.exit(1));
    }

    private void startStorage() {
        System.out.println("Starting storage...");
        var storageConfig = config.getStorageConfig();
        storage = new StorageComponent(storageConfig);
        storage.start();
    }

    private void startEventBus() {
        System.out.println("Starting event bus...");
        var eventBusContext = new EventBusContext(system);
        eventBus = new EventBusComponent(eventBusContext);
        eventBus.start();
    }

    private void startAnalytics() {
        System.out.println("Starting analytics...");
        var analyticsConfig = config.getAnalyticsConfig();
        var analyticsContext = new AnalyticsContext(system, storage.getDataSource(), eventBus.getEventBusRef());
        analytics = new AnalyticsComponent(analyticsConfig, analyticsContext);
        analytics.start();
        var analyticsMessage = String.format(
                "Analytics service started on %s:%s",
                analyticsConfig.getHost(),
                analyticsConfig.getPort()
        );
        System.out.println(analyticsMessage);

    }

    private void startRegistration() {
        System.out.println("Starting registration...");
        var registrationConfig = config.getRegistrationConfig();
        var registrationContext = new RegistrationContext(system, storage.getDataSource(), eventBus.getEventBusRef());
        registration = new RegistrationComponent(registrationConfig, registrationContext);
        registration.start();
        var registrationMessage = String.format(
                "Registration service started on %s:%s",
                registrationConfig.getHost(),
                registrationConfig.getPort()
        );
        System.out.println(registrationMessage);
    }

    private void registerShutdownHook() {
        var runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new Thread(this::stopApplication));
    }

}
