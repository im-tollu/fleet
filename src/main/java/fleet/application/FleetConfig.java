package fleet.application;

import fleet.components.analytics.config.AnalyticsConfig;
import fleet.components.registration.config.RegistrationConfig;
import fleet.components.storage.StorageConfig;

public class FleetConfig {
    private final AnalyticsConfig analyticsConfig;
    private final RegistrationConfig registrationConfig;
    private final StorageConfig storageConfig;

    public FleetConfig(
            AnalyticsConfig analyticsConfig,
            RegistrationConfig registrationConfig,
            StorageConfig storageConfig
    ) {
        this.analyticsConfig = analyticsConfig;
        this.registrationConfig = registrationConfig;
        this.storageConfig = storageConfig;
    }

    public AnalyticsConfig getAnalyticsConfig() {
        return analyticsConfig;
    }

    public RegistrationConfig getRegistrationConfig() {
        return registrationConfig;
    }

    public StorageConfig getStorageConfig() {
        return storageConfig;
    }
}
