package fleet.aux.validation;

import java.util.List;

public interface ValidationResult {
    boolean isValid();

    default boolean isNotValid() {
        return !isValid();
    }

    ValidationResult flatMap(ValidationResult other);

    List<ValidationError> getErrors();

    List<String> getMessages();
}
