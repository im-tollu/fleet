package fleet.aux.validation;

import java.util.Collections;
import java.util.List;

public class ValidationSuccess implements ValidationResult {
    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public ValidationResult flatMap(ValidationResult other) {
        return other;
    }

    @Override
    public List<ValidationError> getErrors() {
        return Collections.emptyList();
    }

    @Override
    public List<String> getMessages() {
        return Collections.emptyList();
    }
}
