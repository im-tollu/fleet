package fleet.aux.validation;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ValidationFailure implements ValidationResult {

    private final List<ValidationError> errors;
    private final ValidationResult other;

    public ValidationFailure(List<ValidationError> errors) {
        this(errors, new ValidationSuccess());
    }

    public ValidationFailure(List<ValidationError> errors, ValidationResult other) {
        this.errors = errors;
        this.other = other;
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public ValidationResult flatMap(ValidationResult other) {
        return new ValidationFailure(getErrors(), other);
    }

    @Override
    public List<ValidationError> getErrors() {
        List<ValidationError> errors = new LinkedList<>(this.errors);
        errors.addAll(other.getErrors());
        return errors;
    }

    @Override
    public List<String> getMessages() {
        return getErrors().stream().map(ValidationError::getMessage).collect(Collectors.toList());
    }
}
