package fleet.aux.validation;

public interface Validator {
    public ValidationResult validate();
}
