package fleet.aux;

public interface Component {
    void start();

    void stop();
}
