package fleet.aux;

import java.util.function.Supplier;

@FunctionalInterface
public interface ThrowingSupplier<A> {
    A get() throws Exception;

    static <A> Supplier<A> wrap(
            ThrowingSupplier<A> throwingSupplier
    ) {
        return () -> {
            try {
                return throwingSupplier.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
