package fleet.aux.result;

import java.util.List;

public interface WithDetails {
    List<String> getDetails();
}
