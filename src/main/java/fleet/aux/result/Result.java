package fleet.aux.result;

public interface Result {
    boolean isSuccess();

    default boolean isFailure() {
        return !isSuccess();
    }
}
