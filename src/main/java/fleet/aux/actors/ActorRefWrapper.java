package fleet.aux.actors;

import akka.actor.ActorRef;
import akka.pattern.PatternsCS;
import fleet.aux.Wrapper;

public abstract class ActorRefWrapper implements Wrapper<ActorRef> {
    private final ActorRef value;

    protected ActorRefWrapper(ActorRef value) {
        this.value = value;
    }

    @Override
    public ActorRef getValue() {
        return value;
    }

    protected void tell(Object o) {
        value.tell(o, ActorRef.noSender());
    }

    public Object ask(Object message, long timeoutMillis) {
        var future = PatternsCS.ask(value, message, timeoutMillis).toCompletableFuture();
        try {
            return future.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
