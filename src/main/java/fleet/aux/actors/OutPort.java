package fleet.aux.actors;

import akka.actor.ActorRef;

public abstract class OutPort extends ActorRefWrapper {

    public OutPort(ActorRef value) {
        super(value);
    }

    public void registerAdapter(ActorRef adapter){
        var adapterRef = new AdapterPlug(adapter);
        tell(adapterRef);
    }

    public static class AdapterPlug extends ActorRefWrapper {
        AdapterPlug(ActorRef value) {
            super(value);
        }
    }

}
