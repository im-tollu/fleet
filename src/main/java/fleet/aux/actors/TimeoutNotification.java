package fleet.aux.actors;

public class TimeoutNotification {
    private final String key;

    public TimeoutNotification(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
