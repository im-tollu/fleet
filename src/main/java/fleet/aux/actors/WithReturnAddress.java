package fleet.aux.actors;

import akka.actor.ActorRef;

public interface WithReturnAddress {

    void setReturnAddress(ActorRef address);

    ActorRef getReturnAddress();

    default void sendBack() {
        getReturnAddress().tell(this, ActorRef.noSender());
    }
}
