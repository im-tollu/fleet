package fleet.aux.webserver;

public class ParsingFailure extends ParsingResult {
    private final String message;

    public ParsingFailure(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }
}
