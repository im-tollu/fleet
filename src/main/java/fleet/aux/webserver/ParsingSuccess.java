package fleet.aux.webserver;

public class ParsingSuccess<T> extends ParsingResult {
    private final T parsedEntity;

    public ParsingSuccess(T parsedEntity) {
        this.parsedEntity = parsedEntity;
    }

    public T getParsedEntity() {
        return parsedEntity;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }
}
