package fleet.aux.webserver;

public abstract class ParsingResult {
    public abstract boolean isSuccess();

    public boolean isFailure() {
        return !isSuccess();
    }
}
