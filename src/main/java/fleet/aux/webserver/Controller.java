package fleet.aux.webserver;

import akka.NotUsed;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.FlowShape;
import akka.stream.javadsl.GraphDSL;

public interface Controller {
    boolean matchRequest(HttpRequest request);

    FlowShape<HttpRequest, HttpResponse> getFlowShape(GraphDSL.Builder<NotUsed> b);
}
