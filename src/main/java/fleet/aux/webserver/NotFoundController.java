package fleet.aux.webserver;

import akka.NotUsed;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.stream.FlowShape;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;

public class NotFoundController implements Controller {
    private final Materializer materializer;

    public NotFoundController(Materializer materializer) {
        this.materializer = materializer;
    }

    @Override
    public boolean matchRequest(HttpRequest request) {
        return false;
    }

    @Override
    public FlowShape<HttpRequest, HttpResponse> getFlowShape(GraphDSL.Builder<NotUsed> b) {
        return b.add(Flow.of(HttpRequest.class).map(this::handleRequest));
    }

    private HttpResponse handleRequest(HttpRequest request) {
        request.discardEntityBytes(materializer);
        return HttpResponse.create().withEntity(HttpEntities.EMPTY).withStatus(StatusCodes.NOT_FOUND);
    }
}
