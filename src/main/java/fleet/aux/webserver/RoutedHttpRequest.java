package fleet.aux.webserver;

import akka.http.javadsl.model.HttpRequest;

public class RoutedHttpRequest {
    private final HttpRequest request;
    private final Class<? extends Controller> routeTo;

    public RoutedHttpRequest(HttpRequest request, Class<? extends Controller> routeTo) {
        this.request = request;
        this.routeTo = routeTo;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public Class<? extends Controller> getRouteTo() {
        return routeTo;
    }
}
