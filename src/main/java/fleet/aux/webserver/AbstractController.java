package fleet.aux.webserver;

import akka.NotUsed;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.FlowShape;
import akka.stream.javadsl.Broadcast;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.Merge;

public abstract class AbstractController<ParsedEntity, ProcessedEntity> implements Controller {

    public abstract Flow<HttpRequest, ParsingResult, NotUsed> getParsingFlow();

    public abstract Flow<ParsedEntity, ProcessedEntity, NotUsed> getProcessingFlow();

    public abstract Flow<ProcessedEntity, HttpResponse, NotUsed> getResponseFormattingFlow();

    public abstract Flow<ParsingFailure, HttpResponse, NotUsed> getParsingFailureFlow();

    @SuppressWarnings("unchecked")
    @Override
    public FlowShape<HttpRequest, HttpResponse> getFlowShape(GraphDSL.Builder<NotUsed> b) {
        var parsingFlow = b.add(getParsingFlow());
        var parsingBroadcast = b.add(Broadcast.create(ParsingResult.class, 2));
        var responseMerge = b.add(Merge.create(HttpResponse.class, 2));
        var parsedEntityFlow = b.add(
                Flow.of(ParsingResult.class)
                .filter(ParsingResult::isSuccess)
                .map(result -> ((ParsingSuccess<ParsedEntity>) result).getParsedEntity())
                .via(getProcessingFlow())
                .via(getResponseFormattingFlow())
        );
        var parsingFailureFlow = b.add(
                Flow.of(ParsingResult.class)
                .filter(ParsingResult::isFailure)
                .map(result -> (ParsingFailure) result)
                .via(getParsingFailureFlow())
        );
        b.from(parsingFlow.out())
                .toFanOut(parsingBroadcast);
        b.from(parsingBroadcast)
                .via(parsingFailureFlow)
                .toFanIn(responseMerge);
        b.from(parsingBroadcast)
                .via(parsedEntityFlow)
                .toFanIn(responseMerge);
        return FlowShape.of(parsingFlow.in(), responseMerge.out());
    }
}
