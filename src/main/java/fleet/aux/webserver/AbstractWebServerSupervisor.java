package fleet.aux.webserver;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.FlowShape;
import akka.stream.Materializer;
import akka.stream.javadsl.Broadcast;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.Merge;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractWebServerSupervisor extends AbstractActor {
    private final String host;
    private final int port;
    private final List<Controller> controllers;
    private final Materializer materializer;
    private final ServerBinding binding;

    protected AbstractWebServerSupervisor(String host, int port, List<Controller> controllers, ActorSystem system, Materializer materializer) {
        this.host = host;
        this.port = port;
        this.materializer = materializer;
        this.controllers = new LinkedList<>(controllers);
        this.controllers.add(new NotFoundController(materializer));
        this.binding = startWebServer(system);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }

    @Override
    public void postStop() throws Exception {
        var unbindFuture = binding.unbind().toCompletableFuture();
        unbindFuture.get();
    }

    private ServerBinding startWebServer(ActorSystem system) {
        try {
            var http = Http.get(system);
            var bindingFuture = http.bindAndHandle(
                    getRequestResponseFlow(),
                    ConnectHttp.toHost(host, port),
                    materializer
            ).toCompletableFuture();
            return bindingFuture.get();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private Flow<HttpRequest, HttpResponse, NotUsed> getRequestResponseFlow() {
        var routingFlow = Flow.of(HttpRequest.class)
                .map(request -> new RoutedHttpRequest(request, route(request)));
        var graph = GraphDSL.create(b -> {
            var router = b.add(routingFlow);
            var broadcast = b.add(Broadcast.create(RoutedHttpRequest.class, controllers.size()));
            var merge = b.add(Merge.create(HttpResponse.class, controllers.size()));
            b.from(router.out()).toFanOut(broadcast);
            for (var controller : controllers) {
                var filter = b.add(routerFilter(controller));
                var controllerFlow = controller.getFlowShape(b);
                b.from(broadcast).via(filter);
                b.from(filter).via(controllerFlow);
                b.from(controllerFlow).toFanIn(merge);
            }
            return FlowShape.of(router.in(), merge.out());
        });
        return Flow.fromGraph(graph);
    }

    private static Flow<RoutedHttpRequest, HttpRequest, NotUsed> routerFilter(Controller controller) {
        var controllerClass = controller.getClass();
        return Flow.of(RoutedHttpRequest.class)
                .filter(routedRequest -> controllerClass.isAssignableFrom(routedRequest.getRouteTo()))
                .map(RoutedHttpRequest::getRequest);

    }

    private Class<? extends Controller> route(HttpRequest request) {
        for (Controller controller : controllers) {
            if (controller.matchRequest(request)) {
                return controller.getClass();
            }
        }
        return NotFoundController.class;
    }

}
