package fleet.aux.correlation;

public interface WithCorrelationId {
    CorrelationId getCorrelationId();
}
