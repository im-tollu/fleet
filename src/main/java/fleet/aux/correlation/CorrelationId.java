package fleet.aux.correlation;

import fleet.aux.Wrapper;

import java.util.Objects;
import java.util.UUID;

public class CorrelationId implements Wrapper<String> {
    private final UUID id;

    private CorrelationId(UUID id) {
        this.id = id;
    }

    @Override
    public String getValue() {
        return id.toString();
    }

    public static CorrelationId wrap(String s) {
        var nonNullString = Objects.requireNonNullElse(s, "");
        var uuid = UUID.fromString(nonNullString);
        return new CorrelationId(uuid);
    }

    public static CorrelationId generate() {
        var randomUUID = UUID.randomUUID();
        return new CorrelationId(randomUUID);
    }
}
