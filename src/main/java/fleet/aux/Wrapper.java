package fleet.aux;

public interface Wrapper<V> {
    V getValue();
}
