package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonLocation;

public class PublicJsonProcessingException extends com.fasterxml.jackson.core.JsonProcessingException {
    public PublicJsonProcessingException(String msg, JsonLocation loc, Throwable rootCause) {
        super(msg, loc, rootCause);
    }
}
