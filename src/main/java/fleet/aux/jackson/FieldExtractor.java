package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FieldExtractor {
    private final ObjectMapper mapper;

    public FieldExtractor(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public <T> T extract(TreeNode node, FieldTyping<T> fieldTyping) throws JsonProcessingException {
        var fieldName = fieldTyping.fieldName;
        var fieldType = fieldTyping.fieldType;
        var treeNode = node.get(fieldTyping.fieldName);
        return mapper.treeToValue(node.get(fieldTyping.fieldName), fieldTyping.fieldType);
    }
}
