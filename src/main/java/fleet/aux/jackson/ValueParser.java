package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;
import java.math.BigDecimal;

public interface ValueParser<V> {
    V extract(JsonParser jsonParser) throws IOException;

    static ValueParser<String> forString() {
        return JsonParser::getText;
    }

    static ValueParser<BigDecimal> forDecimal() {
        return JsonParser::getDecimalValue;
    }

    static ValueParser<Long> forLong() {
        return JsonParser::getLongValue;
    }

}
