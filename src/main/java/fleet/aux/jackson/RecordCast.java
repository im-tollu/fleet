package fleet.aux.jackson;

import java.util.function.Function;

public class RecordCast<T> {
    private final FieldTyping<?>[] definition;
    private final Function<Object[], T> valueFactory;
    private final Function<T, Object[]> valueExtractor;

    public RecordCast(FieldTyping<?>[] definition, Function<Object[], T> valueFactory, Function<T, Object[]> valueExtractor) {
        this.definition = definition;
        this.valueFactory = valueFactory;
        this.valueExtractor = valueExtractor;
    }

    public FieldTyping<?>[] getDefinition() {
        return definition;
    }

    public T apply(Object[] fieldValues) {
        validateFieldValues(fieldValues);
        return valueFactory.apply(fieldValues);
    }

    public Object[] extract(T value) {
        var fieldValues = valueExtractor.apply(value);
        validateFieldValues(fieldValues);
        return fieldValues;
    }

    private void validateFieldValues(Object[] fieldValues) {
        var isFieldValuesCountOk = fieldValues.length == definition.length;
        if (!isFieldValuesCountOk) {
            String message = "Field values count doesn't match definition";
            throw new IllegalArgumentException(message);
        }

        boolean isTypingValid = true;
        for (int i = 0; i < definition.length; i++) {
            var definedType = definition[i].fieldType;
            var actualType = fieldValues[i].getClass();
            isTypingValid = definedType.isAssignableFrom(actualType) && isTypingValid;
        }
        if (!isTypingValid) {
            String message = "Field values typing doesn't match definition";
            throw new ClassCastException(message);
        }
    }


}
