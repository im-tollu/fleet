package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.function.Function;

public class WrapperDeserializer<V, W> extends StdDeserializer<W> {
    private final Function<V, W> wrapper;
    private final ValueParser<V> valueParser;
    private final Class<W> type;

    public WrapperDeserializer(Class<W> w, Function<V, W> wrapper, ValueParser<V> valueParser) {
        super(w);
        this.type = w;
        this.wrapper = wrapper;
        this.valueParser = valueParser;
    }

    @Override
    public W deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext
    ) throws IOException {
        var jsonValue = valueParser.extract(jsonParser);
        try {
            return wrapper.apply(jsonValue);
        } catch (Exception e) {
            String message = String.format("Could not parse %s [%s]", type.getCanonicalName(), jsonValue);
            throw new PublicJsonProcessingException(message, jsonParser.getCurrentLocation(), e);
        }
    }
}
