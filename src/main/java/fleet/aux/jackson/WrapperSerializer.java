package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import fleet.aux.Wrapper;

import java.io.IOException;

public class WrapperSerializer<V> extends StdSerializer<Wrapper<V>> {

    private final ValueWriter<V> valueWriter;

    public WrapperSerializer(Class<? extends Wrapper<V>> w, ValueWriter<V> valueWriter) {
        //noinspection unchecked
        super((Class<Wrapper<V>>)w);
        this.valueWriter = valueWriter;
    }

    @Override
    public void serialize(Wrapper<V> w, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        var jsonValue = w.getValue();
        valueWriter.write(jsonValue, jsonGenerator);
    }

}
