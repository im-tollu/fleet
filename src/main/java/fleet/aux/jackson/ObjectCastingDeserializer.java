package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.LinkedList;

public class ObjectCastingDeserializer<T> extends StdDeserializer<T> {
    private final RecordCast<T> recordCast;

    public ObjectCastingDeserializer(Class<T> vc, RecordCast<T> recordCast) {
        super(vc);
        this.recordCast = recordCast;
    }

    @Override
    public T deserialize(
            JsonParser jsonParser,
            DeserializationContext context
    ) throws IOException {
        var objectMapper = (ObjectMapper) jsonParser.getCodec();
        var extractor = new FieldExtractor(objectMapper);
        var node = objectMapper.readTree(jsonParser);
        var fieldValues = extractFieldValues(node, extractor);
        return recordCast.apply(fieldValues);
    }

    private Object[] extractFieldValues(TreeNode node, FieldExtractor extractor) throws JsonProcessingException {
        var definition = recordCast.getDefinition();
        var fieldValues = new LinkedList<>();
        for (FieldTyping<?> fieldTyping : definition) {
            fieldValues.addLast(extractor.extract(node, fieldTyping));
        }
        return fieldValues.toArray();
    }
}
