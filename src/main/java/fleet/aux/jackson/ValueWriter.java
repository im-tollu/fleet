package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.math.BigDecimal;

public interface ValueWriter<V> {
    void write(V value, JsonGenerator generator) throws IOException;

    static ValueWriter<String> forString() {
        return (v, generator) -> generator.writeString(v);
    }

    static ValueWriter<BigDecimal> forDecimal() {
        return (v, generator) -> generator.writeNumber(v);
    }

    static ValueWriter<Long> forLong() {
        return (v, generator) -> generator.writeNumber(v);
    }
}
