package fleet.aux.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class ObjectCastingSerializer<T> extends StdSerializer<T> {
    private final RecordCast<T> recordCast;

    public ObjectCastingSerializer(Class<T> vc, RecordCast<T> recordCast) {
        super(vc);
        this.recordCast = recordCast;
    }

    @Override
    public void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        var mapper = jsonGenerator.getCodec();
        var definition = recordCast.getDefinition();
        var values = recordCast.extract(t);

        jsonGenerator.writeStartObject();
        for (int i = 0; i < definition.length; i++){
            var fieldTyping = definition[i];
            jsonGenerator.writeFieldName(fieldTyping.fieldName);
            mapper.writeValue(jsonGenerator, values[i]);
        }
        jsonGenerator.writeEndObject();
    }
}

