package fleet.aux.jackson;

public class FieldTyping<T> {
    final String fieldName;
    final Class<T> fieldType;

    public FieldTyping(String fieldName, Class<T> fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }
}