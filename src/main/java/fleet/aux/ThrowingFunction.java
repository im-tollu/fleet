package fleet.aux;

import java.util.function.Function;

@FunctionalInterface
public interface ThrowingFunction<A, B> {
    B apply(A a) throws Exception;

    static <A, B> Function<A, B> wrap(
            ThrowingFunction<A, B> throwingFunction
    ) {
        return arg -> {
            try {
                return throwingFunction.apply(arg);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
