package fleet.aux.statemachine;

import java.util.Arrays;

public abstract class StateMachine<Status extends Enum<Status>> {
    private Status status;

    protected StateMachine() {
        this.status = getInitialState();
    }

    protected abstract Status getInitialState();

    public Status getStatus() {
        return status;
    }

    protected void ensureStatus(Status... expected) {
        var expectedStatuses = Arrays.asList(expected);
        if (!expectedStatuses.contains(this.status)) {
            String message = String.format("Unexpected status: %s", this.status.name());
            throw new IllegalStateException(message);
        }
    }

    protected void transitionTo(Status status) {
        this.status = status;
    }

}
