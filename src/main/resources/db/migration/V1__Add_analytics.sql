CREATE SCHEMA IF NOT EXISTS
    analytics
;

CREATE TABLE
    analytics.date_dim (
        date_sk IDENTITY PRIMARY KEY,
        date DATE NOT NULL,
        month_name VARCHAR(9) NOT NULL,
        month INT NOT NULL,
        quarter INT NOT NULL,
        year INT NOT NULL
    )
;

CREATE TABLE
    analytics.driver_dim (
        driver_sk IDENTITY PRIMARY KEY,
        driver_id UUID NOT NULL,
        driver_name VARCHAR(128) NOT NULL
    )
;

CREATE TABLE
    analytics.fuel_dim (
        fuel_sk IDENTITY PRIMARY KEY,
        fuel_type VARCHAR(8) NOT NULL
    )
;

CREATE TABLE
    analytics.fueling_fact (
        fueling_fact_sk IDENTITY PRIMARY KEY,
        fueling_key BIGINT NOT NULL,
        date_sk BIGINT NOT NULL REFERENCES analytics.date_dim(date_sk),
        driver_sk BIGINT NOT NULL REFERENCES analytics.driver_dim(driver_sk),
        fuel_sk BIGINT NOT NULL REFERENCES analytics.fuel_dim(fuel_sk),
        price DECIMAL NOT NULL,
        volume DECIMAL NOT NULL
    )
;
