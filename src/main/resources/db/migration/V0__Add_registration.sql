CREATE SCHEMA IF NOT EXISTS
    registration
;

CREATE TABLE
    registration.fueling (
        fueling_key IDENTITY PRIMARY KEY,
        fuel VARCHAR(8) NOT NULL,
        price DECIMAL NOT NULL,
        volume DECIMAL NOT NULL,
        fueling_date DATE NOT NULL,
        driver_id UUID NOT NULL,
        created_at TIMESTAMP NOT NULL
    )
;

CREATE TRIGGER
    prevent_duplicate_fueling
    BEFORE INSERT
    ON
        registration.fueling
    FOR EACH ROW CALL
        "fleet.components.storage.PreventDuplicateFuelingTrigger"
;