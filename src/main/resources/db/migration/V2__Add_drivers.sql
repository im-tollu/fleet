CREATE SCHEMA IF NOT EXISTS
    drivers
;

CREATE TABLE
    drivers.driver (
        driver_key IDENTITY PRIMARY KEY,
        driver_id UUID NOT NULL,
        driver_name VARCHAR(128) NOT NULL
    )
;

INSERT INTO
    drivers.driver (
        driver_id,
        driver_name
    )
VALUES
    ('e3ab5042-900d-467c-8a3f-090919548701', 'Nilima Matthijs'),
    ('1272778f-9dbc-49de-8154-1bfe5c323129', 'Marijan Danya'),
    ('d7f839fd-1e7f-43e3-9c85-92229b3ac9ca', 'Chandan Cristian'),
    ('41733766-c91c-417a-aba7-8605c474f62e', 'Fionnuala Joktan'),
    ('727f6582-4133-4539-a44b-a8e88c886478', 'Rupa Alexandros'),
    ('f72ac21d-24f3-4e8f-84c1-9f1dffed8dff', 'Surya Turid'),
    ('007fad85-8758-4d9b-92e5-0a51fe51d51b', 'Longinus Sara'),
    ('3e73094b-2d99-4dfd-bf9e-3a60afea0b96', 'Amar Deniel'),
    ('326b9868-c3ac-4ffb-b5f8-4e6d7887e896', 'Kennedy Nuka'),
    ('7e767b42-0a97-40c6-b71b-a02fd2b8377c', 'Dunstan Octavia'),
    ('1ad6a290-c96d-48c6-b181-4ba12e5a4016', 'Antônio Jayson'),
    ('cc3ad88d-52b8-41ce-ab8b-875b8901307e', 'Bárbara Shakuntala'),
    ('9458b401-85c1-4ed8-b62b-36e3a6ae7e33', 'Emmanouel Dimas'),
    ('017f1fdc-ebf1-473d-9904-f15dba08cdc3', 'Justinian Narkissos'),
    ('1961b23b-352d-49f2-bbe6-419616cce534', 'Abubakar Viktoriya'),
    ('0f0daafe-12fd-404b-84f6-d875ecfad616', 'Jyoti Terpsichore'),
    ('c139bac4-9684-4837-bb14-6bca73066678', 'Viktoria Guillem'),
    ('8a3441f4-2bea-431d-bf46-79627e207352', 'Achilleus Siavash'),
    ('5df06e7a-7cd1-4b5f-b56c-ea0a198d6e32', 'Aristaeus Madeline'),
    ('a72617b6-c116-4890-896c-86a5d3377eb7', 'Ásdís Wanjiku'),
    ('91ff64da-070f-401d-ad6d-4e7c9be63017', 'Nisha Hanna'),
    ('d53ae7b1-7992-4ef9-86c6-bb00c4678c42', 'Priyanka Lucija'),
    ('e4821d52-7888-41e8-878e-f41fcfb7076e', 'Jozef Lütfiye'),
    ('97a4adb8-3449-4cbe-9e83-a0ed1094c6f2', 'Varinia Eulália')
;