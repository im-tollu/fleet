# Fuel consumption application

## Build and run

To run the system with default parameters, run from the project's root:

```
$ mvn clean compile test exec:exec 
```

When the system is run for the first time, `~/fleet.mv.db` is created by H2. Delete it to 
drop all the data.

Registration and analytics web servers are run on a separate hosts/ports and can be 
configured by ENV variables as well as fueling expiration days:

```
$ FLEET_REGISTRATION_HOST=localhost FLEET_REGISTRATION_PORT=8080 FLEET_FUELING_EXPIRATION_DAYS=12 FLEET_ANALYTICS_HOST=localhost FLEET_ANALYTICS_PORT=8081 mvn clean compile test exec:exec
```

### Registration endpoints

The system provides the following registration endpoints (when Registration service is run on http://localhost:8080):

```
POST http://localhost:8080/registration
```

Use content-type "application/json" to post a single fueling and "application/json-seq" for bulk loading.
In the latter case fueling body should consist of a sequence of fueling registration JSONs.

Fueling registration example:

```
{"fuel": "A95",  "price": "12.01",  "volume": "12",  "date": "2018-09-18",  "driverId": "e3ab5042-900d-467c-8a3f-090919548701"}
```

Bulk registration example can be found in `./examples/fuelingRegistrations.json-seq`.

### Analytics endpoints

The system provides the following analytics endpoints (given Analytics service is run on http://localhost:8081):

```
http://localhost:8081/consumption?year=2018&month=9&driver=e3ab5042-900d-467c-8a3f-090919548701
```

```
http://localhost:8081/statistics?driver=e3ab5042-900d-467c-8a3f-090919548701
```

```
http://localhost:8081/total?driver=e3ab5042-900d-467c-8a3f-090919548701
```

In the given examples "driver" parameter is optional, while "year" and "month" are mandatory (for consumption query).


## System context

The author supposes that this task is a part of a fleet management effort.

The system gets fueling data from fueling cards provider(s) over http.
Next, the company's manager queries analytics from the system over http to be rendered in some web-based BI application.

In order to operate the system also needs drivers' data from a drivers' service.

## Design

The developer's goal is to implement the system according to the
 [reactive principles](https://www.reactivemanifesto.org/). 

The assignment says that the business is small. 
It means, that from the start we don't need to develop for big scale.
The first release will be implemented in a single JVM, but with modularization to make the following separation as easy as possible.

The system supposes asymmetric load pattern regarding writes and reads.
Writes should enforce some business invariants bounded by driver ID and may operate on recent data.
Read model is of analytic warehouse kind and operates on historic data.

This write/read diversity makes Command and Query Responsibility Segregation with Event Sourcing (CQRS/ES) a good implementation pattern.
The author separates operational and analytic storage as well. First of all, operational storage acts as a single source of truth 
and may be emptied every month or quarter (when historic data has no use for preserving consistency) to decrease load. 
Second, analytic storage may be rebuilt completely from the fuel registration data, when BI needs change and there is a need to redesign it.

### Components

* *Registrations* - is a "C"-part in CQRS. Processes registration requests, enforces business rules validation, emits registration events.
* *Analytics* - is a "Q"-part in CQRS. Subscribes to registration events, makes ETL, serves analytics data.
* *Storage* - is present as a single component, though has decoupled parts for Registration and Analytics components.
* *EventBus* - connects Registration and Analytics components.

### Hexagonal architecture and Domain-Driven Design

The system uses a form of Domain-Driven Design. It has vocabulary, which models the system's ubiquitous language as a set of value objects.
It also has entities, which are identified by keys. But their state is formed by vocabulary. It has decision maker as an aggregate.
And it has domain commands and events, which go into and out of decision maker.

Registration and Analytics are implemented using Hexagonal architecture, which combines greatly with Domain-Driven Design.
Business core doesn't depend on infrastructure adapters and doesn't contain any blocking code. It has functional (pure, testable) part and operational part.

Business core communicates with the outer world via interface ports. There are primary ports, where interaction with the core
is initiated from the outside. And secondary ports, where the business core initiates interaction.

Adapters serve as mediators between ports and infrastructure, implement IO.

### The Actor Model

The Actor Model is used for the operational logic of the business cores. It is asynchronous, resilient and message driven. 
Such concepts as timeouts, retries and back-offs are modeled with ease using The Actor Model. 

### Reactive streams

Reactive streams support non-blocking backpressure and allow the system to operate on streaming data
within a limited memory footprint. For example, when getting analytics reports from DB or bulk loading data.
In the system reactive streams are used for IO.

### Configuration

Configuration parameters are read, parsed and validated in a single code module. Parsing 
config results in an application configuration object model. Based on this model individual 
component's configuration object models are built.

Application is supposed to be containerized, so externally configurable parameters are
read from ENV variables and, if set, override defaults. 

### Startup & shutdown

The system startup and shutdown are coordinated by Runner.

### To be done
Though integration with external services is shown on the Drivers' service example, it is 
supposed, that there are more external services to integrate with. E.g. Fueling Providers service,
Authorization service, Vehicles' service and so on.

HTTPS encryption with mutual authentication can be used to integrate with a fueling providers.

Price and Volume in JSON are better to be represented as Strings, not Numbers.

Monitoring/instrumentation is not implemented. It is necessary to observe how the system operates
in production and to make up- and downscaling decisions, reject external requests based on load. 
Liveness and readiness checks are not implemented as well.

More can be done to make system more resilient to external dependencies' failures.

More tests should be added: 

* Integration tests to test adapters. 
* E2E tests with source data and expected reports data.
* There are places, where more unit tests would be useful as well. For example, to expect the same CorrelationId at
multiple processing stages.

## Technologies

* Akka Actors - an implementation of The Actor Model
* Akka Streams - an implementation of Reactive Streams
* Akka Http (low-level reactive stream-based server API) - web server
* HOCON - configuration notation and library
* Jackson (low-level API) - JSON object mapping
* JUnit

* H2 - embedded RDBMS
* Flyway - DB migrations
* Maven 3.5+
* Java 10